import { Pipe, PipeTransform } from '@angular/core';

import moment from 'moment';
/**
 * Generated class for the FechasPipe pipe.
 *
 * See https://angular.io/api/core/Pipe for more info on Angular Pipes.
 */
@Pipe({
  name: 'fechas',
})
export class FechasPipe implements PipeTransform {
  /**
   * Takes a value and makes it lowercase.
   */
  transform(value: string, args) {

    moment.locale('es');
    
    if(args == "long"){
      var a = moment(value).format('ddd D/MMM/YYYY H:mm');;
    }else if(args == "short"){
      var a = moment(value).format('D/MMM/YYYY H:mm');;
    }else if(args == "hora"){
      var a = moment(value).format('H:mm');;
    }
    
    return a;
  }
}
