import { NgModule } from '@angular/core';
import { FechasPipe } from './fechas/fechas';
@NgModule({
	declarations: [FechasPipe],
	imports: [],
	exports: [
		FechasPipe
		]
})
export class PipesModule {}
 