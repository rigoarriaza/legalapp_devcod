import { Injectable } from '@angular/core';
import { Http,Headers,RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/timeout';
import { ToastController } from "ionic-angular";

export interface User{
  nombre:string;
  apellido:string;
  foto:string;
  tipo:number;
}

export interface datos{
  correo:string;
  password:string;
}

@Injectable()
export class AuthProvider {

  currentUser: User;
  duser: datos;
  public usuario: any;
  public foto: string;
  public datalogin: any;
  //produccion
  // ruta = "http://legalbo.com/legalapp/API_REST/";
  //dev
  ruta = "http://legalbo.com/legalapp/API_REST_dev/";
  

  constructor(public toastCtrl: ToastController, public http: Http) {
  }

  login(email: string, pw: string) : Promise<string>{

    return new Promise((resolve,reject)=>{
      this.duser = {
        correo: email,
        password: pw,
      };
      // debugger;
      this.postLogin(this.duser).timeout(10000).subscribe( data=> { 
        console.log(data);
        // debugger;
        if (data.errorCode==0){
          data.msg['tipo_user'] = '1';
          // definir foto
          if(data.msg['u_foto'] == ''){
              data.msg['u_foto'] = 'assets/img/avatar.png';
          }else{
              data.msg['u_foto'] = 'http://legalbo.com/legalapp/IMGUSERS/' +  data.msg['u_foto'];
          }
          // this.setFoto(data.msg['u_foto']);    
          localStorage.setItem('legal_userApp',JSON.stringify(data.msg));
          
          resolve('0');
        }else if (data.errorCode==3){
          let msj = "Datos incorrectos, por favor vuelva a intentarlo";
            reject(msj);
        }
      }, (err) => {
          reject(err);
          this.toastCtrl.create({
            message: `Problemas con la conexión a internet.`,
            duration: 3000
          }).present();
      });

    });

  }
  loginFB(datos: any) : Promise<string>{

    return new Promise((resolve,reject)=>{
      
      this.datalogin = datos;
    
      // debugger;
      this.postLogin(this.datalogin).timeout(15000).subscribe( data=> { 
        console.log(data);
        // debugger;
        if (data.errorCode==0){
          data.msg['tipo_user'] = '1';
          // definir foto
          if(data.msg['u_foto'] == ''){
              data.msg['u_foto'] = 'assets/img/avatar.png';
          }else{
              data.msg['u_foto'] = data.msg['u_foto'];
          }
          // this.setFoto(data.msg['u_foto']);    
          localStorage.setItem('legal_userApp',JSON.stringify(data.msg));
          
          resolve('0');
        }else if (data.errorCode==3){
          let msj = "Datos incorrectos, por favor vuelva a intentarlo";
            reject(msj);
        }
      }, (err) => {
          reject(err);
          this.toastCtrl.create({
            message: `Problemas con la conexión a internet.`,
            duration: 3000
          }).present();
      });

    });

  }

  loginPro(email: string, pw: string) : Promise<string>{

    return new Promise((resolve,reject)=>{
      this.duser = {
        correo: email,
        password: pw,
      };
      
      this.getLoginPro(this.duser).timeout(10000).subscribe( data=> { 
        console.log(data);
        if (data.errorCode==0){
          data.msg['tipo_user'] = '2';
          // definir foto
          if(data.msg['u_foto'] == ''){
              data.msg['u_foto'] = 'assets/img/avatar.png';
          }else{
              data.msg['u_foto'] = 'http://legalbo.com/legalapp/IMGUSERS/' +  data.msg['u_foto'];
          }
          this.setFotoPro(data.msg['u_foto']);   
          localStorage.setItem('legal_userApp',JSON.stringify(data.msg));

          resolve('0');
        }else if (data.errorCode==3){
          let msj = "Datos incorrectos, por favor vuelva a intentarlo";
            reject(msj);
        }
      }, (err) => {
          reject(err);
          this.toastCtrl.create({
            message: `Problemas con la conexión a internet.`,
            duration: 3000
          }).present();
      });

    });

  }

  isLoggedIn(){
    let userApp = localStorage.getItem('legal_userApp');
    if(userApp == '0' || userApp == undefined){
      return false;
    }else{
      return true;
    }
  }

  setSesionData(correo,clave,tipo,activar){
    let sData = {};
    if(localStorage.getItem('legal_sessionData') != undefined){
      sData = JSON.parse(localStorage.getItem('legal_sessionData'));
    }
    if(tipo == 1){
      if(activar == 1){
        sData['sesion_user'] = '1' ; 
        sData['user_u'] = correo ; 
        sData['pass_u'] = clave ; 
      }else{
        sData['sesion_user'] = '0'; 
      }
    }else{
      if(activar == 1){
        sData['sesion_pro'] = '1' ; 
        sData['user_p'] = correo ; 
        sData['pass_p'] = clave ; 
      }else{
        sData['sesion_pro'] = '0' ; 
      }    
    }
    localStorage.setItem('legal_sessionData',JSON.stringify(sData));
  }
  getSesionData(tipo){
    if(localStorage.getItem('legal_sessionData') == undefined){
      return 0 ;
    }else{
      // return 0;
      let userApp = JSON.parse(localStorage.getItem('legal_sessionData')); 
      if(tipo == 1){
        if(userApp['sesion_user'] == undefined||userApp['sesion_user'] == null){
          return 0;
        }else if(userApp['sesion_user'] == '1' ){
          let sData = JSON.parse(localStorage.getItem('legal_sessionData')); 
          return sData;
        }else{
          return 0 ;
        }
      }else{
        if(userApp['sesion_pro'] == undefined||userApp['sesion_pro'] == null){
          return 0;
        }else if(userApp['sesion_pro'] == '1'){
          let sData = JSON.parse(localStorage.getItem('legal_sessionData')); 
          return sData;
        }else{
          return 0 ;
        }
      }

    }
    
  }

  logout(){
    this.currentUser = null;
    localStorage.setItem('legal_userApp','0')
    localStorage.setItem('legal_token',''); 
    localStorage.setItem('legal_user',''); 
    localStorage.setItem('legal_idprofesional',''); 
  }

  isUsuario(){
    let userApp = JSON.parse(localStorage.getItem('legal_userApp')); 
    return userApp['tipo_user'] === '1';
  }

  getToken(){
    let userApp = JSON.parse(localStorage.getItem('legal_userApp')); 
    return userApp['token_session'];
  }

  getUsuario(){
    let userApp = JSON.parse(localStorage.getItem('legal_userApp')); 
    return userApp['iduserapp'];
  }
  getProfesional(){
    let userApp = JSON.parse(localStorage.getItem('legal_userApp')); 
    return userApp['idprofesional'];
  }
  getFotoPro(){
    let userApp = JSON.parse(localStorage.getItem('legal_userApp')); 
    // return userApp['u_foto'];
    return userApp['u_foto'];
  }
  setFoto(foto){
    let userApp = JSON.parse(localStorage.getItem('legal_userApp')); 
    userApp['u_foto'] = foto;
    localStorage.setItem('legal_userApp',JSON.stringify(userApp));
    this.foto = foto;

  }
  setFotoPro(foto){
    localStorage.setItem('legal_userApp_Foto', foto);
  }
  getFotoProfesional(){
    return localStorage.getItem('legal_userApp_Foto');
  }
  getSaldoUsuario(){
    let userApp = JSON.parse(localStorage.getItem('legal_userApp')); 
    return userApp['u_ssaldo'];
  }

  getEstado(){
    // debugger;
    let userApp = JSON.parse(localStorage.getItem('legal_userApp')); 
    return userApp['u_estado'];
  }

  getPlanProfesional(){
    let userApp = JSON.parse(localStorage.getItem('legal_userApp')); 

    return userApp['plan']['id_plan'];
  }

  postLogin(contacto){
    // debugger;
      var dsInfo = this.ruta+'setLoginUsuarioApp';
      return this.http.post( dsInfo, contacto, this.getheaders())
        .map( res => res.json())
  }
  getLoginPro(usuario){
      var dsInfo = this.ruta+'setLoginProfesionalApp';
      // var dsInfo = 'http://legalbo.com/legalapp/API_REST/getLoginPro/'+usuario+'/'+clave;
      return this.http.post( dsInfo,usuario, this.getheaders() )
        .map( res => res.json())
  }
  getheaders()
{
    

 var headers = new Headers();
    headers.append('Access-Control-Allow-Origin' , '*');
    headers.append('Access-Control-Allow-Headers','POST, GET, PUT, DELETE, OPTIONS, HEAD, authorization');
    // headers.append('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT');
    // headers.append('Accept','application/json');
    // headers.append('content-type','application/json');
     let options = new RequestOptions({ headers:headers,withCredentials: true});

     return options;

}

}
