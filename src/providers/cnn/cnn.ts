import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';

import { Network } from "@ionic-native/network";
import { ToastController } from "ionic-angular";
/*
  Generated class for the CnnProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class CnnProvider {

  public Online : boolean = true;
  
  constructor( private network: Network, public toastCtrl: ToastController) {
    console.log('Hello CnnProvider Provider');
  }
 
   con_info() {
    this.network.onConnect().subscribe(data => {
      // console.log(data)
      // localStorage.setItem('mindful_cnn', 'true');
      this.Online= true;
      this.displayNetworkUpdate(data.type);
    }, error => console.error(error));
  
    this.network.onDisconnect().subscribe(data => {
      console.log(data)
      this.Online= false;
      // localStorage.setItem('mindful_cnn', 'false');
      this.displayNetworkUpdate(data.type);
    }, error => console.error(error));
  }

  isOnline(){
    return this.Online;
  }


  displayNetworkUpdate(connectionState: string){
    if(`${connectionState}` == "offline"){
      this.toastCtrl.create({
        message: `Has perdido la conexión a internet.`,
        duration: 3000
      }).present();
    }

  }
}
