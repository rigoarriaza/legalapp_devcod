import { Injectable } from '@angular/core';
import { Http, RequestOptions,Headers } from '@angular/http';
import 'rxjs/add/operator/map';


@Injectable()
export class ServiceProvider {

    //produccion
  // ruta = "http://legalbo.com/legalapp/API_REST/";
    //dev
  ruta = "http://legalbo.com/legalapp/API_REST_dev/";
  
  constructor(public http: Http) {
    console.log('Hello ServiceProvider Provider');
  }

getheaders()
{
    
    // headers.append(‘Content-Type’, ‘application/x-www-form-urlencoded; charset=UTF-8’);


    var headers = new Headers();
    headers.append('Access-Control-Allow-Origin' , '*');
    headers.append('Access-Control-Allow-Headers','POST, GET, PUT, DELETE, OPTIONS, HEAD, authorization');
    // headers.append('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT');
    // headers.append('Accept','application/json');
    // headers.append('content-type','application/json');
     let options = new RequestOptions({ headers:headers,withCredentials: true});

     return options;

// return this.http.post(this.url, postParams, options).do( res =&gt; console.log(res)); 

}
  postLogin(contacto){
      debugger;
      var dsInfo = this.ruta+'setLoginUsuarioApp';
      return this.http.post( dsInfo, contacto, this.getheaders())
        .map( res => res.json());   

  }
  

  getLogin(usuario,clave){
      var dsInfo = this.ruta+'setLoginUsuarioApp/'+usuario+'/'+clave;
      return this.http.get(dsInfo)
        .map( res => res.json())
  }

  getLoginPro(usuario,clave){
      var dsInfo = this.ruta+'getLoginPro/'+usuario+'/'+clave;
      return this.http.get(dsInfo)
        .map( res => res.json())
  }

  postRegistro(datos){
      var dsInfo = this.ruta+'setRegistroUsuarioApp';
      return this.http.post(dsInfo, datos, this.getheaders())
        .map( res => res.json())
  }
  postRegistroPro(datos){
      var dsInfo = this.ruta+'setRegistroProfesionalApp';
      return this.http.post(dsInfo, datos, this.getheaders())
        .map( res => res.json())
  }

  postPreguntaExpress(datos){
      var dsInfo = this.ruta+'setPreguntaExpress';
      return this.http.post(dsInfo, datos, this.getheaders())
        .map( res => res.json())
  }
  
  postModificarPreguntaExpress(datos){
      var dsInfo = this.ruta+'setModificarExpress';
      return this.http.post(dsInfo, datos, this.getheaders())
        .map( res => res.json())
  }

  setPaymentPreguntaExpress(datos){
      var dsInfo = this.ruta+'setPaymentPreguntaExpress';
      return this.http.post(dsInfo, datos, this.getheaders())
        .map( res => res.json())      
  }

  postCancelarPreguntaExpress(datos){
      var dsInfo = this.ruta+'setCancelarExpress';
      return this.http.post(dsInfo, datos, this.getheaders())
        .map( res => res.json())
  }
  setRespuestaPreguntaExpressUApp(datos){
      var dsInfo = this.ruta+'setRespuestaPreguntaExpressUApp';
      return this.http.post(dsInfo, datos, this.getheaders())
        .map( res => res.json())
  }

  postPreguntaDirecta(datos){
      var dsInfo = this.ruta+'setPreguntaDirectaUsuarioApp';
      return this.http.post(dsInfo, datos, this.getheaders())
        .map( res => res.json())
  }
  getValorServiciosProfesional(id){
      var dsInfo = this.ruta+'getValorServiciosProfesional/'+id;
      return this.http.get(dsInfo)
        .map( res => res.json())
  }
  setCancelarPreguntaDirecta(datos){
      var dsInfo = this.ruta+'setCancelarPreguntaDirecta';
      return this.http.post(dsInfo, datos, this.getheaders())
        .map( res => res.json())
  }
  setPaymentPreguntaDirecta(datos){
      var dsInfo = this.ruta+'setPaymentPreguntaDirecta';
      return this.http.post(dsInfo, datos, this.getheaders())
        .map( res => res.json())      
  }
  setModificarPreguntaDirecta(datos){
      var dsInfo = this.ruta+'setModificarPreguntaDirecta';
      return this.http.post(dsInfo, datos, this.getheaders())
        .map( res => res.json())
  }
  setRespuestaPreguntaDirectaUApp(datos){
      var dsInfo = this.ruta+'setRespuestaPreguntaDirectaUApp';
      return this.http.post(dsInfo, datos, this.getheaders())
        .map( res => res.json())
  }
  setCotizacionUsuarioapp(datos){
      var dsInfo = this.ruta+'setCotizacionUsuarioapp';
      return this.http.post(dsInfo, datos, this.getheaders())
        .map( res => res.json())
  }
  
  setAbogadoFavorito(datos){
      var dsInfo = this.ruta+'setAbogadoFavorito';
      return this.http.post(dsInfo, datos, this.getheaders()) 
        .map( res => res.json())
  }
  getAbogadosFavorito(user,token,inicio, fin){
      var dsInfo = this.ruta+'getAbogadosFavorito/'+user+'/'+token+'/'+inicio+'/'+fin;
      return this.http.get(dsInfo, this.getheaders())
        .map( res => res.json())
  }
  
//   getAbogados(user,token,inicio, fin){
//       var dsInfo = this.ruta+'getAbogados/'+user+'/'+token+'/'+inicio+'/'+fin;
//       return this.http.get(dsInfo, this.getheaders())
//         .map( res => res.json())
//   }
  getAbogados(user,token,inicio, fin, random){
      var dsInfo = this.ruta+'getAbogados/'+user+'/'+token+'/'+inicio+'/'+fin+'/'+random;
      return this.http.get(dsInfo, this.getheaders())
        .map( res => res.json())
  }
//   getAbogadosFiltroEspecialidad(user,token,especialidad,inicio, fin){
//       var dsInfo = this.ruta+'getAbogadosFiltroEspecialidad/'+user+'/'+token+'/'+especialidad+'/'+inicio+'/'+fin;
//       return this.http.get(dsInfo, this.getheaders())
//         .map( res => res.json())
//   }
  getAbogadosFiltroEspecialidad(user,token,especialidad,inicio, fin,random){
      var dsInfo = this.ruta+'getAbogadosFiltroEspecialidad/'+user+'/'+token+'/'+especialidad+'/'+inicio+'/'+fin+'/'+random;
      return this.http.get(dsInfo, this.getheaders())
        .map( res => res.json())
  }


  getAbogadosDestacados(user,token,random){
      var dsInfo = this.ruta+'getAbogadosDestacados/'+user+'/'+token+'/0/20/'+random;
      return this.http.get(dsInfo)
        .map( res => res.json())
  }
  getAbogadosDestacadosFiltroEspecialidad(user,token,especialidad, random){
      var dsInfo = this.ruta+'getAbogadosDestacadosFiltroEspecialidad/'+user+'/'+token+'/0/20/'+especialidad+'/'+random;
      return this.http.get(dsInfo)
        .map( res => res.json())
  }
  getPerfilProfesionalApp(user,token,id){
      var dsInfo = this.ruta+'getPerfilProfesionalApp/'+user+'/'+token+'/'+id;
      return this.http.get(dsInfo)
        .map( res => res.json())
    
  }

  getPreguntasExpress(user,token,inicio,fin){
      var dsInfo = this.ruta+'getHistorialExpressUsuarioApp/'+user+'/'+token+'/'+inicio+'/'+fin;
      return this.http.get(dsInfo)
        .map( res => res.json())
  }

  getPreguntaExpressDetalle(user,token,pregunta){
      var dsInfo = this.ruta+'getDetalleExpressUsuarioApp/'+user+'/'+token+'/'+pregunta;
      return this.http.get(dsInfo)
        .map( res => res.json())
  }

  getPreguntasDirectas(user,token){
      var dsInfo = this.ruta+'getHistorialPreguntaDirectaU/'+user+'/'+token+'/0/10';
      return this.http.get(dsInfo)
        .map( res => res.json())
  }
  
  getPreguntaDirectaDetalle(user,token,pregunta){
      var dsInfo = this.ruta+'getDetallePreguntaDirectaUApp/'+user+'/'+token+'/'+pregunta;
      return this.http.get(dsInfo)
        .map( res => res.json())
  }
  getPreguntasCotizacion(user,token){
      var dsInfo = this.ruta+'getHistorialCotizacionesUapp/'+user+'/'+token+'/0/10';
      return this.http.get(dsInfo)
        .map( res => res.json())
  }
  getPreguntasCotizacionDetalle(user,token,cotizacion){
      var dsInfo = this.ruta+'getDetalleCotizacionUsuarioApp/'+user+'/'+token+'/'+cotizacion;
      return this.http.get(dsInfo)
        .map( res => res.json())
  }
  setInformacionUsuarioApp(datos){
      var dsInfo = this.ruta+'setInformacionUsuarioApp';
      return this.http.post(dsInfo, datos, this.getheaders())
        .map( res => res.json())
  }
  setPasswordUsuario(datos){
      var dsInfo = this.ruta+'setPasswordUsuario';
      return this.http.post(dsInfo, datos, this.getheaders())
        .map( res => res.json())
  }
  setModificarFotoUsuario(datos){
      var dsInfo = this.ruta+'setModificarFotoUsuario';
      return this.http.post(dsInfo, datos, this.getheaders())
        .map( res => res.json())
  }
  getRecuperarPass(mail){
      var dsInfo = this.ruta+'getRecuperarPass/'+mail;
      return this.http.get(dsInfo)
        .map( res => res.json())
  }
  getRecuperarPassProfesional(mail){
      var dsInfo = this.ruta+'getRecuperarPassProfesional/'+mail;
      return this.http.get(dsInfo)
        .map( res => res.json())
  }
  getDetallePagosUsuario(user,token){
      var dsInfo = this.ruta+'getHistorialRecargasUsuarioApp/'+user+'/'+token;
      return this.http.get(dsInfo)
        .map( res => res.json())
  }
  getHistorialDebitosUsuarioApp(user,token){
      var dsInfo = this.ruta+'getHistorialDebitosUsuarioApp/'+user+'/'+token;
      return this.http.get(dsInfo)
        .map( res => res.json())
  }
  getCarnetUsuario(user,token){
      var dsInfo = this.ruta+'getCarnetUsuario/'+user+'/'+token;
      return this.http.get(dsInfo)
        .map( res => res.json())
  }
  
  //profesional

  setModificarPerfilProfesional(datos){
      var dsInfo = this.ruta+'setModificarPerfilProfesional';
      return this.http.post(dsInfo, datos, this.getheaders())
        .map( res => res.json())
  }
  setPasswordProfesional(datos){
      var dsInfo = this.ruta+'setPasswordProfesional';
      return this.http.post(dsInfo, datos, this.getheaders())
        .map( res => res.json())
  }
  setModificarFotoProfesional(datos){
      var dsInfo = this.ruta+'setModificarFotoProfesional';
      return this.http.post(dsInfo, datos, this.getheaders())
        .map( res => res.json())
  }
  setImagenRequerimiento(datos){
      var dsInfo = this.ruta+'setImagenRequerimiento';
      return this.http.post(dsInfo, datos, this.getheaders())
        .map( res => res.json())
  }
  getInformacionProfesionalApp(user,token){
      var dsInfo = this.ruta+'getInformacionProfesionalApp/'+user+'/'+token;
      return this.http.get(dsInfo)
        .map( res => res.json())
  }
  getPreguntaExpressPendiente(user,token){
      var dsInfo = this.ruta+'getPreguntasExpressPendientes/'+user+'/'+token+'/0/10';
      return this.http.get(dsInfo)
        .map( res => res.json())
  }
  getPreguntasDirectasPro(user,token){
      var dsInfo = this.ruta+'getPreguntasDirectas/'+user+'/'+token+'/0/10';
      return this.http.get(dsInfo)
        .map( res => res.json())
  }
  getDetallePreguntaDirectaProfesional(user,token,id){
      var dsInfo = this.ruta+'getDetallePreguntaDirectaProfesional/'+user+'/'+token+'/'+id;
      return this.http.get(dsInfo)
        .map( res => res.json())
  }
  getCotizacionesPro(user,token){
      var dsInfo = this.ruta+'getCotizacionesPendientes/'+user+'/'+token+'/0/10';
      return this.http.get(dsInfo)
        .map( res => res.json())
  }
  getDetalleCotizacion(user,token,id){
      var dsInfo = this.ruta+'getDetalleCotizacion/'+user+'/'+token+'/'+id;
      return this.http.get(dsInfo)
        .map( res => res.json())
  }
  setPreguntaCompleja(datos){
      var dsInfo = this.ruta+'setPreguntaCompleja';
      return this.http.post(dsInfo, datos, this.getheaders())
        .map( res => res.json())
  }
  setRechazarPreguntaDirecta(datos){
      var dsInfo = this.ruta+'setRechazarPreguntaDirecta';
      return this.http.post(dsInfo, datos, this.getheaders())
        .map( res => res.json())
  }
  setRechazarCotizacion(datos){
      var dsInfo = this.ruta+'setRechazarCotizacion';
      return this.http.post(dsInfo, datos, this.getheaders())
        .map( res => res.json())
  }
  getDetallePreguntaContestada(user,token,id){
      var dsInfo = this.ruta+'getDetallePreguntaContestada/'+user+'/'+token+'/'+id;
      return this.http.get(dsInfo)
        .map( res => res.json())
  }
  postRespuestaPreguntaExpress(datos){
      var dsInfo = this.ruta+'setRespuestapreguntaExpress';
      return this.http.post(dsInfo, datos, this.getheaders())
        .map( res => res.json())
  }
  postRespuestaPreguntaExpressTomar(datos){
      var dsInfo = this.ruta+'setTomarPreguntaExpress';
      return this.http.post(dsInfo, datos, this.getheaders())
        .map( res => res.json())
  }
  postRespuestaPreguntaDirecta(datos){
      var dsInfo = this.ruta+'setRespuestaPreguntaDirectaPApp';
      return this.http.post(dsInfo, datos, this.getheaders())
        .map( res => res.json())
  }
  postRespuestaCotizacion(datos){
      var dsInfo = this.ruta+'setRespuestaCotizacion';
      return this.http.post(dsInfo, datos, this.getheaders())
        .map( res => res.json())
  }
  
  postCalificacionExpress(datos){
      var dsInfo = this.ruta+'setCalificarConsultaExpress';
      return this.http.post(dsInfo, datos, this.getheaders())
        .map( res => res.json())
  }
  
  getEspecialidades(){
      var dsInfo = this.ruta+'getEspecialidades/0/10';
      return this.http.get(dsInfo)
        .map( res => res.json())
  }
  setModificarEspecialidadProfesional(datos){
      var dsInfo = this.ruta+'setModificarEspecialidadProfesional';
      return this.http.post(dsInfo, datos, this.getheaders())
        .map( res => res.json())
  }
  
  getPasantias(){
      var dsInfo = this.ruta+'getPasantias/0/10';
      return this.http.get(dsInfo)
        .map( res => res.json())
  }
  getNotarias(){
      var dsInfo = this.ruta+'getNotarias';
      return this.http.get(dsInfo)
        .map( res => res.json())
  }
  
  getPlanesProfesional(){
      var dsInfo = this.ruta+'getPlanesProfesional';
      return this.http.get(dsInfo)
        .map( res => res.json())     
  }
  setCompraPlanProfesionalApp(datos){
      var dsInfo = this.ruta+'setCompraPlanProfesionalApp';
      return this.http.post(dsInfo, datos, this.getheaders())
        .map( res => res.json())
  }
  
  // listado historiales 
  getDetallePendientes(user,token){
      var dsInfo = this.ruta+'getDetallePendientes/'+user+'/'+token;
      return this.http.get(dsInfo)
      .map( res => res.json())
  }

  getHistorialPreguntasContestadas(user,token){
      var dsInfo = this.ruta+'getHistorialPreguntasContestadas/'+user+'/'+token+'/0/10';
      return this.http.get(dsInfo)
      .map( res => res.json())
  }

  getHistorialPreguntasDirectas(user,token){
      var dsInfo = this.ruta+'getHistorialPreguntasDirectas/'+user+'/'+token+'/0/10';
      return this.http.get(dsInfo)
      .map( res => res.json())
  }

  getHistorialdeCotizacionesProfesional(user,token){
      var dsInfo = this.ruta+'getHistorialdeCotizacionesProfesional/'+user+'/'+token+'/0/10';
      return this.http.get(dsInfo)
      .map( res => res.json())
  }

  //ARCHIVOS
  getArchivosDisponiblesProfesional(user,token){
      var dsInfo = this.ruta+'getArchivosDisponiblesProfesional/'+user+'/'+token+'/0/10';
      return this.http.get(dsInfo)
        .map( res => res.json())
  }

  //examenes
  getExamenesDisponiblesProfesional(user,token){
      var dsInfo = this.ruta+'getExamenesDisponiblesProfesional/'+user+'/'+token+'/0/10';
      return this.http.get(dsInfo)
        .map( res => res.json())
  }

  getDetalleExameneProfesional(user,token,id){
      var dsInfo = this.ruta+'getDetalleExameneProfesional/'+user+'/'+token+'/'+id;
      return this.http.get(dsInfo)
        .map( res => res.json())
  }
  setExamenProfesional(datos){
      var dsInfo = this.ruta+'setExamenProfesional';
      return this.http.post(dsInfo, datos, this.getheaders())
        .map( res => res.json())
  }
  getHistorialExamenesProfesional(user,token){
      var dsInfo = this.ruta+'getHistorialExamenesProfesional/'+user+'/'+token+'/0/10';
      return this.http.get(dsInfo)
        .map( res => res.json())
  }
  getHistorialRecargasProfesionalApp(user,token){
      var dsInfo = this.ruta+'getHistorialRecargasProfesionalApp/'+user+'/'+token;
      return this.http.get(dsInfo)
        .map( res => res.json())
  }
  getHistorialGananciasProfesionalApp(user,token){
      var dsInfo = this.ruta+'getHistorialGananciasProfesionalApp/'+user+'/'+token;
      return this.http.get(dsInfo)
        .map( res => res.json())
  }
  getHistorialCobrosProfesionalApp(user,token){
      var dsInfo = this.ruta+'getHistorialGananciasProfesionalApp/'+user+'/'+token;
      return this.http.get(dsInfo)
        .map( res => res.json())
  }
  getCarnetProfesional(user,token){
      var dsInfo = this.ruta+'getCarnetProfesional/'+user+'/'+token;
      return this.http.get(dsInfo)
        .map( res => res.json())
  }


}
