import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ModalmsgPage } from './modalmsg';

@NgModule({
  declarations: [
    ModalmsgPage,
  ],
  imports: [
    IonicPageModule.forChild(ModalmsgPage),
  ],
})
export class ModalmsgPageModule {}
