import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';

/**
 * Generated class for the ModalmsgPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-modalmsg',
  templateUrl: 'modalmsg.html',
})
export class ModalmsgPage {

  public datos : any;
  constructor(public viewCtrl: ViewController, public navCtrl: NavController, public navParams: NavParams) {
    this.datos = {};
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ModalmsgPage');
    this.datos = this.navParams.get('datos');
    console.log(this.datos);
  }

  regresar(){
    const data = '0';
    this.viewCtrl.dismiss(data).catch(() => {});
    
  }

  gotoPerfil()
  {
    const data = 'PlanesPage';
    this.viewCtrl.dismiss(data).catch(() => {});
    
  }
}
