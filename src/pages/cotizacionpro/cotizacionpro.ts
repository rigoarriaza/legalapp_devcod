import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, AlertController, LoadingController } from 'ionic-angular';

import { ServiceProvider } from '../../providers/service/service';
import { AuthProvider } from "../../providers/auth/auth";

@IonicPage()
@Component({
  selector: 'page-cotizacionpro',
  templateUrl: 'cotizacionpro.html',
})
export class CotizacionproPage {

  public idcotizacion: any;
  public datos:any;
  constructor( private authProvider: AuthProvider,public loadingCtrl: LoadingController, public alertCtrl: AlertController,public toastCtrl: ToastController,public ds: ServiceProvider,public navCtrl: NavController, public navParams: NavParams) {
    // this.data = this.navParams.get('pregunta');
    this.datos = {};
    // console.log(this.data);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CotizacionproPage');

    this.datos = this.navParams.get('pregunta');    
     console.log(this.datos);
    var token = this.authProvider.getToken() ; 
    var user = this.authProvider.getProfesional(); 

    this.ds.getDetalleCotizacion(user,token,this.datos.idpreguntad).subscribe( data=> {
        

          if (data.errorCode==0){
            this.datos = data.msg;
            // this.respuestas = data.msg.detalle_r;
          }
          console.log(data);
        });
  }

  responder(){

    this.datos['idcotizacion'] = this.datos['idpreguntad']; 
    this.datos['estado'] = 1; 
    this.datos['token_session'] = this.authProvider.getToken();  
    this.datos['idprofesional']= this.authProvider.getProfesional(); ; 
    
    console.log(this.datos);


    if(this.datos['respuesta']==undefined || this.datos['respuesta']=='' ){
      //error pregunta vacia
      let toast = this.toastCtrl.create({
              message: `Ingrese su respuesta`,
              duration: 2500,
              position: 'top'
          });
      toast.present();
    }else{
      let loader = this.loadingCtrl.create({
          content: "Enviando..."
      });
      loader.present();
      //asegurar envio de pregunta
      let confirm = this.alertCtrl.create({
      title: '¿Ha finalizado su respuesta?',
      message: 'Recuerde que no podrá realizar cambios una vez enviada su respuesta. Asegurese que su respuesta contiene todo lo necesario.',
      buttons: [
        {
          text: 'No, editar',
          handler: () => {
            console.log('Disagree clicked');
            loader.dismiss();
          }
        },
        {
          text: 'Si, enviar',
          handler: () => {
            
            
               this.ds.postRespuestaCotizacion(this.datos).subscribe( data=> {
                console.log(data);
                loader.dismiss();
                let alert = this.alertCtrl.create({
                  title: '¡Respuesta enviada!',
                  subTitle: 'Tu respuesta ha sido enviada',
                  buttons: [{
                      text: 'Aceptar',
                      handler: data => {
                        this.navCtrl.popToRoot();
                      }
                    }]
                });
                alert.present();
              });
            
            
            
          }
        }
      ]
    });
    confirm.present();

         
    }
  }
}
 