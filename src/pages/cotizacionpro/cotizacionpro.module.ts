import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CotizacionproPage } from './cotizacionpro';

import { PipesModule } from '../../pipes/pipes.module';
@NgModule({
  declarations: [
    CotizacionproPage,
  ],
  imports: [
    IonicPageModule.forChild(CotizacionproPage),PipesModule
  ],
  exports: [
    CotizacionproPage
  ]
})
export class CotizacionproPageModule {}
