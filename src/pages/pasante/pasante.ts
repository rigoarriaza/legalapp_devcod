import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';


import {ServiceProvider} from '../../providers/service/service';

@IonicPage()
@Component({
  selector: 'page-pasante',
  templateUrl: 'pasante.html',
})
export class PasantePage {

  public pasantes: any;
  constructor(public ds: ServiceProvider,public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PasantePage');

      this.ds.getPasantias().subscribe( data=> {
      
        if (data.errorCode==0){
          this.pasantes = data.msg;
        }
        console.log(data);
      });
  }

}
