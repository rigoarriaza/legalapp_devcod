import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PasantePage } from './pasante';

@NgModule({
  declarations: [
    PasantePage,
  ],
  imports: [
    IonicPageModule.forChild(PasantePage),
  ],
  exports: [
    PasantePage
  ]
})
export class PasantePageModule {}
