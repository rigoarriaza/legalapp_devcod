import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { XpaisPage } from './xpais';

@NgModule({
  declarations: [
    XpaisPage,
  ],
  imports: [
    IonicPageModule.forChild(XpaisPage),
  ],
})
export class XpaisPageModule {}
