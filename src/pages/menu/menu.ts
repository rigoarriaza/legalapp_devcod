import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Nav, App, ActionSheetController, Events } from 'ionic-angular';
import { IntroPage } from "../intro/intro";
import { AuthProvider } from "../../providers/auth/auth";
// import { TimelinePage } from "../timeline/timeline";

/**
 * Generated class for the MenuPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-menu',
  templateUrl: 'menu.html',
})
export class MenuPage {
   
  user = '';
  pages = [];
  public userTipo;
  public n_usuario;
  public n_foto='';

  @ViewChild(Nav) nav:Nav;
  
  constructor(public events: Events,private authProvider: AuthProvider,public actionSheetCtrl: ActionSheetController, public navCtrl: NavController, public navParams: NavParams, private appCtrl: App) {

     events.subscribe('user:fotou', (foto) => {
      this.n_foto = foto;
    });
}

  ionViewDidEnter() {
    this.n_foto = this.authProvider.getFotoPro(); 
  }

  ionViewWillEnter() {

    // let userApp = localStorage.getItem('legal_userApp');
    // userApp = JSON.parse(userApp);
    this.n_foto = this.authProvider.getFotoPro(); 
   
    if(this.authProvider.isUsuario()){
      this.pages=[
        { title: 'Inicio', page: 'TimelinePage', icon: 'ios-home-outline'},
        { title: 'Mis preguntas', page: 'HistorialPage', icon: 'ios-time-outline'},
        { title: 'Mi perfil', page: 'EditprofilePage', icon: 'ios-person-outline'},
        { title: 'Mi cuenta', page: 'WalletPage', icon: 'ios-card-outline'},
        { title: 'Notarías', page: 'MapaPage', icon: 'ios-pin-outline'},
        { title: 'Pasantías', page: 'PasantePage', icon: 'ios-school-outline'},
        { title: 'Términos y condiciones', page: 'TermsPage', icon: 'ios-document-outline'},
        { title: 'Salir', page: 'Salir', icon: 'ios-log-out-outline'},
        ];
        this.openPage('UpdatestatusPage');
    }else{
      this.pages=[
        { title: 'Inicio', page: 'HomeproPage', icon: 'ios-home-outline'},
        { title: 'Mis preguntas', page: 'HistorialproPage', icon: 'ios-time-outline'},
        { title: 'Archivos', page: 'FilelistPage', icon: 'ios-folder-outline'},
        { title: 'Mi perfil', page: 'EditperfilproPage', icon: 'ios-person-outline'},
        { title: 'Mi cuenta', page: 'WalletproPage', icon: 'ios-card-outline'},
        { title: 'Notarías', page: 'MapaPage', icon: 'ios-pin-outline'},
        { title: 'Términos y condiciones', page: 'TermsPage', icon: 'ios-document-outline'},
        { title: 'Salir', page: 'Salir', icon: 'ios-log-out-outline'},
        ];
        this.openPage('HomeproPage');
    }
  }

  openPage(page){
    if(page == 'Salir'){
      this.logout();
    }else{
      this.nav.push(page);
    }
  }

  logout(){
        let actionSheet = this.actionSheetCtrl.create({
        title: '¿Seguro que quieres cerrar sesión?',
        buttons: [
          {
            text: 'Cerrar Sesión',
            role: 'destructive',
            handler: () => {
              
              this.authProvider.logout();
                        
              this.appCtrl.getRootNav().setRoot(IntroPage);
            }
          },{
            text: 'Cancelar',
            role: 'cancel',
            handler: () => {
              console.log('Cancel clicked');
            }
          }
        ]
      });
      actionSheet.present();
    
  }
}
