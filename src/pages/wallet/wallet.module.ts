import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { WalletPage } from './wallet';

import { PipesModule } from '../../pipes/pipes.module';

@NgModule({
  declarations: [
    WalletPage,
  ],
  imports: [
    IonicPageModule.forChild(WalletPage),
    PipesModule
  ],
  exports: [
    WalletPage
  ]
})
export class WalletModule {}
