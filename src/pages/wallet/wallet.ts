import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, ViewController } from 'ionic-angular';
import { ServiceProvider } from "../../providers/service/service";
import { AuthProvider } from "../../providers/auth/auth";

// import { RecargaPage } from '../recarga/recarga';

@IonicPage()
@Component({
  selector: 'page-wallet',
  templateUrl: 'wallet.html',
})
export class WalletPage {
  public saldo:any;
  public userApp:any;
  public datos: any;
  public datosDebito: any;
  public profile: string = "in";
  constructor(private authProvider: AuthProvider, public ds: ServiceProvider, public navCtrl: NavController, public navParams: NavParams,public modalCtrl: ModalController,public viewCtrl: ViewController) {
  
    this.userApp = JSON.parse(localStorage.getItem('legal_userApp')); 
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Wallet');
    this.saldo= this.authProvider.getSaldoUsuario(); 

    this.ds.getDetallePagosUsuario(this.authProvider.getUsuario(),this.authProvider.getToken()).subscribe( data=> { 
      console.log(data);
      if (data.errorCode==0){
        this.datos = data.msg;
      }
    }); 

    this.ds.getHistorialDebitosUsuarioApp(this.authProvider.getUsuario(),this.authProvider.getToken()).subscribe( data=> { 
      console.log(data);
      if (data.errorCode==0){
        this.datosDebito = data.msg;
      }
    });  
  }

  gotowalletdetails(pago) {
    let modal = this.modalCtrl.create('Walletdetails',{datos: pago, tipo: this.profile});
    modal.present();
  }
  recarga(){
    

    this.navCtrl.push('RecargaPage');
  }

  sumar(d1, d2){
    let total = parseInt(d1) + parseInt(d2);
    return total;
  }
}
