import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,AlertController, ToastController, LoadingController } from 'ionic-angular';

import { ServiceProvider } from '../../providers/service/service';
import { AuthProvider } from "../../providers/auth/auth";

@IonicPage()
@Component({
  selector: 'page-updatestatus',
  templateUrl: 'updatestatus.html',
})
export class UpdatestatusPage {

  data: {};
  public n_foto='';
  public paginaOrigen = false;

  constructor(private authProvider: AuthProvider, public loadingCtrl: LoadingController, public toastCtrl: ToastController,public alertCtrl: AlertController, public ds: ServiceProvider, public navCtrl: NavController, public navParams: NavParams) {
    this.data = {};
    this.data['p_contenido'] = '';



    this.data['p_titulo'] = navParams.get('titulo');
    if( navParams.get('detalle') != undefined){
      this.data['p_contenido'] = navParams.get('detalle');
    }else{
      this.data['p_contenido'] = '';
    }

    if( navParams.get('origenPage') != undefined){
      this.paginaOrigen = true;
    }
    
    //verificar si viene para modifcar
    let pregunta = navParams.get('pregunta');
    if( pregunta != undefined){
      console.log(pregunta);
      this.data = pregunta;
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad UpdatestatusPage');
    this.n_foto = this.authProvider.getFotoPro();
    
  }

  preguntar(){
    // this.data['p_titulo'] = "Pregunta Express";
    this.data['iduserapp'] = this.authProvider.getUsuario(); 
    this.data['token_session'] = this.authProvider.getToken(); 
    this.data['p_privacidad']=0;

    if(this.data['p_privacidadform']==true){
      this.data['p_privacidad']=1;
    }

    console.log(this.data);
    if(this.data['p_contenido']==undefined || this.data['p_contenido']=='' || this.data['p_titulo']==undefined || this.data['p_titulo']=='' ){
      //error pregunta vacia
      let toast = this.toastCtrl.create({
              message: `Ingrese el título y detalle de su consulta.`,
              duration: 5000,
              position: 'top'
          });
      toast.present();
    }else{

      let confirm = this.alertCtrl.create({
      title: '¿Haz finalizado su pregunta?',
      message: 'Recuerde que no podrá realizar cambios una vez enviada su pregunta. Asegúrese que contiene todo lo necesario.',
      buttons: [
        {
          text: 'No, editar',
          handler: () => {        
            console.log('Disagree clicked');
          }
        },
        {
          text: 'Si, enviar',
          handler: () => {
            let loader = this.loadingCtrl.create({
              content: "Enviando...",
              dismissOnPageChange: true,
            });
            loader.present();

            //verificar si es para modificar o nueva
            if(this.data['id']!= undefined){
              //modificar p. express
              this.data['idpreguntae']=this.data['id'];
              console.log('modificada');
              this.ds.postModificarPreguntaExpress(this.data).subscribe( data=> {
                loader.dismiss();
                if (data.errorCode==0){
                  let alert = this.alertCtrl.create({
                    title: '¡Pregunta modificada!',
                    subTitle: 'Tu pregunta ha sido modificada. <br>                    Recuerda que para que la pregunta sea visible, confirma tu saldo y en breve un abogado te responderá.',
                    buttons: [
                      {
                          text: 'Pagar pregunta',
                          handler: () => {  
                            this.navCtrl.popToRoot();      
                            this.navCtrl.push('WalletPage');
                            
                          }
                      },
                      {
                        text: 'Aceptar',
                        handler: data => {
                          this.navCtrl.popToRoot();
                          
                        }
                      }]
                  });
                  alert.present();
                }else{
                  let alert = this.alertCtrl.create({
                    title: 'Error en el envio!',
                    subTitle: 'Tu pregunta no se puedo enviar: '+ data.errorMessage,
                    buttons: [{
                        text: 'Aceptar',
                        handler: data => {
                          
                        }
                      }]
                  });
                  alert.present();
                }


              });   
            }else{
              //guardar nuevapregunta express
              this.ds.postPreguntaExpress(this.data).subscribe( data=> {
                loader.dismiss();
                if (data.errorCode==0){
                  console.log(data);
                  if(this.authProvider.getSaldoUsuario()>0){
                    let alert = this.alertCtrl.create({
                      title: '¡Pregunta enviada!',
                      subTitle: 'Tu pregunta ha sido enviada a la comunidad de abogados. <br>                    Para que la pregunta sea visible, confirma tu saldo y en breve un abogado te responderá. Tienes saldo disponible, ¿quieres pagar tu pregunta ahora?',
                      buttons: [
                        {
                            text: 'Pagar pregunta',
                            handler: () => {  
                              let pago = {};
                              pago['iduserapp'] = this.authProvider.getUsuario(); 
                              pago['token_session'] = this.authProvider.getToken(); 
                              pago['idpreguntae']=data.msg;
                              this.ds.setPaymentPreguntaExpress(pago).subscribe( data=> {
                                if (data.errorCode==0){ //exitoso
                                  let alert2 = this.alertCtrl.create({
                                    title: '¡Pregunta pagada!',
                                    subTitle: 'Tu pregunta ha sido enviada a la comunidad de abogados. <br>                    En breve un abogado te responderá.',
                                    buttons: [
                                      {
                                          text: 'Mis preguntas',
                                          handler: () => {  
                                            this.navCtrl.popToRoot();      
                                            this.navCtrl.push('HistorialPage');
                                          }
                                      },
                                      {
                                        text: 'Aceptar',
                                        handler: data => {
                                          this.navCtrl.popToRoot();
                                        }
                                      }]
                                  });
                                  alert2.present();
                                }else if (data.errorCode==15){ //insuficiente saldo
                                  let alert2 = this.alertCtrl.create({
                                    title: '¡Saldo insuficiente!',
                                    subTitle: 'Tu saldo actual no es suficiente para pagar la pregunta.',
                                    buttons: [
                                      {
                                          text: 'Pagar pregunta',
                                          handler: () => {  
                                            this.navCtrl.popToRoot();      
                                            this.navCtrl.push('RecargaPage');
                                          }
                                      },
                                      {
                                        text: 'Aceptar',
                                        handler: data => {
                                          this.navCtrl.popToRoot();
                                        }
                                      }]
                                  });
                                  alert2.present();
                                }else{ //error
                                  let alert2 = this.alertCtrl.create({
                                    title: '¡Error en pago!',
                                    subTitle: 'Se ha presentado un inconveniente. Intenta de nuevo o contacta con nuestro equipo.',
                                    buttons: [
                                      {
                                        text: 'Aceptar',
                                        handler: data => {
                                          this.navCtrl.popToRoot();
                                        }
                                      }]
                                  });
                                  alert2.present();
                                }
                              });
                              // this.navCtrl.popToRoot();      
                              // this.navCtrl.push('RecargaPage');
                            }
                        },
                        {
                          text: 'Ir a mis preguntas',
                          handler: data => {
                              this.navCtrl.popToRoot();      
                              this.navCtrl.push('HistorialPage');
                          }
                        }]
                    });
                    alert.present();
                  }else{
                    let alert = this.alertCtrl.create({
                      title: '¡Pregunta enviada!',
                      subTitle: 'Tu pregunta ha sido enviada a la comunidad de abogados. <br>                    Para que la pregunta sea visible, confirma tu saldo y en breve un abogado te responderá.',
                      buttons: [
                        {
                            text: 'Pagar pregunta',
                            handler: () => {  
                              this.navCtrl.popToRoot();      
                              this.navCtrl.push('RecargaPage');
                            }
                        },
                        {
                          text: 'Aceptar',
                          handler: data => {
                            this.navCtrl.popToRoot();
                          }
                        }]
                    });
                    alert.present();
                  }

                }else{
                  let alert = this.alertCtrl.create({
                    title: 'Error en el envio!',
                    subTitle: 'Tu pregunta no se puedo enviar: '+ data.errorMessage,
                    buttons: [{
                        text: 'Aceptar',
                        handler: data => {
                          
                        }
                      }]
                  });
                  alert.present();
                }


              });   
            }
          }
        }]
    });

    confirm.present();
      
    }

    
  }

  change() {
    // get elements
    var element   = document.getElementById('p_contenido');
    var textarea  = element.getElementsByTagName('textarea')[0];

    // set default style for textarea
    textarea.style.minHeight  = '0';
    textarea.style.height     = '0';

    // limit size to 96 pixels (6 lines of text)
    var scroll_height = textarea.scrollHeight;
    if(scroll_height > 260)
      scroll_height = 260;

    // apply new style
    element.style.height      = scroll_height + "px";
    textarea.style.minHeight  = scroll_height + "px";
    textarea.style.height     = scroll_height + "px";
  }

  cerrar(){
    this.navCtrl.push('TimelinePage');
  }

}
