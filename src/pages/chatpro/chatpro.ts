import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, PopoverController, ModalController, AlertController, ToastController, LoadingController } from 'ionic-angular';

import { ServiceProvider } from '../../providers/service/service';
import { AuthProvider } from "../../providers/auth/auth";
import moment from 'moment'; 

@IonicPage()
@Component({
  selector: 'page-chatpro',
  templateUrl: 'chatpro.html',
})
export class ChatproPage {

   data: {};
  public preguntadet: any;
  public respuestas: any;
  public datos: any;
  public tipo: any;
  public n_foto: any;
  public token: any;
  public user:any;

  public formDisabled:boolean = true;
  public tiempoRestante:any;

  constructor(private authProvider: AuthProvider,public loadingCtrl: LoadingController, public toastCtrl: ToastController,public alertCtrl: AlertController, public ds: ServiceProvider,public modalCtrl: ModalController, public navCtrl: NavController, public navParams: NavParams,public popoverCtrl:PopoverController) {
    this.data = {};
    this.preguntadet = {};
    this.respuestas = [];
    this.n_foto = this.authProvider.getFotoPro();

    this.token = this.authProvider.getToken();
    this.user = this.authProvider.getProfesional();
  
}

  ionViewDidLoad() {
    console.log('ionViewDidLoad ChatproPage');
    
     this.tipo = this.navParams.get('tipo')
     this.datos = this.navParams.get('pregunta');    
     
     if(this.tipo == 1){
        this.datos = this.navParams.get('pregunta'); 

        this.ds.getDetallePreguntaContestada(this.user,this.token,this.datos.idpreguntae).subscribe( data=> {
        

          if (data.errorCode==0){
            this.datos = data.msg;
            this.datos.idpreguntae = data.msg.id;
            this.respuestas = data.msg.p_respuesta;
          
            if(this.datos.p_estado == 2  || this.datos.p_estado == 4  || this.datos.p_estado == 6  ){
              this.formDisabled = false;
                
                var a = moment(this.datos.p_fechaTomada);
                var b = moment();
                this.tiempoRestante  = b.diff(a,  'hours');

                if(this.tiempoRestante > 15){
                  // this.formDisabled = true;
                  this.tiempoRestante = "Tu usuario estará bloqueado hasta que respondas todas las réplicas pendientes";
                }else{
                  this.formDisabled = false;
                  this.tiempoRestante = "Te quedan " + (15 - this.tiempoRestante) + " horas para contestar.";
                }  
            }else{
              this.formDisabled = true;
            }      
          }

          
          
          console.log(data);
        });          
     }else{
        console.log(this.datos);
        
        this.ds.getDetallePreguntaDirectaProfesional(this.user,this.token,this.datos.idpreguntad).subscribe( data=> {
        

          if (data.errorCode==0){
            this.datos = data.msg;
            this.respuestas = data.msg.detalle_r;

            if(this.datos.p_estado == 5 ){
              this.formDisabled = true;
            }else{
              var fecha = "";
              if(this.respuestas.lenght >0){
                fecha = this.respuestas[this.respuestas.lenght-1].datetime;
              }else{
                fecha = this.datos.p_datetimepago;
              }
              var a = moment(fecha);
              var b = moment();
              this.tiempoRestante  = b.diff(a,  'hours');

              if(this.tiempoRestante > 15){
                  // this.formDisabled = true;
                  this.tiempoRestante = "Tu usuario estará bloqueado hasta que respondas todas las réplicas pendientes";
                  // this.tiempoRestante = "Esta pregunta ha expirado en su tiempo de respuesta. El sistema ha cerrado la pregunta y en minutos quedará en estado No Contestada."
                  
              }else{
                  this.formDisabled = false;
                  this.tiempoRestante = "Te quedan " + (15 - this.tiempoRestante) + " horas para contestar.";
              }  
              // this.formDisabled = false;
            }
          
          }
          console.log(this.datos);
        });
     }
  }

  responder(){
 
    let loader = this.loadingCtrl.create({
        content: "Enviando..."
    });
    loader.present();

    this.data['idpreguntae'] = this.datos.idpreguntae; 
    this.data['token_session'] = this.authProvider.getToken(); 
    this.data['idprofesional']= this.authProvider.getProfesional();    
    this.data['iduserapp'] = this.datos.iduserapp; 
              
    console.log(this.data);


    if(this.data['respuesta']==undefined || this.data['respuesta']=='' ){
      loader.dismiss();
      //error pregunta vacia
      let toast = this.toastCtrl.create({
              message: `Ingrese su respuesta`,
              duration: 2500,
              position: 'top'
          });
      toast.present();
    }else{
      //asegurar envio de pregunta
      let confirm = this.alertCtrl.create({
      title: '¿Has finalizado tu respuesta?',
      message: 'Recuerda que no podrás realizar cambios una vez enviada tu respuesta. Asegúrate que contiene todo lo necesario.',
      buttons: [
        {
          text: 'No, editar',
          handler: () => {
            loader.dismiss();
            console.log('Disagree clicked');
          }
        },
        {
          text: 'Si, enviar',
          handler: () => {
            
            if(this.tipo ==1 ){
              this.ds.postRespuestaPreguntaExpress(this.data).subscribe( data=> {
                
                if(data.errorCode== 0){
                  loader.dismiss();
                  let alert = this.alertCtrl.create({
                    title: '¡Respuesta enviada!',
                    subTitle: 'Tu respuesta ha sido enviada',
                    buttons: [{
                        text: 'Aceptar',
                        handler: data => {
                          this.navCtrl.popToRoot();
                        }
                      }]
                  });
                  alert.present();
                }else{
                  loader.dismiss();
                  let alert = this.alertCtrl.create({
                    title: 'Error al guardar',
                    subTitle: 'Error: '+ data.errorMessage,
                    buttons: [{
                        text: 'Aceptar',
                        handler: data => {
                          this.navCtrl.popToRoot();
                        }
                      }]
                  });
                  alert.present();
                }

              }); 
            }else{
              this.data['idpdirecta'] = this.datos.idpreguntad; 
              this.data['r_detalle'] = this.data['respuesta'];
              console.log(this.data);
              this.ds.postRespuestaPreguntaDirecta(this.data).subscribe( data=> {
                console.log(data);
                loader.dismiss();
                let alert = this.alertCtrl.create({
                  title: '¡Respuesta enviada!',
                  subTitle: 'Tu respuesta ha sido enviada',
                  buttons: [{
                      text: 'Aceptar',
                      handler: data => {
                        this.navCtrl.popToRoot();
                      }
                    }]
                });
                alert.present();
              });
            }
            
            
          }
        }
      ]
    });
    confirm.present();

         
    }
  }

   ionViewWillEnter() {
    let tabs = document.querySelectorAll('.show-tabbar');
    if (tabs !== null) {
      Object.keys(tabs).map((key) => {
        tabs[key].style.display = 'none';
      });
    } // end if
  }

  ionViewDidLeave() {
    let tabs = document.querySelectorAll('.show-tabbar');
    if (tabs !== null) {
      Object.keys(tabs).map((key) => {
        tabs[key].style.display = 'flex';
      });
    } // end if
  }

  messageattach(myEvent){
    let popover = this.popoverCtrl.create('Messageattach');
    popover.present({
      ev:myEvent
    })
  }

  changetextarea() {
    // get elements
    var element   = document.getElementById('messageInputBox');
    var textarea  = element.getElementsByTagName('textarea')[0];

    // set default style for textarea
    textarea.style.minHeight  = '50px';
    textarea.style.height     = '0';

    // limit size to 96 pixels (6 lines of text)
    var scroll_height = textarea.scrollHeight;
    if(scroll_height > 160)
      scroll_height = 160;

    // apply new style
    element.style.height      = scroll_height + "px";
    textarea.style.minHeight  = scroll_height + "px";
    textarea.style.height     = scroll_height + "px";
  }

}
