import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ChatproPage } from './chatpro';

import { PipesModule } from '../../pipes/pipes.module';
@NgModule({
  declarations: [
    ChatproPage,
  ],
  imports: [
    IonicPageModule.forChild(ChatproPage),PipesModule
  ],
  exports: [
    ChatproPage
  ]
})
export class ChatproPageModule {}
