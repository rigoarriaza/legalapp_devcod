import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { ServiceProvider } from "../../providers/service/service";
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { AuthProvider } from "../../providers/auth/auth";

/**
 * Generated class for the RecargaPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-recarga',
  templateUrl: 'recarga.html',
})
export class RecargaPage {

  public paso : any;
  public datos : any;
  public userApp : any;
  public carnet: any;
  constructor(private auth: AuthProvider, private iab: InAppBrowser, public ds: ServiceProvider,public alertCtrl: AlertController,public navCtrl: NavController, public navParams: NavParams) {
    this.paso =1;
    this.datos = {};
    this.userApp = JSON.parse(localStorage.getItem('legal_userApp')); 

    console.log(this.userApp);
  }

  ionViewDidEnter() {
    console.log('ionViewDidLoad RecargaPage');
    this.datos = this.navParams.get('plan');

    console.log(this.datos);
    // debugger;
  

    this.verificarCarnet();
  }

  verificarCarnet(){
    if(this.auth.isUsuario()){
      this.ds.getCarnetUsuario(this.auth.getUsuario(), this.auth.getToken()).subscribe(data =>{
        // debugger;

        if (data.errorCode==0){
            this.carnet = data.msg;
        }else{
                let alert = this.alertCtrl.create({
                              title: 'Actualiza tus datos',
                              subTitle: 'Necesitamos actualizar tu CI para poder realizar un pago.',
                              buttons: [{
                                  text: 'Cancelar',
                                  handler: data => {
                                    //
                                  }
                              },
                                {
                                  text: 'Ir a Configuración',
                                  handler: data => {
                                    this.navCtrl.push('EditprofilePage');
                                  }
                                }]
                });
                alert.present();
          this.carnet = '0';
        }
      });
    }else{ // si es profesional
      this.ds.getCarnetProfesional(this.auth.getProfesional(), this.auth.getToken()).subscribe(data =>{
        // debugger;
        if (data.errorCode==0){
            this.carnet = data.msg;
        }else{
                let alert = this.alertCtrl.create({
                              title: 'Actualiza tus datos',
                              subTitle: 'Necesitamos que actualices tu CI para poder realizar un pago.',
                              buttons: [{
                                  text: 'Cancelar',
                                  handler: data => {
                                    //
                                  }
                              },
                                {
                                  text: 'Ir a Configuración',
                                  handler: data => {
                                    this.navCtrl.push('EditperfilproPage');
                                  }
                                }]
                });
                alert.present();
          this.carnet = '0';
        }
      });
    }
  }

  recarga(step){

    if(this.carnet == '0'){
              let alert = this.alertCtrl.create({
                            title: 'Actualiza tus datos',
                            subTitle: 'Necesitamos actualizar tu CI para poder realizar un pago.',
                            buttons: [{
                                text: 'Cancelar',
                                handler: data => {
                                  //
                                }
                            },
                              {
                                text: 'Ir a Configuración',
                                handler: data => {
                                  if(this.auth.isUsuario()){
                                    this.navCtrl.push('EditprofilePage');
                                  }else{
                                    this.navCtrl.push('EditperfilproPage');
                                  }
                                  
                                }
                              }]
               });
               alert.present();
    }else{
      // debugger;
      if(step == 3){ //pago en banco
        //notificar el registro de pre-compra
        let msj = "";
        if(this.userApp['tipo_user'] == 2){ //profesional
          msj = 'Al elegir pago en banco nuestro sistema genera un pago pendiente, el cual con la sola presentación de tu cédula de identidad puedes dirigirte a cualquiera de los puntos de pago';
        }else{
          msj = 'Estimado usuario, puedes pagar en cualquiera de las sucursales del Banco Fassil. Solicita al cajero pagar el servicio de Legal App para preguntas Express o preguntas Directas. Tienes que presentar tu cédula de identidad.';
        }
        let confirm = this.alertCtrl.create({
              title: 'Pago en Banco',
              message: msj ,
              buttons: [
                {
                  text: 'No, regresar',
                  handler: () => {
                    console.log('Disagree clicked');
                  }
                },
                {
                  text: 'ACEPTAR',
                  handler: () => {
                    if(this.userApp['tipo_user'] == 2){ //profesional
                      this.datos['idprofesional'] = this.userApp['idprofesional'];
                      this.datos['token_session'] = this.userApp['token_session'];

                      console.log(this.datos);
                      this.ds.setCompraPlanProfesionalApp(this.datos).subscribe( data=> {
                          if (data.errorCode==0){
                              let alert = this.alertCtrl.create({
                              title: 'Registro realizado!',
                              subTitle: 'Ahora ya puedes acercarte a cualquier punto de pago!',
                              buttons: [{
                                  text: 'Aceptar',
                                  handler: data => {
                                    this.navCtrl.popToRoot();
                                  }
                                }]
                            });
                            alert.present();
                          }else{
                            let alert = this.alertCtrl.create({
                              title: 'Error',
                              subTitle: 'Tu pago no ha sido generado: '+data.errorMessage,
                              buttons: [{
                                  text: 'Aceptar',
                                  handler: data => {
                                    this.navCtrl.pop();
                                  }
                                }]
                            });
                            alert.present();
                          }
                      }); 

                    }
                  
                  }
                }
              ]
            });
        confirm.present();

      }else if(step == 2){ // pago con tarjeta
      
        if(this.userApp['tipo_user'] == 2){ //profesional

          this.datos['idprofesional'] = this.userApp['idprofesional'];
          this.datos['token_session'] = this.userApp['token_session'];

          console.log(this.datos);
          this.ds.setCompraPlanProfesionalApp(this.datos).subscribe( data=> {
            if (data.errorCode==0){
                let target = "_self";
                let url = 'http://multipago.bo/service/legal_app/external/start/'+this.carnet+'/'+this.userApp['tipo_user'];
                console.log(url);
                const browser = this.iab.create(url,target);

            }else{
                let alert = this.alertCtrl.create({
                              title: 'Error',
                              subTitle: 'Tu pago no ha sido generado: '+data.errorMessage,
                              buttons: [{
                                  text: 'Aceptar',
                                  handler: data => {
                                    this.navCtrl.pop();
                                  }
                                }]
                });
                alert.present();
            }
          }); 
        }else{
            let target = "_self";
            let url = 'http://multipago.bo/service/legal_app/external/start/'+this.carnet+'/'+this.userApp['tipo_user'];
            console.log(url);            
            const browser = this.iab.create(url,target);
        }

      }else{
        this.paso= step;
      }

    }

  }

  pagado(){
    this.navCtrl.popToRoot();
  }
}
