import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController, ModalController } from 'ionic-angular';

import {ServiceProvider} from '../../providers/service/service';
// import { Badge } from '@ionic-native/badge';
import { LocalNotifications } from '@ionic-native/local-notifications';
import { AuthProvider } from "../../providers/auth/auth";

@IonicPage()
@Component({
  selector: 'page-homepro',
  templateUrl: 'homepro.html',
})
export class HomeproPage {

  public userApp: any;
  public express: any; 
  public pendientes: any;
  public cotizaciones: any;
  public estado: any;
  // constructor( public modalCtrl: ModalController, private menu: MenuController, public ds: ServiceProvider,public navCtrl: NavController, public navParams: NavParams) {
  constructor(private authProvider: AuthProvider, private localNotifications: LocalNotifications, public modalCtrl: ModalController, private menu: MenuController, public ds: ServiceProvider,public navCtrl: NavController, public navParams: NavParams) {
    this.pendientes = {};
    this.userApp = JSON.parse(localStorage.getItem('legal_userApp')); 
  }

  ionViewDidEnter() {
    this.menu.swipeEnable(true);

    this.estado = this.userApp['u_estadoverificacion'];
    
  }

  async registrarPermiso(){
    try {

      // let hasPermition = await this.badge.hasPermission();
      // if(!hasPermition){
      //    let permission = await this.badge.requestPermission();
      //    console.log(permission);
      //   //  alert(permission);
      // }
      // alert(hasPermition);
    } catch (error) {
      
    }
  }
  
  async setBadge(bdg: number){
    try {
          // let badges = await this.badge.set(bdg);
          // console.log(badges);
          //  alert(badges);
        } catch (err) {
          
        }
  }

  ionViewWillEnter() {
    
   
    this.ds.getInformacionProfesionalApp(this.userApp['idprofesional'],this.userApp['token_session']).subscribe( data=> {
      this.userApp['plan'] = data.msg.user_plan;
      this.userApp['u_ganancia'] = data.msg.u_ganancia;
      localStorage.setItem('legal_userApp', JSON.stringify(this.userApp));
      console.log (this.userApp);
      console.log (data);
    });
    console.log(this.userApp);
    this.registrarPermiso();
    
    this.ds.getDetallePendientes(this.userApp['idprofesional'],this.userApp['token_session']).subscribe( data=> {
      
        if (data.errorCode==0){
          this.pendientes = data.msg;
        }
        
        //configurar notificaciones locales
        this.localNotifications.cancelAll();

        let year = new Date().getFullYear();
        let month = new Date().getMonth();
        let day = new Date().getDate();
        let tomorrow = new Date().getDate() + 1;
        
//         var today = moment();
// var tomorrow = moment(today).add(1, 'days');
        // debugger;
        let time1 = new Date(year, month, day, 8, 30, 0, 0);
        let time2 = new Date(year, month, day, 12, 0, 0, 0);
        let time3 = new Date(year, month, tomorrow, 8, 30, 0, 0);
        let time4 = new Date(year, month, tomorrow, 12, 0, 0, 0);
        this.localNotifications.schedule([{
                id: 1,
                text: 'Ingresa a Preguntas Express y verifica si hay preguntas de la comunidad para contestar.',
                data: 'Legal App',
                // firstAt: new Date(new Date().getTime() + 3600),
                trigger: {at: new Date(time1)},
                // every: 'day',
                badge: 1,
                // wakeup: true,
        }
        ,{ 
                id: 2,
                text: 'Recuerda ingresar a Mis preguntas y verificar si los usuarios te piden réplica.',
                data: 'Legal App',
                // firstAt: new Date(new Date().getTime() + 3600),
                trigger: {at: new Date(time2)},
                // every: 'minute',
                badge: 1,
                // wakeup: true,
        },{
                id: 3,
                text: 'Ingresa a Preguntas Express y verifica si hay preguntas de la comunidad para contestar.',
                data: 'Legal App',
                // firstAt: new Date(new Date().getTime() + 3600),
                trigger: {at: new Date(time3)},
                // every: 'day',
                badge: 1,
                // wakeup: true,
        }
        ,{ 
                id: 4,
                text: 'Recuerda ingresar a Mis preguntas y verificar si los usuarios te piden réplica.',
                data: 'Legal App',
                // firstAt: new Date(new Date().getTime() + 3600),
                trigger: {at: new Date(time4)},
                // every: 'minute',
                badge: 1,
                // wakeup: true,
        }
        ]);
        
    });
    
  }

  gotoLista(tipo){

    this.navCtrl.push("ListservicePage",{tipo:tipo})
    //estado  y verificacion de correo
    // if(this.userApp['u_estado'] == 2 ){
    //   //mensaje
    //   let msj = {};
    //   msj['titulo'] = "Acceso restringido";
    //   // if(this.userApp['u_estadoverificacion'] == 0){
    //   //   msj['cuerpo'] = "Su correo no ha sido verficado. Por favor ingrese a su cuenta y verifique su bandeja de entrada.";
    //   // }else{
    //     msj['cuerpo'] = "Su cuenta esta pendiente de ser verficada por los administradores.";
    //   // }
      
    //   msj['opt'] = "";

    //   let modal = this.modalCtrl.create('ModalmsgPage',{datos: msj});
    //   modal.present();     

    // // }else if(this.userApp.plan.id_plan == null){
    // //   //mensaje
    // //   let msj = {};
    // //   msj['titulo'] = "Compra tu plan";
    // //   msj['cuerpo'] = "Suscríbete a un plan para poder ver las preguntas de los usuarios. Ve a tu perfil y revisa los planes disponibles";
    // //   msj['opt'] = "";

    // //   let modal = this.modalCtrl.create('ModalmsgPage',{datos: msj});
    // //   modal.present();  
    
    // }else{
    //   this.navCtrl.push("ListservicePage",{tipo:tipo})
    // }
    
  }

  gotoPasantia(){

    this.navCtrl.push("PasantePage");

    // if( this.authProvider.getEstado() == 1 ){
    //   if(this.authProvider.getPlanProfesional() == null){
    //     //mensaje
    //     let msj = {};
    //     msj['titulo'] = "Adquiere tu plan Premium";
    //     msj['cuerpo'] = "Obtén un plan premium para poder acceder a las pasantías que Legal App tiene para la comunidad de abogados";
    //     msj['opt'] = "1";

    //     let modal = this.modalCtrl.create('ModalmsgPage',{datos: msj});
    //     modal.present(); 
    //     modal.onDidDismiss((data)=>{
    //       console.log(data);
    //       if(data != '0'){
    //         this.navCtrl.push(data);
    //       }
        
    //     }); 
    //   }else{
    //     this.navCtrl.push("PasantePage");
      
    //   }
    // }else {
    //   //mensaje
    //   let msj = {};
    //   msj['titulo'] = "Acceso restringido";
    //   msj['cuerpo'] = "Su cuenta esta pendiente de ser verficada por los administradores.";
    //   msj['opt'] = "0";

    //   let modal = this.modalCtrl.create('ModalmsgPage',{datos: msj});
    //   modal.present();
    //   modal.onDidDismiss((data)=>{
    //     console.log(data);
    //     if(data != '0'){
    //       this.navCtrl.push(data);
    //     }
      
    //   });
        
    // }
  }

  gotoHistorialPro(){
    this.navCtrl.push("HistorialproPage");
  }
  gotoExamen(){
    if( this.authProvider.getEstado() == 2 ){
      if(this.authProvider.getPlanProfesional() == null){
        //mensaje
        let msj = {};
        msj['titulo'] = "Adquiere tu plan Premium";
        msj['cuerpo'] = "Obtén un plan premium para poder acceder a los archivos que Legal App tiene para la comunidad de abogados";
        msj['opt'] = "";

        let modal = this.modalCtrl.create('ModalmsgPage',{datos: msj});
        modal.present();  
      }else{
        this.navCtrl.push("ExamenlistPage");
      
      }
    }else {
      //mensaje
      let msj = {};
      msj['titulo'] = "Adquiere tu plan Premium";
      msj['cuerpo'] = "Su cuenta esta pendiente de ser verficada por los administradores.";
      msj['opt'] = "";

      let modal = this.modalCtrl.create('ModalmsgPage',{datos: msj});
      modal.present();  
    }
    // else{
    //   //mensaje
    //   let msj = {};
    //   msj['titulo'] = "Correo no verificado";
    //   if(this.userApp['u_estadoverificacion'] == 0){
    //     msj['cuerpo'] = "Su correo no ha sido verficado. Por favor ingrese a su cuenta y verifique su bandeja de entrada.";
    //   }else{
    //     msj['cuerpo'] = "Su cuenta esta pendiente de ser verficada por los administradores.";
    //   }msj['opt'] = "";

    //   let modal = this.modalCtrl.create('ModalmsgPage',{datos: msj});
    //   modal.present();     
    // }
    
  }
  
  gotoNotification(){
    let modal = this.modalCtrl.create('NotificationListPage');
    modal.present();
  }
}
