import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { HomeproPage } from './homepro';

@NgModule({
  declarations: [
    HomeproPage,
  ],
  imports: [
    IonicPageModule.forChild(HomeproPage),
  ],
  exports: [
    HomeproPage
  ]
})
export class HomeproPageModule {}
