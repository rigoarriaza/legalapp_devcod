import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the WalletprodetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-walletprodetails',
  templateUrl: 'walletprodetails.html',
})
export class WalletprodetailsPage {

  public datos: any;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.datos = {};
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad WalletprodetailsPage');

    this.datos = this.navParams.get('detalle');
    console.log(this.datos);
  }

}
