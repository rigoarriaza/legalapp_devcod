import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { WalletprodetailsPage } from './walletprodetails';

@NgModule({
  declarations: [
    WalletprodetailsPage,
  ],
  imports: [
    IonicPageModule.forChild(WalletprodetailsPage),
  ],
})
export class WalletprodetailsPageModule {}
