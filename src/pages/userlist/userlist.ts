import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import {ServiceProvider} from '../../providers/service/service';
import {TimelinePage} from '../timeline/timeline';

@IonicPage()
@Component({
  selector: 'page-userlist',
  templateUrl: 'userlist.html',
})
export class Userlist {

  public especialidad: any;
  constructor(public ds: ServiceProvider,public navCtrl: NavController, public navParams: NavParams) {
     
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Userlist');

    this.ds.getEspecialidades().subscribe( data=> {
      
      if (data.errorCode==0){
        this.especialidad = data.msg;
      }else if(data.errorCode==7){
        //error de token
      }
       console.log(data);
    });
  }
  gotochat(){
    this.navCtrl.push('ChatPage')
  }
  sendEspecialidad(idespecialidad){
    this.navCtrl.push(TimelinePage,{especialidad:idespecialidad});
  }
}
