import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ServiceProvider } from "../../providers/service/service";

/**
 * Generated class for the WalletprolistPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-walletprolist',
  templateUrl: 'walletprolist.html',
})
export class WalletprolistPage {

  public opt : any;
  public userApp:any;
  public datos: any;
  constructor(public ds: ServiceProvider, public navCtrl: NavController, public navParams: NavParams) {
    this.opt = this.navParams.get("lista")
    this.userApp = JSON.parse(localStorage.getItem('legal_userApp')); 
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad WalletprolistPage');

    if(this.opt == 'ingreso'){
      this.ds.getHistorialGananciasProfesionalApp(this.userApp['idprofesional'],this.userApp['token_session']).subscribe( data=> { 
        console.log(data);
        if (data.errorCode==0){
          this.datos = data.msg;
        }
      }); 
    }else if(this.opt == 'abono'){
      this.ds.getHistorialCobrosProfesionalApp(this.userApp['idprofesional'],this.userApp['token_session']).subscribe( data=> { 
        console.log(data);
        if (data.errorCode==0){
          this.datos = data.msg;
        }
      }); 
    }else if(this.opt == 'suscripcion'){
      this.ds.getHistorialRecargasProfesionalApp(this.userApp['idprofesional'],this.userApp['token_session']).subscribe( data=> { 
        console.log(data);
        if (data.errorCode==0){
          this.datos = data.msg;
        }
      }); 
    }

  }

  gotoDetalle(dato){
    dato['opt'] = this.opt;
    this.navCtrl.push('WalletprodetailsPage',{detalle: dato});
  }

}
