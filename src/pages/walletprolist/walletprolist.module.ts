import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { WalletprolistPage } from './walletprolist';

@NgModule({
  declarations: [
    WalletprolistPage,
  ],
  imports: [
    IonicPageModule.forChild(WalletprolistPage),
  ],
})
export class WalletprolistPageModule {}
