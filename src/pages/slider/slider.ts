import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';


@IonicPage()
@Component({
  selector: 'page-slider',
  templateUrl: 'slider.html',
})
export class Slider {

  public tipo: any;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Slider');
    this.tipo =this.navParams.get('tipo');
  }
  gotologin(){
    // this.navCtrl.setRoot('Login')
    // this.navCtrl.setRoot(TabsPage)
  }
  gotohome(){
    this.navCtrl.setRoot('MenuPage');
   
  }
}
