import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';


import { ToastController } from 'ionic-angular';
import { ServiceProvider } from '../../providers/service/service';
import { AuthProvider } from "../../providers/auth/auth";

@IonicPage()
@Component({
  selector: 'page-examen',
  templateUrl: 'examen.html',
})
export class ExamenPage {

  public data: any;
  public status: number=0;
  public pregunta: string;
  public id: number=0;
  public calificacion: number=0;
  public listpreguntas: any;
  constructor(private authProvider: AuthProvider, public toastCtrl: ToastController, public ds: ServiceProvider, public viewCtrl: ViewController, public navCtrl: NavController, public navParams: NavParams) {
  
    this.data = this.navParams.get('examen');
    this.listpreguntas = {};
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ExamenPage');
    console.log(this.data);
  }

  iniciar(){
    this.status = 1;
    this.pregunta = this.data.detalle_preguntas[this.id].pregunta;
    
  }

  respuesta(valor){
    this.data.detalle_preguntas[this.id].respuesta = valor;
    console.log(this.data.detalle_preguntas.length);
    console.log(this.id);
    if(this.data.detalle_preguntas.length == (this.id+1)){
      //examen finalizado
      this.status = 2;

      //enviar preguntas
      this.listpreguntas['idprofesional'] = this.authProvider.getProfesional(); 
      this.listpreguntas['token_session'] = this.authProvider.getToken();
      this.listpreguntas['idexamen'] = this.data['idexamen'];
      this.listpreguntas['cuerpo_respuestas'] = this.data.detalle_preguntas;
      
      console.log(this.listpreguntas);
        this.ds.setExamenProfesional(this.listpreguntas).subscribe( data=> {
          if (data.errorCode==0){ //si no existe error
            console.log(data);
            this.calificacion = data.calificacion;
          }else{
            let toast = this.toastCtrl.create({
                    message: `Error, vuelva a intentarlo`,
                    duration: 2500,
                    position: 'bottom'
                });
            toast.present();
          }
        });      
    
    }else{
      this.id = this.id + 1;
      this.pregunta = this.data.detalle_preguntas[this.id].pregunta;  
      
    }
    console.log(this.data);
    
  }

  gotoX(){
    this.viewCtrl.dismiss();
  }

}
