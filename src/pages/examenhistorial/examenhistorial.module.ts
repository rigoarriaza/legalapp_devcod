import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ExamenhistorialPage } from './examenhistorial';

@NgModule({
  declarations: [
    ExamenhistorialPage,
  ],
  imports: [
    IonicPageModule.forChild(ExamenhistorialPage),
  ],
})
export class ExamenhistorialPageModule {}
