import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';


import { ServiceProvider } from '../../providers/service/service';
import { AuthProvider } from "../../providers/auth/auth";

@IonicPage() 
@Component({
  selector: 'page-examenhistorial',
  templateUrl: 'examenhistorial.html',
})
export class ExamenhistorialPage {

  public examen: any;
  constructor(private authProvider: AuthProvider, public ds: ServiceProvider, public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ExamenhistorialPage');

    var token = this.authProvider.getToken();
    var user = this.authProvider.getProfesional();
    console.log(user);
    this.ds.getHistorialExamenesProfesional(user,token).subscribe( data=> {
      
        if (data.errorCode==0){
          this.examen = data.msg;
        }
        console.log(data);
        //  console.log(this.preguntas);
    });
  }

}
