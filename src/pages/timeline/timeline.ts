import {Component} from '@angular/core';
import { IonicPage, NavController, NavParams, PopoverController, ModalController, MenuController, AlertController, App } from 'ionic-angular';


import { ServiceProvider } from '../../providers/service/service';
import { AuthProvider } from "../../providers/auth/auth";
import { IntroPage } from "../intro/intro";

@IonicPage()
@Component({
  selector: 'page-timeline',
  templateUrl: 'timeline.html',
})
export class TimelinePage {

  public abogados: any;
  public fakedatos: any;
  public arreglo = { "dato":
      [{"id":"1"},{"id":"2"},{"id":"3"},{"id":"4"},{"id":"5"},{"id":"6"}] 
    };
  public abogadosDestacados: any;
  public userApp:any;
  public inicio:number = 0;
  public cantidad: number = 4;
  public random: number = 1;
  public filtroEspecialidad: any;

  public destacados: boolean = true;
  public haydatos: boolean = false;

  constructor(private appCtrl: App, public alertCtrl: AlertController,private authProvider: AuthProvider, private menu: MenuController, public ds: ServiceProvider,public navCtrl: NavController, public navParams: NavParams,public popoverCtrl: PopoverController,public modalCtrl: ModalController) {
    this.fakedatos = this.arreglo.dato;
  }

  ionViewDidLoad() {
    this.cargarData();
  }
  ionViewDidLeave() {
    this.menu.swipeEnable(true);
  }
  cargarData(){

    this.random = this.getRandomSpan();
    console.log(this.random);
    
    this.filtroEspecialidad = this.navParams.get('especialidad');

    if(this.filtroEspecialidad !=undefined){
      this.ds.getAbogadosFiltroEspecialidad(this.authProvider.getUsuario(),this.authProvider.getToken(), this.navParams.get('especialidad'),this.inicio,this.cantidad,this.random).subscribe( data=> {
        
        if (data.errorCode==0){
          this.abogados = data.msg;
        }else if(data.errorCode==3){ //no hay datos
          // this.listabogados = false;
          this.haydatos= true;
        }else if(data.errorCode==7){
          //error de token
          this.alertCtrl.create({
                title: '¡Sesión ha expirado!',
                subTitle: 'Parece que tu sesión ha expirado, ingresa nuevamente con tus datos',
                buttons: [
                    {
                     text: 'Aceptar',
                     handler: data => {
                        this.authProvider.logout();
                        
                        this.appCtrl.getRootNav().setRoot(IntroPage);
                      }
                    }]
          }).present();
        }
        this.haydatos= true;
        console.log(data);
      });  

      console.log(this.navParams.get('especialidad'));
    
      this.ds.getAbogadosDestacadosFiltroEspecialidad(this.authProvider.getUsuario(),this.authProvider.getToken(), this.navParams.get('especialidad'),this.random).subscribe( data=> {
        
        if (data.errorCode==0){
          this.abogadosDestacados = data.msg;
        }else if(data.errorCode==3){
          this.destacados = false;
        }else if(data.errorCode==7){
          //error de token
        }
        console.log(data);
      });
    }else{
      this.ObtenerAbogadosInicio();
    }
  }

  ObtenerAbogadosInicio(){
      this.ds.getAbogados(this.authProvider.getUsuario(),this.authProvider.getToken(),this.inicio,this.cantidad,this.random).subscribe( data=> {
        // debugger;
        if (data.errorCode==0){
          this.abogados = data.msg;
        }else if(data.errorCode==3){ //no hay datos
          // this.listabogados = false;
        }else if(data.errorCode==7){
          //error de token
          this.alertCtrl.create({
                title: '¡Sesión ha expirado!',
                subTitle: 'Parece que tu sesión ha expirado, ingresa nuevamente con tus datos',
                buttons: [
                    {
                     text: 'Aceptar',
                     handler: data => {
                        this.authProvider.logout();
                        
                        this.appCtrl.getRootNav().setRoot(IntroPage);
                      }
                    }]
          }).present();

        }
        this.haydatos= true;
        console.log(data);
      });

      this.ds.getAbogadosDestacados(this.authProvider.getUsuario(),this.authProvider.getToken(),this.random).subscribe( data=> {
        
        if (data.errorCode==0){
          this.abogadosDestacados = data.msg;
        }else if(data.errorCode==3){
          this.destacados = false;
        }else if(data.errorCode==7){
          //error de token
        }
        console.log(data);
      });
  }

  getRandomSpan(){
	    return Math.floor((Math.random()*100)+1);
	};

  postpopover(myEvent) {
    let popover = this.popoverCtrl.create('PostpopoverPage');
    popover.present({
      ev: myEvent
    });
  }
  gotoEspecialidades(){
    this.navCtrl.push('Userlist')
  }
  gotoFavoritos(){
    this.navCtrl.push('FavoritosPage')
  }
  viewpost(){
    let modal = this.modalCtrl.create('PostviewmodalPage');
    modal.present();
  }
  gotoexpres(){
   this.navCtrl.push('UpdatestatusPage',{origenPage: true}) 
  }
  gotomapa(){
   this.navCtrl.push('MapaPage') 
  }
  gotoperfil(datos){
   
   this.navCtrl.push('PerfilPage', { abogado: datos}); 
  }

  doRefresh(refresher) {
    this.inicio = 0;
    this.destacados = true;
    this.haydatos = false;
    this.filtroEspecialidad = undefined;
    console.log('Begin async operation', refresher);
    this.random = this.getRandomSpan();
    this.ObtenerAbogadosInicio();

    setTimeout(() => {
      console.log('Async operation has ended');
      refresher.complete();
    }, 2000);
  }

  cargarMas(infiniteScroll) {
    
    console.log('Begin async operation');
    this.inicio = this.inicio+4;
    // this.fin+=3;
    setTimeout(() => {
      if(this.filtroEspecialidad!=undefined){
        this.ds.getAbogadosFiltroEspecialidad(this.authProvider.getUsuario(),this.authProvider.getToken(), this.navParams.get('especialidad'),this.inicio,this.cantidad,this.random).subscribe( data=> {
          console.log("==============")
          console.log(this.inicio)
          console.log(this.cantidad)
          console.log(data.msg)
          if (data.errorCode==0){
            for (let i = 0; i < data.msg.length; i++) {
              this.abogados.push( data.msg[i] );
            }
            // this.abogados.concat(data.msg);
          }else if(data.errorCode==7){
            //error de token
          }
        }); 
      }else{
        this.ds.getAbogados(this.authProvider.getUsuario(),this.authProvider.getToken(),this.inicio,this.cantidad,this.random).subscribe( data=> {
    
          console.log("==============")
          console.log(this.inicio)
          console.log(this.cantidad)
          console.log(data.msg)
          if (data.errorCode==0){
            for (let i = 0; i < data.msg.length; i++) {
              this.abogados.push( data.msg[i] );
            }
            // this.abogados.concat(data.msg);
          }else if(data.errorCode==7){
            //error de token
          }
          console.log(this.abogados);
        });
      }
      console.log('Async operation has ended');
      infiniteScroll.complete();
    }, 500);
  }
    
}
