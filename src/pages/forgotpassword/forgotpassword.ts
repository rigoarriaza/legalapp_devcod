import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { ServiceProvider } from "../../providers/service/service";


@IonicPage()
@Component({
  selector: 'page-forgotpassword',
  templateUrl: 'forgotpassword.html',
})
export class ForgotpasswordPage {
   
  public correo : string="";
  public tipo: any;
  constructor(public alertCtrl: AlertController,public ds: ServiceProvider, public navCtrl: NavController, public navParams: NavParams) {
    this.tipo = this.navParams.get('tipo');
}

  ionViewDidLoad() {
    console.log('ionViewDidLoad ForgotpasswordPage');
  }
  gotoConfirm(correo){
    //this.navCtrl.push('OtpconfirmPage')

    if( correo == "" || correo == undefined){
            let alert = this.alertCtrl.create({
                    title: 'Faltan datos',
                    subTitle: 'Ingresa una cuenta de correo.',
                    buttons: [{
                        text: 'Aceptar',
                        handler: data => {
                          //
                          this.correo = ""
                        }
                      }]
                  });
            alert.present();    
       //falta correo
    }else{

      if(this.tipo == 1){ //PERSONA NATURAL
        this.ds.getRecuperarPass(correo).subscribe( data=> {
        
          if (data.errorCode==0){
            let alert = this.alertCtrl.create({
                    title: 'Correo enviado',
                    subTitle: 'Hemos enviado a tu cuentas las instrucciones para recuperar tu contraseña.',
                    buttons: [{
                        text: 'Aceptar',
                        handler: data => {
                          //
                          this.correo = ""
                        }
                      }]
                  });
            alert.present();
          }else if(data.errorCode==3){
            let alert = this.alertCtrl.create({
                    title: 'Correo no encontrado',
                    subTitle: 'Hemos revisado en nuestra base y tu correo no esta registrado.',
                    buttons: [{
                        text: 'Aceptar',
                        handler: data => {
                          
                        }
                      }]
                  });
            alert.present();
          }
          
        });
      }
      else{
        this.ds.getRecuperarPassProfesional(correo).subscribe( data=> {
        
          if (data.errorCode==0){
            let alert = this.alertCtrl.create({
                    title: 'Correo enviado',
                    subTitle: 'Hemos enviado a tu cuentas las instrucciones para recuperar tu contraseña.',
                    buttons: [{
                        text: 'Aceptar',
                        handler: data => {
                          //
                          this.correo = ""
                        }
                      }]
                  });
            alert.present();
          }else if(data.errorCode==3){
            let alert = this.alertCtrl.create({
                    title: 'Correo no encontrado',
                    subTitle: 'Hemos revisado en nuestra base y tu correo no esta registrado.',
                    buttons: [{
                        text: 'Aceptar',
                        handler: data => {
                          
                        }
                      }]
                  });
            alert.present();
          }
          
        });     
      }

    }


  }
}
