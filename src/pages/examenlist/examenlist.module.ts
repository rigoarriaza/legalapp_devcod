import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ExamenlistPage } from './examenlist';

@NgModule({
  declarations: [
    ExamenlistPage,
  ],
  imports: [
    IonicPageModule.forChild(ExamenlistPage),
  ],
  exports: [
    ExamenlistPage
  ]
})
export class ExamenlistPageModule {}
