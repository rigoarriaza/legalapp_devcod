import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, AlertController } from 'ionic-angular';


import { ServiceProvider } from '../../providers/service/service';
import { AuthProvider } from "../../providers/auth/auth";

@IonicPage()
@Component({
  selector: 'page-examenlist',
  templateUrl: 'examenlist.html',
})
export class ExamenlistPage {

  public userApp: any;
  public examen: any;
  constructor(public alertCtrl: AlertController, public modalCtrl:ModalController, private authProvider: AuthProvider, public ds: ServiceProvider,public navCtrl: NavController, public navParams: NavParams) {
    this.userApp = JSON.parse(localStorage.getItem('legal_userApp')); 
  }

  ionViewDidLoad() {


    if( this.authProvider.getEstado() == 2 ){
      if(this.authProvider.getPlanProfesional() == null){
        //mensaje
        let msj = {};
        msj['titulo'] = "Adquiere tu plan Premium";
        msj['cuerpo'] = "Obtén un plan premium para poder acceder a los archivos que Legal App tiene para la comunidad de abogados";
        msj['opt'] = "1";

        let modal = this.modalCtrl.create('ModalmsgPage',{datos: msj});
        modal.present(); 
        modal.onDidDismiss((data)=>{
          console.log(data);
          if(data != '0'){
            this.navCtrl.push(data);
          }
        
        }); 
      }else{

        console.log('ionViewDidLoad ExamenlistPage');

        var token = this.authProvider.getToken();
        var user = this.authProvider.getProfesional();
        console.log(user);
        this.ds.getExamenesDisponiblesProfesional(user,token).subscribe( data=> {
          
            if (data.errorCode==0){
              this.examen = data.msg;
            }
            console.log(data);
            //  console.log(this.preguntas);
        });
      
      }
    }else {
      //mensaje
      let msj = {};
      msj['titulo'] = "Acceso restringido";
      msj['cuerpo'] = "Su cuenta esta pendiente de ser verficada por los administradores.";
      msj['opt'] = "0";

      let modal = this.modalCtrl.create('ModalmsgPage',{datos: msj});
      modal.present();
      modal.onDidDismiss((data)=>{
        console.log(data);
        if(data != '0'){
          this.navCtrl.push(data);
        }
      
      });
        
    }

  }

  gotoExamen(data){

    this.navCtrl.push('ExamendetailPage',{examen:data});
  }

  gotohistorial(){
    this.navCtrl.push('ExamenhistorialPage');
  }

}
