import { Component, LOCALE_ID, ChangeDetectorRef } from '@angular/core';
import { IonicPage, NavController, NavParams, PopoverController, ModalController, ToastController } from 'ionic-angular';


import { ServiceProvider } from '../../providers/service/service';
import { AuthProvider } from "../../providers/auth/auth";

@IonicPage()
@Component({
  selector: 'page-perfil',
  templateUrl: 'perfil.html',
    providers: [ { provide: LOCALE_ID, useValue: 'es-BO' } ],
})
export class PerfilPage {
   
 profile: string = "about";

 public datos: any;
 public id: any;
 public firma: any ; preguntas:any;
 public haydatos: boolean = false;
  
 data_fav: {};

  constructor(private cf: ChangeDetectorRef, private authProvider: AuthProvider, public toastCtrl: ToastController, public ds: ServiceProvider, public navCtrl: NavController,public modalCtrl: ModalController, public navParams: NavParams,public popoverCtrl: PopoverController) {
    // this.datos = this.navParams.get('abogado');
    // console.log(this.datos);
    this.data_fav = {};
    this.datos = {};
    this.datos.idplan = 0 ; //definir no mostrar botones hasta q se cargue la info
    this.firma = {}; this.preguntas={};
  }

 ionViewDidLoad() {
    console.log('ionViewDidLoad PerfilPage');

    var token = localStorage.getItem('legal_token'); 
    var user = localStorage.getItem('legal_user'); 
    this.id = this.navParams.get('abogado');
    
    console.log(this.id);
    
    this.ds.getPerfilProfesionalApp(this.authProvider.getUsuario(),this.authProvider.getToken(),this.id.idprofesional).subscribe( data=> {
        
        this.haydatos= true;
        if (data.errorCode==0){
          this.datos = data.msg[0];
          // this.firma = data.msg[0].firma_abogado;
          this.preguntas = data.msg[0].ultima_pregunta;
        }else if(data.errorCode==7){
          //error de token
        }
        console.log(this.datos);
        // console.log(this.firma);
      });
  }

  postpopover(myEvent) { 
    let popover = this.popoverCtrl.create('PostpopoverPage');
    popover.present({
      ev: myEvent
    });
  }

  profileedit(){
    this.navCtrl.push('EditprofilePage')
  }

  gotoConsulta(idprofesional, tipo){
    // let modal = this.modalCtrl.create('ConsultaPage');
    // modal.present();
    let nombrep = this.datos.p_nombre + ' ' + this.datos.p_apellido ;  
    console.log(idprofesional);
     this.navCtrl.push('ConsultaPage',{idprofesional: idprofesional,tipo:tipo, valor:this.datos.p_valorpregunta, profesional: nombrep});
  }
  gotoConsultaRel(idprofesional, tipo,titulo,detalle){
    let nombrep = this.datos.p_nombre + ' ' + this.datos.p_apellido ;  
    
     this.navCtrl.push('UpdatestatusPage',{idprofesional: idprofesional,tipo:tipo, titulo:titulo, detalle:detalle, valor: this.datos.p_valorpregunta, profesional:nombrep});
  }

  favorito(idprofesional){

    console.log(idprofesional);
    this.data_fav['idprofesional']=idprofesional;
    this.data_fav['iduserapp'] = this.authProvider.getUsuario(); 
    this.data_fav['token_session'] = this.authProvider.getToken(); 
    
    let msj = "";
    console.log(this.data_fav); 
    this.ds.setAbogadoFavorito(this.data_fav).subscribe( data=> {
      console.log(data);
      if(this.datos.favorito==1){
        this.datos.favorito = 0; 
        msj = "Abogado ya no esta en tu lista de favoritos";
      }else{
        this.datos.favorito = 1; 
        msj = "Abogado marcado como favorito";
      }
      let toast = this.toastCtrl.create({
          message: msj,
          duration: 5000,
          position: 'bottom'
      });
      toast.present();
    });

  }

  gotomapa(){
    // this.navCtrl.push('MapaproPage');
  }

  segmentChanged()
  {
    this.cf.detectChanges();
  }
}
