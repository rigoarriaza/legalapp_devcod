import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { HistorialproPage } from './historialpro';

import { PipesModule } from '../../pipes/pipes.module';
@NgModule({
  declarations: [
    HistorialproPage,
  ],
  imports: [
    IonicPageModule.forChild(HistorialproPage),PipesModule
  ],
  exports: [
    HistorialproPage
  ]
})
export class HistorialproPageModule {}
