import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';


import { ServiceProvider } from '../../providers/service/service';
import { AuthProvider } from "../../providers/auth/auth";

@IonicPage()
@Component({
  selector: 'page-historialpro',
  templateUrl: 'historialpro.html',
})
export class HistorialproPage {

  public preguntas: any;
  public preguntasDirectas: any;
  public preguntasCotizacion:any;
  tipopregunta: string = "exp";

  public token: any;
  public user:any;
  constructor(private authProvider: AuthProvider,public ds: ServiceProvider,public navCtrl: NavController, public navParams: NavParams) {
    this.token = this.authProvider.getToken();
    this.user = this.authProvider.getProfesional();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad HistorialproPage');

    this.cargarData();

  }

  cargarData(){
    
    this.ds.getHistorialPreguntasDirectas(this.user,this.token).subscribe( data=> {
      
      if (data.errorCode==0){
        this.preguntasDirectas = data.msg;
      }
       console.log(data);
    });
 
    //get Preguntas express
    this.ds.getHistorialPreguntasContestadas(this.user,this.token).subscribe( data=> {
      
      if (data.errorCode==0){
        this.preguntas = data.msg;
      }
       console.log(data);
    });    
    
    //get Preguntas cotizaciones
    this.ds.getHistorialdeCotizacionesProfesional(this.user,this.token).subscribe( data=> {
      
      if (data.errorCode==0){
        this.preguntasCotizacion = data.msg;
      }
       console.log(data);
    });
  }

  doRefresh(refresher) {
    console.log('Begin async operation', refresher);
    this.cargarData();

    setTimeout(() => {
      console.log('Async operation has ended');
      refresher.complete();
    }, 2000);
  }

  gotoPregunta(datos){
    this.navCtrl.push('ChatproPage', {pregunta:datos,tipo:1});
  }
  gotoPreguntaDirecta(datos){
      this.navCtrl.push('ChatproPage', {pregunta:datos,tipo:2});
    }

}
