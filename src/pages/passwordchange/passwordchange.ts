import { Component } from '@angular/core';
import {IonicPage, NavController, NavParams, ToastController, ViewController} from 'ionic-angular';

import {ServiceProvider} from '../../providers/service/service';

@IonicPage()
@Component({
  selector: 'page-passwordchange',
  templateUrl: 'passwordchange.html',
})
export class PasswordchangePage {

  data: {};
  public userApp:any;
  constructor(public ds: ServiceProvider, public navCtrl: NavController, public navParams: NavParams,private toastCtrl: ToastController,public viewCntrl:ViewController) {
    this.data = {};
    this.userApp = JSON.parse(localStorage.getItem('legal_userApp')); 
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PasswordchangePage');
  }

  passwordchangetoast(clave, clave2){

    var tipo = this.navParams.get('tipo');

    if(tipo == 'user'){
      
      this.data['idusuarioapp'] = this.userApp['iduserapp']; 
      this.data['token_session'] = this.userApp['token_session']; 
      this.data['password'] = clave; 
      console.log(this.data);
      //verificar si son iguales y no vacias
      if(clave=='' || clave==undefined || clave2=='' || clave2==undefined){
          let toast = this.toastCtrl.create({
            message: 'Error: ingrese ambas claves',
            duration: 3000,
            position: 'middle'
          });
          toast.present();
      }else if(clave != clave2){
          let toast = this.toastCtrl.create({
            message: 'Error: las contraseñas no son iguales.',
            duration: 3000,
            position: 'middle'
          });
          toast.present();
      }else{
        this.ds.setPasswordUsuario(this.data).subscribe( data=> {

          console.log(data);
          if (data.errorCode==0){ 
            let toast = this.toastCtrl.create({
              message: 'La contraseña se cambio correctamente',
              duration: 4000,
              position: 'middle'
            });

            toast.onDidDismiss(() => {
              this.navCtrl.pop();
            });

            toast.present();
          }else{
              let toast = this.toastCtrl.create({
              message: 'Error al guardar, intente más tarde.',
              duration: 3000,
              position: 'middle'
            });

            toast.onDidDismiss(() => {
              this.navCtrl.pop();
            });

            toast.present();
          }
        });
      }
    }else //PROFESIONAL
    {
      this.data['idprofesional'] = this.userApp['idprofesional']; 
      this.data['token_session'] = this.userApp['token_session']; 
      this.data['password'] = clave; 
      
      console.log(this.data);
      //verificar si son iguales y no vacias
      if(clave=='' || clave==undefined || clave2=='' || clave2==undefined){
          let toast = this.toastCtrl.create({
            message: 'Error: ingrese ambas claves',
            duration: 3000,
            position: 'middle'
          });
          toast.present();
      }else if(clave != clave2){
          let toast = this.toastCtrl.create({
            message: 'Error: las contraseñas no son iguales.',
            duration: 3000,
            position: 'middle'
          });
          toast.present();
      }else{
        this.ds.setPasswordProfesional(this.data).subscribe( data=> {

          console.log(data);
          if (data.errorCode==0){ 
            let toast = this.toastCtrl.create({
              message: 'La contraseña se cambio correctamente',
              duration: 4000,
              position: 'middle'
            });

            toast.onDidDismiss(() => {
              this.navCtrl.pop();
            });

            toast.present();
          }else{
              let toast = this.toastCtrl.create({
              message: 'Error al guardar, intente más tarde.',
              duration: 3000,
              position: 'middle'
            });

            toast.onDidDismiss(() => {
              this.navCtrl.pop();
            });

            toast.present();
          }
        });
      }
    }

  }
}
