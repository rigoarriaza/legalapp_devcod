import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { WalletproPage } from './walletpro';

@NgModule({
  declarations: [
    WalletproPage,
  ],
  imports: [
    IonicPageModule.forChild(WalletproPage),
  ],
  exports: [
    WalletproPage
  ]
})
export class WalletproPageModule {}
