import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ServiceProvider } from "../../providers/service/service";

@IonicPage()
@Component({
  selector: 'page-walletpro',
  templateUrl: 'walletpro.html',
})
export class WalletproPage {

  public userApp:any;
  public datos: any;
  public profile: string = "in";
  public saldo: any;
  public show: number= 0 ;
  constructor(public ds: ServiceProvider, public navCtrl: NavController, public navParams: NavParams) {
    this.userApp = JSON.parse(localStorage.getItem('legal_userApp')); 
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad WalletproPage');
    this.saldo = this.userApp['u_ganancia']; 
    this.ds.getHistorialRecargasProfesionalApp(this.userApp['idprofesional'],this.userApp['token_session']).subscribe( data=> { 
      console.log(data);
      if (data.errorCode==0){
        this.datos = data.msg;
      }
    }); 
  }

  gotoLista(opt){
    this.navCtrl.push('WalletprolistPage',{ lista: opt});
    // if(opt == 'ingreso'){
    //   this.navCtrl.push('WalletprolistPage',{ lista: opt});
    // }else if(opt == 'ingreso'){
    //   this.navCtrl.push('WalletprolistPage');
    // }else if(opt == 'suscripcion'){
    //   this.navCtrl.push('WalletprolistPage');
    // }
  }
  showdiv(){
    if(this.show == 0){
      this.show = 1;
    }else{
      this.show = 0;
    }
  }

}
