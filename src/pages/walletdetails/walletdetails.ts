import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,ViewController } from 'ionic-angular';

/**
 * Generated class for the Walletdetails page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-walletdetails',
  templateUrl: 'walletdetails.html',
})
export class Walletdetails {

  public datos: any;
  public tipo: any;
  constructor(public navCtrl: NavController, public navParams: NavParams,public viewCtrl: ViewController) {
    this.datos = {};
    this.tipo = this.navParams.get('tipo');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Walletdetails');

    this.datos = this.navParams.get('datos');

    console.log(this.navParams.get('datos'));
  }
  dismiss() {
    this.viewCtrl.dismiss();
  }
}
