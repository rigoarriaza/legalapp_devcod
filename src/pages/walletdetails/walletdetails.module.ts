import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Walletdetails } from './walletdetails';

import { PipesModule } from '../../pipes/pipes.module';
@NgModule({
  declarations: [
    Walletdetails,
  ],
  imports: [
    IonicPageModule.forChild(Walletdetails),
    PipesModule
  ],
  exports: [
    Walletdetails
  ]
})
export class WalletdetailsModule {}
