import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { GoogleMaps, GoogleMap, GoogleMapsEvent, GoogleMapOptions, MarkerOptions, Marker } from '@ionic-native/google-maps';
import { ServiceProvider } from "../../providers/service/service";
// import { Geolocation ,GeolocationOptions ,Geoposition ,PositionError } from '@ionic-native/geolocation'; 

/**
 AIzaSyDvIZr3UjuGsFKoUdWLb5kQHYkFd16mgbI
 sudo ionic cordova plugin add cordova-plugin-googlemaps --variable API_KEY_FOR_ANDROID="AIzaSyDvIZr3UjuGsFKoUdWLb5kQHYkFd16mgbI" --variable API_KEY_FOR_IOS="AIzaSyDvIZr3UjuGsFKoUdWLb5kQHYkFd16mgbI"
//  */
// declare var google;

@IonicPage()
@Component({
  selector: 'page-mapa',
  templateUrl: 'mapa.html'
}) 
export class MapaPage {

//     options : GeolocationOptions;
//   currentPos : Geoposition;
  // @ViewChild('map') mapElement: ElementRef;
  // map: any;
   map: GoogleMap;
   
//   constructor(private geolocation : Geolocation, public navCtrl: NavController, public navParams: NavParams) {
  public datos: any;
  constructor(public ds: ServiceProvider, public loadingCtrl: LoadingController, private googleMaps: GoogleMaps, public navCtrl: NavController, public navParams: NavParams) {
 
 }
  
 
  ionViewDidLoad() {
    console.log('ionViewDidLoad MapaPage');
    
    // this.getUserPosition();
    this.loadMap();

    // this.addMap( -17.783314,-63.182134);
  }

  loadMap() {

    let mapOptions: GoogleMapOptions = {
      camera: {
        target: {
          // lat: 43.0741904,
          // lng: -89.3809802
          lat: -17.783314,
          lng: -63.182134
        },
        zoom: 12,
        tilt: 30
      }
    };

    this.map = GoogleMaps.create('map_canvas', mapOptions);

    // // Wait the MAP_READY before using any methods.
    // let loader = this.loadingCtrl.create({
    //     content: "Verificando...",
    //     dismissOnPageChange: true,
    // });
    // loader.present();

    // Wait the MAP_READY before using any methods.
    this.map.one(GoogleMapsEvent.MAP_READY)
    .then(() => {
      // Now you can use all methods safely.
      this.cargarNotarias();
    })
    .catch(error =>{
      console.log(error);
    });

  }

  cargarNotarias(){
    this.ds.getNotarias().subscribe( data=> { 
        console.log(data);
        
        // debugger;
        if (data.errorCode==0){
          this.datos = data.msg;

          for (var index = 0; index < this.datos.length; index++) {

            let marker: Marker = this.map.addMarkerSync({
              title: this.datos[index].nombre,
              icon: "assets/icon/ts-map-pin.png",
              animation: 'DROP',
              position: {
                lat: parseFloat(this.datos[index].latitud),
                lng: parseFloat(this.datos[index].longitud)
              }
            });
 
          }
        }
    });
  }

   getUserPosition(){
    //   this.options = {
    //       enableHighAccuracy : true
    //   };

    // //      let latLng = new google.maps.LatLng(-34.9290, 138.6010);
 
    // // let mapOptions = {
    // //   center: latLng,
    // //   zoom: 15,
    // //   mapTypeId: google.maps.MapTypeId.ROADMAP
    // // }
 
    // // this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);

    //   this.geolocation.getCurrentPosition(this.options).then((pos : Geoposition) => {

    //       this.currentPos = pos;      
    //       console.log(pos);
    //       this.addMap(pos.coords.latitude,pos.coords.longitude);

    //   },(err : PositionError)=>{
    //       console.log("error : " + err.message);
    //   });
  }

  // addMap(lat,long){

  //     let latLng = new google.maps.LatLng(lat, long);

  //     let mapOptions = {
  //     center: latLng,
  //     zoom: 14,
  //     mapTypeId: google.maps.MapTypeId.ROADMAP
  //     }

  //     this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
  //     this.addMarker();

  // }
  addMarker(){

      // let marker = new google.maps.Marker({
      // map: this.map,
      // animation: google.maps.Animation.DROP,
      // position: this.map.getCenter()
      // });

      // let content = "<p>LegalApp !</p>";          
      // let infoWindow = new google.maps.InfoWindow({
      //   content: content
      // });

      // google.maps.event.addListener(marker, 'click', () => {
      //   infoWindow.open(this.map, marker);
      // });

      // let lista = []; 

      // this.ds.getNotarias().subscribe( data=> { 
      //   console.log(data);
        
      //   // debugger;
      //   if (data.errorCode==0){
      //     this.datos = data.msg;

      //     // for (var index = 0; index < this.datos.length; index++) {
      //     //   var element = { lat: parseFloat(this.datos[index].latitud), lng: parseFloat(this.datos[index].longitud )};
      //     //   lista.push(element);
      //     // }

      //     // for (var index = 0; index < lista.length; index++) {
      //     for (var index = 0; index < this.datos.length; index++) {

      //         var info = this.datos[index].nombre;
      //         let marker1 = new google.maps.Marker({
      //           position: new google.maps.LatLng(parseFloat(this.datos[index].latitud),parseFloat(this.datos[index].longitud)),
      //           map: this.map,
      //           icon: "assets/icon/ts-map-pin.png",
      //           animation: google.maps.Animation.DROP //bounce animation
      //         });

      //         // var info = this.datos[index].nombre;
                  

      //         (function(marker1, index, info2) {
      //                                 // add click event
      //             google.maps.event.addListener(marker1, 'click', function() {
      //                 let infowindow = new google.maps.InfoWindow({
      //                       content: info2 
      //                 });
      //                 infowindow.open(this.map, marker1);
      //             });
      //         })(marker1, index, info);
      //       }
      //   }
      // }); 
      //arreglo

      // let lista2 = [
      //   {lat: -17.8183686, lng: -63.182374900000006},
      //   {lat: -17.776556, lng: -63.181846},
      //   {lat: -17.779741, lng:  -63.175827}];

      //   console.log(lista);
      //   console.log(lista2);
      

  }

// if($obRecorrido){
// 						foreach ($obRecorrido AS $id => $array) {

// 				?>			
// 							var LatLon = {lat: <?php echo $array['latitud'];?>, lng: <?php echo $array['longitud'];?>};
							

// 							marker = new google.maps.Marker({
// 							  position: new google.maps.LatLng(LatLon),
// 							  map: map,
// 							  animation: google.maps.Animation.DROP //bounce animation
// 					          //custom pin icon icon: "http://www.google.com/favicon.ico"
					      
// 							});

// 							 // process multiple info windows
// 		                    (function(marker, i) {
// 		                        // add click event
// 		                        google.maps.event.addListener(marker, 'click', function() {
// 		                            infowindow = new google.maps.InfoWindow({
// 		                                content: "<?php echo $array['nombre'];?> " + "<?php echo $array['apellido'];?> <br> <?php echo $array['identificacion'];?> | Saldo: <?php echo $array['saldo'];?>"
// 		                            });
// 		                            infowindow.open(map, marker);
// 		                        });
// 		                    })(marker, i);
// 							i=i+1;
							


  //  loadMap(){
 
  //   let latLng = new google.maps.LatLng(-34.9290, 138.6010);
 
  //   let mapOptions = {
  //     center: latLng,
  //     zoom: 15,
  //     mapTypeId: google.maps.MapTypeId.ROADMAP
  //   }
 
  //   this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
 
  // }
}
