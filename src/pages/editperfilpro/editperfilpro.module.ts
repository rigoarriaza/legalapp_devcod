import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EditperfilproPage } from './editperfilpro';

import { PipesModule } from '../../pipes/pipes.module';
@NgModule({
  declarations: [
    EditperfilproPage,
  ],
  imports: [
    IonicPageModule.forChild(EditperfilproPage),PipesModule
  ],
  exports: [
    EditperfilproPage
  ]
})
export class EditperfilproPageModule {}
