import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, ActionSheetController, Events, ToastController } from 'ionic-angular';
import {Camera} from '@ionic-native/camera';

import { ServiceProvider } from '../../providers/service/service';
import { AuthProvider } from "../../providers/auth/auth";

@IonicPage()
@Component({
  selector: 'page-editperfilpro',
  templateUrl: 'editperfilpro.html',
})
export class EditperfilproPage {

  profile: string = "about";
  public data: any;
  public especialidad: any;
  public datosPlan: any;
  public userApp: any;  
  checkedItems:boolean[];

  public base64Image: string;
  public user: any;
  public token: any;
  constructor(public toastCtrl: ToastController,private authProvider: AuthProvider, public events: Events, public ds: ServiceProvider, public alertCtrl: AlertController, public navCtrl: NavController, public navParams: NavParams,public actionSheetCtrl: ActionSheetController, private camera: Camera) {
    this.data = {};
    this.datosPlan = {};
    // this.especialidad = {};
    this.base64Image = this.authProvider.getFotoProfesional();
    this.userApp = JSON.parse(localStorage.getItem('legal_userApp')); 
    
    this.user = this.authProvider.getProfesional();
    this.token= this.authProvider.getToken();
  }

  ionViewDidEnter() {
    this.base64Image =  this.authProvider.getFotoProfesional(); 
        // let alert = this.alertCtrl.create({
        //   title: 'imagen',
        //   subTitle: this.base64Image,
        //   buttons: [{
        //     text: 'Aceptar',
        //     handler: data => {}
        //   }]
        //  });
        // alert.present();   

  }
  ionViewWillEnter(){
    // this.base64Image =  this.authProvider.getFotoPro();
  }
  ionViewDidLoad() {
    // this.base64Image = this.authProvider.getFotoPro();
    //obtener informacion actual del profesional
    this.ds.getInformacionProfesionalApp(this.user,this.token).subscribe( data=> {
        console.log(data);
        if (data.errorCode==0){
          this.data = data.msg;
          this.datosPlan = data.msg.user_plan;

          //setear calificacion si aun no trae datos
          if(this.data['u_calificacion']== ''){
            this.data['u_calificacion']=0;
          }

          if(this.datosPlan.id_plan == null){
            this.datosPlan.id_plan = 0;
          }

          // obtener las especialidades y chequearlas
          this.ds.getEspecialidades().subscribe( data=> {
            for (var i = 0; i < data.msg.length; i++){
              data.msg[i].estado = false;
              for (var j = 0; j < this.data.user_especialidad.length; j++){
                if(this.data.user_especialidad[j].id == data.msg[i].id_especialidad ){
                  data.msg[i].estado = true;
                }        
              }

            } 
            this.especialidad = data.msg ;
            this.checkedItems = new Array(this.especialidad.length);
          });
        }else if(data.errorCode==7){
          //error de token
          this.navCtrl.popToRoot();
        }
        // this.datosPlan.n_especialidades = 3;
        console.log(this.data);
      });

  }
  
  suscripciones(){
    if(this.userApp['u_estado'] == 1){
        let alert = this.alertCtrl.create({
          title: 'Cuenta Pendiente',
          subTitle: "Tu cuenta aún esta pendiente de verificar. Completa los datos y se notificará tu habilitación por correo.",
          buttons: [{
            text: 'Aceptar',
            handler: data => {}
          }]
        });
        alert.present();        
    }else{
      this.navCtrl.push('PlanesPage');       
    }
  }
  cambiarclave(){
    this.navCtrl.push('PasswordchangePage',{tipo:'pro'});
  }
  
  showCheckbox() {
    let alert = this.alertCtrl.create();
    alert.setTitle('Elije tus especialidades:');

    this.ds.getEspecialidades().subscribe( data=> {

      for (var i = 0; i < data.msg.length; i++) {
        
          alert.addInput({
            type: 'checkbox',
            label: data.msg[i].e_nombre,
            value: data.msg[i].id_especialidad,
            checked: false
          });
      }

      alert.addButton('Cancelar');
      alert.addButton({
        text: 'Guardar',
        handler: data => {
          console.log('Checkbox data:', data);
          // this.testCheckboxOpen = false;
          // this.testCheckboxResult = data;
        }
      });
      alert.present();
    });

  }

  changeprofilepic() {
    
    this.data['idprofesional']=this.user;
    this.data['token_session']=this.token;

    let actionSheet = this.actionSheetCtrl.create({
      title: 'Elegir Método',
      buttons: [
        {
          text: 'Cámara',
          role: 'camera',
          handler: () => {
            this.camera.getPicture({
              destinationType: this.camera.DestinationType.DATA_URL,
              quality: 70,
              targetWidth: 1280,
              targetHeight: 1280,
              allowEdit: true,
              correctOrientation: true,
            }).then((imageData) => {
              // imageData is a base64 encoded string
              this.base64Image = "data:image/jpeg;base64," + imageData;

              //guardar foto
              // this.datos['p_foto']=this.base64Image;
              this.data['p_foto']=this.base64Image;
              this.ds.setModificarFotoProfesional(this.data).subscribe( data=> {
                //mensaje error o success
                this.events.publish('user:fotou', this.data['p_foto']);
                this.authProvider.setFoto(this.data['p_foto']);
                this.userApp['u_foto'] = this.base64Image;
                localStorage.setItem('legal_userApp',JSON.stringify(this.userApp));
                
                this.authProvider.setFotoPro(this.base64Image);

                // let alert = this.alertCtrl.create({
                //   title: 'imagen',
                //   subTitle: this.base64Image,
                //   buttons: [{
                //     text: 'Aceptar',
                //     handler: data => {}
                //   }]
                // });
                // alert.present(); 
              });

            }, (err) => {
              console.log(err);
            });
          }
        },{
          text: 'Galería',
          role: 'gallery',
          handler: () => {
            this.camera.getPicture({
              destinationType: this.camera.DestinationType.DATA_URL,
              quality: 70,
              targetWidth: 1280,
              targetHeight: 1280,
              // quality: 100,
              // targetWidth: 2000,
              // targetHeight: 2000,
              allowEdit: true,
              correctOrientation: true,
              sourceType: this.camera.PictureSourceType.SAVEDPHOTOALBUM
            }).then((imageData) => {
              // imageData is a base64 encoded string
              this.base64Image = "data:image/jpeg;base64," + imageData;

              //guardar foto
              // this.datos['p_foto']=this.base64Image;
              this.data['p_foto']=this.base64Image;
              this.authProvider.setFoto(this.base64Image);
              this.ds.setModificarFotoProfesional(this.data).subscribe( data=> {
                //mensaje error o success

                this.events.publish('user:fotou', this.data['p_foto']);
                // this.authProvider.setFoto(this.base64Image);
              
              });
              
            }, (err) => {
              console.log(err);
            });
          }
        },{
          text: 'Cancelar',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });
    actionSheet.present();
  }

  changeRegistroimg() {
    
 
    this.data['idprofesional']=this.user;
    this.data['token_session']=this.token;

    let actionSheet = this.actionSheetCtrl.create({
      title: 'Elegir Método',
      buttons: [
        {
          text: 'Cámara',
          role: 'camera',
          handler: () => {
            this.camera.getPicture({
              destinationType: this.camera.DestinationType.DATA_URL,
              quality: 70,
              targetWidth: 900,
              targetHeight: 900,
              allowEdit: true,
              correctOrientation: true,
            }).then((imageData) => {
              // imageData is a base64 encoded string
              // this.base64Image = "data:image/jpeg;base64," + imageData;

              //guardar foto
              this.data['documento']= "data:image/jpeg;base64," + imageData;
              this.ds.setImagenRequerimiento(this.data).subscribe( data=> {
                //mensaje error o success
                console.log(data);
                let toast = this.toastCtrl.create({
                      message: data.errorCode,
                      duration: 5500,
                      position: 'top'
                  });
                toast.present();
              });

            }, (err) => {
              console.log(err);
            });
          }
        },{
          text: 'Galería',
          role: 'gallery',
          handler: () => {
            this.camera.getPicture({
              destinationType: this.camera.DestinationType.DATA_URL,
              quality: 70,
              targetWidth: 900,
              targetHeight: 900,
              // quality: 100,
              // targetWidth: 2000,
              // targetHeight: 2000,
              allowEdit: true,
              correctOrientation: true,
              sourceType: this.camera.PictureSourceType.SAVEDPHOTOALBUM
            }).then((imageData) => {
              // imageData is a base64 encoded string
              // this.base64Image = "data:image/jpeg;base64," + imageData;

              //guardar foto
              this.data['documento']= "data:image/jpeg;base64," + imageData;
              this.ds.setImagenRequerimiento(this.data).subscribe( data=> {
                //mensaje error o success
                console.log(data);
              });
              
            }, (err) => {
              console.log(err);
            });
          }
        },{
          text: 'Cancelar',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });
    actionSheet.present();
  }

  chkEspecialidad(id_especialidad){
    // debugger;
    if(this.datosPlan.id_plan == 0 ){
      let alert = this.alertCtrl.create({
        title: 'Notificación',
        subTitle: "No puedes elegir especialidades. Para acceder a este beneficio, adquiere una cuenta Premium",
            buttons: [{
              text: 'Aceptar',
              handler: data => {
              
              }
            }]
          });
      alert.present();
    }else{

      //guardamos especialidad
      for (var i = 0; i < this.especialidad.length; i++) {
        if(this.especialidad[i].id_especialidad == id_especialidad){
          this.especialidad[i].estado = true;
          break;
        }
      }

    }
    // let x = 0;
    // for (var index = 0; index < this.especialidad.length; index++) {
    //   if(this.especialidad[index].estado == true){
    //     x++;
    //   }
    // }
    
    // if(x >= this.datosPlan.n_especialidades  ){
    //   //tu plan no permite mas selecciones
    //   let alert = this.alertCtrl.create({
    //     title: 'Notificación',
    //     subTitle: "No puedes elegir especialidades. Para acceder a este beneficio, adquiere una cuenta Premium",
    //         buttons: [{
    //           text: 'Aceptar',
    //           handler: data => {
              
    //           }
    //         }]
    //       });
    //   alert.present();
    // }else{
    //   //guardamos especialidad
    //   for (var i = 0; i < this.especialidad.length; i++) {
    //     if(this.especialidad[i].id_especialidad == id_especialidad){
    //       this.especialidad[i].estado = true;
    //       break;
    //     }
    //   }
    // }

  }

  actualizarDatos(){
    this.data['idprofesional']=this.authProvider.getProfesional();
    this.data['token_session']=this.authProvider.getToken();
    console.log(this.data);

    // debugger;
    //verificar si cconsulta <> 0
    if(this.data['u_valorpregunta']==0 || this.data['u_valorpregunta']==''){
        let alert = this.alertCtrl.create({
          title: 'Completar datos',
          subTitle: "Por favor ingrese el monto que cobrará por sus servicios de contestar cada pregunta directa.",
          buttons: [{
            text: 'Aceptar',
            handler: data => {
     
                localStorage.setItem('legal_userApp',JSON.stringify(this.userApp));
                        
                      }
                    }]
                });
                alert.present();    
    }else if(this.data['u_valorpregunta'] <35 || this.data['u_valorpregunta']>700){
        let alert = this.alertCtrl.create({
          title: 'Valor de servicios no aceptado',
          subTitle: "El monto máximo por los servicios en nuestra plataforma es de 700 Bs y el mínimo de 35 Bs",
          buttons: [{
            text: 'Aceptar',
            handler: data => {
             
                      }
                    }]
                });
                alert.present();    
    }else{

      this.ds.setModificarPerfilProfesional(this.data).subscribe( data=> {
        console.log(data);

        // debugger;
        let esp = {};
        let especialidades = [];
        let item= {};
        console.log(this.checkedItems);
        for (var i = 0; i < this.especialidad.length; i++) {
          if(this.especialidad[i].estado == true){
            item['especialidad_id'] = this.especialidad[i].id_especialidad;
            item['principal'] = 0;
            especialidades.push(item);
            item = {};
          }       
        }
        // for (var i = 0; i < this.checkedItems.length; i++) {
        //   if(this.checkedItems[i] == true){
        //     item['especialidad_id'] = i;
        //     item['principal'] = 0;
        //     especialidades.push(item);
        //     item = {};
        //   }       
        // } 
        
        esp['idprofesional'] =this.user;
        esp['token_session'] =this.token;
        esp['especialidades'] =especialidades;
        console.log(JSON.stringify(esp));
        let nombre = this.data['u_nombre']+' '+this.data['u_apellido'];
        this.events.publish('user:nombre', nombre);
                          
        
        //guardar especialidades
        this.ds.setModificarEspecialidadProfesional(esp).subscribe( data=> {
          console.log(data);
                let alert = this.alertCtrl.create({
                    title: 'Datos modificados',
                    subTitle: "Recuerde completar toda tu información para que tu perfil pueda ser activado.",
                    buttons: [{
                        text: 'Aceptar',
                        handler: data => {
                          this.navCtrl.last();
                          
                          localStorage.setItem('legal_userApp',JSON.stringify(this.userApp));
                          
                        }
                      }]
                  });
                  alert.present();
        });



      });
    }
  }
}
