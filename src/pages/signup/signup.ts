import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams,ActionSheetController, Events,MenuController} from 'ionic-angular';
// import {Camera} from '@ionic-native/camera';

import { ToastController, ModalController, LoadingController } from 'ionic-angular';

// import { Slider } from '../slider/slider';

import { ServiceProvider } from '../../providers/service/service';
import { AuthProvider } from "../../providers/auth/auth";

@IonicPage()
@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html',
  // providers : [Camera]
})
export class SignupPage {
  public base64Image: string;

   data : {};
   public tipo: any;
  constructor( private authProvider: AuthProvider, public loadingCtrl: LoadingController, public modalCtrl: ModalController, private menu: MenuController, public events: Events, public toastCtrl: ToastController,public ds: ServiceProvider,public navCtrl: NavController, public navParams: NavParams,public actionSheetCtrl: ActionSheetController ){
    this.base64Image = "assets/img/avatar.png";

    this.data = {};

    this.tipo = this.navParams.get('tipo');
    
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SignupPage');
  }
  ionViewDidEnter() {
    this.menu.swipeEnable(false);
  }

  // changeprofilepic() {
  //   let actionSheet = this.actionSheetCtrl.create({
  //     title: 'Choose Method',
  //     buttons: [
  //       {
  //         text: 'Camera',
  //         role: 'camera',
  //         handler: () => {
  //           this.camera.getPicture({
  //             destinationType: this.camera.DestinationType.DATA_URL,
  //             quality: 100,
  //             targetWidth: 2000,
  //             targetHeight: 2000,
  //             allowEdit: true,
  //             correctOrientation: true,
  //           }).then((imageData) => {
  //             // imageData is a base64 encoded string
  //             this.base64Image = "data:image/jpeg;base64," + imageData;
  //           }, (err) => {
  //             console.log(err);
  //           });
  //         }
  //       },{
  //         text: 'Gallery',
  //         role: 'gallery',
  //         handler: () => {
  //           this.camera.getPicture({
  //             destinationType: this.camera.DestinationType.DATA_URL,
  //             quality: 100,
  //             targetWidth: 2000,
  //             targetHeight: 2000,
  //             allowEdit: true,
  //             correctOrientation: true,
  //             sourceType: this.camera.PictureSourceType.SAVEDPHOTOALBUM
  //           }).then((imageData) => {
  //             // imageData is a base64 encoded string
  //             this.base64Image = "data:image/jpeg;base64," + imageData;
  //           }, (err) => {
  //             console.log(err);
  //           });
  //         }
  //       },{
  //         text: 'Cancel',
  //         role: 'cancel',
  //         handler: () => {
  //           console.log('Cancel clicked');
  //         }
  //       }
  //     ]
  //   });
  //   actionSheet.present();
  // }

  validarCarnet(carnet){
    if(carnet == undefined || carnet==""){ 
      return false;
    }else{
      if(isNaN(carnet)){
        //no es numero - verificar extranjero
        var x = carnet.length;
        if (carnet.substring(0,2) == 'E-' && carnet.substring(0,2) == 'e-' && !isNaN(carnet.substring(2,x))){
          //es carnet de extranjero
          return true;
        }else if( !isNaN(carnet.substring(0,x-2)) && carnet.substring(x-2,x-1) == '-'){
          //es carnet reposicion
          return true;
        }else{
          return false;
        }
      }else{
        return true;
      }
    }
    
  }

  registrar() {

     console.log(this.data);
    //limpiar correo
    this.data['correo'].trim();

    // if(this.validarCarnet(this.data['carnet'])){
      if(this.data['terms']== undefined || this.data['terms']==false){
        let toast = this.toastCtrl.create({
                message: `Antes de continuar, debe aceptar los términos y condiciones de uso.`,
                duration: 5000,
                position: 'top'
            });
        toast.present();
      }else{
        if(this.tipo == 1){ //PERSONA NATURAL
          if(this.data['nombre']== undefined || this.data['nombre']=="" || this.data['apellido']== undefined || this.data['apellido']=="" || this.data['correo']== undefined || this.data['correo']=="" || this.data['password']== undefined || this.data['password']==""){
            let toast = this.toastCtrl.create({
                    message: `Debe llenar todos los campos`,
                    duration: 5000,
                    position: 'top'
                });
            toast.present();
          }else{
            // if(this.data['password']== this.data['password2']){

              let loader = this.loadingCtrl.create({
                content: "Creando cuenta..."
              });
              loader.present();
              
              // verificar si hay conexion

              this.ds.postRegistro(this.data).subscribe( data=> {
                if (data.errorCode==0){ //si no existe error
                  loader.dismiss();
                  
                  //validadando datos
                  let loader2 = this.loadingCtrl.create({
                    content: "Validando datos..."
                  });
                  loader2.present();

                  //loguearse
                  this.authProvider.login(this.data['correo'],this.data['password']).then(success=>{
                    if(success=='0'){
                      this.navCtrl.push('Slider',{tipo:this.tipo});
                      loader2.dismiss();
                    }
                  }).catch(err =>{
                      loader2.dismiss()

                        let toast = this.toastCtrl.create({
                              message: `Error: `+err,
                              duration: 2500,
                              position: 'top'
                          });
                        toast.present();
                  }); 
             
                }else if (data.errorCode=='2'){
                  loader.dismiss();

                  let toast = this.toastCtrl.create({
                          message: data.errorMessage,
                          duration: 5000,
                          position: 'bottom'
                      });
                  toast.present();
                }else if (data.errorCode=='501'){
                  loader.dismiss();

                  let toast = this.toastCtrl.create({
                          message: data.errorMessage,
                          duration: 5000,
                          position: 'bottom'
                      });
                  toast.present();
                }else if (data.errorCode=='502'){
                  loader.dismiss();

                  let toast = this.toastCtrl.create({
                          message: data.errorMessage,
                          duration: 5000,
                          position: 'bottom'
                      });
                  toast.present();
                }else if (data.errorCode=='6'){
                  loader.dismiss();

                  let toast = this.toastCtrl.create({
                          message: "Cuenta creada pero no se puedo enviar correo. Consulte al administrador para su activación.",
                          duration: 5000,
                          position: 'bottom'
                      });
                  toast.present();
                }else{
                  loader.dismiss();

                  let toast = this.toastCtrl.create({
                          message: `Error, vuelva a intentarlo. `+data.errorMessage,
                          duration: 2500,
                          position: 'bottom'
                      });
                  toast.present();
                }
              });
            // }else{
            //   let toast = this.toastCtrl.create({
            //           message: `Las contraseñas no coinciden`,
            //           duration: 2500,
            //           position: 'top'
            //       });
            //   toast.present();
            // }

          }


        }else{ // PROFESIONAL
          if(this.data['nombre']== undefined || this.data['nombre']=="" || this.data['apellido']== undefined || this.data['apellido']=="" || this.data['correo']== undefined || this.data['correo']=="" || this.data['password']== undefined || this.data['password']==""){

            let toast = this.toastCtrl.create({
                    message: `Debe llenar todos los campos`,
                    duration: 2500,
                    position: 'top'
                });
            toast.present();
          }else{
            // if(this.data['password']== this.data['password2']){

              let loader = this.loadingCtrl.create({
                content: "Creando cuenta..."
              });
              loader.present();

              this.ds.postRegistroPro(this.data).subscribe( data=> {
                console.log(data);
                if (data.errorCode==0){ //si no existe error

                  this.authProvider.loginPro(this.data['correo'],this.data['password']).then(success=>{
                    if(success=='0'){
                        this.navCtrl.push('Slider',{tipo:this.tipo});
                        loader.dismiss();
                      }
                    }).catch(err =>{
                        loader.dismiss();

                          let toast = this.toastCtrl.create({
                                message: `Error: `+err,
                                duration: 2500,
                                position: 'top'
                            });
                          toast.present();
                    });
                  // this.ds.getLoginPro(this.data['correo'],this.data['password']).subscribe( data=> { 
        
                  //   console.log(data);
                  //   localStorage.setItem('legal_token',data.msg['token_session']); 
                  //   localStorage.setItem('legal_idprofesional',data.msg['idprofesional']); 
                  //   localStorage.setItem('legal_name_u',data.msg['u_nombre']+' '+data.msg['u_apellido']); 
                  //   localStorage.setItem('legal_foto_u', 'assets/img/avatar.png'); 

                  //   data.msg['legal_foto'] = 'assets/img/avatar.png';
                  //   data.msg['tipo_user'] = '2';
              
                  //   localStorage.setItem('legal_userApp',JSON.stringify(data.msg)); 
                  //   localStorage.setItem('saldo_pro','0'); 
                    
                  //   this.events.publish('user:created', this.tipo);

                  //   this.navCtrl.push('Slider',{tipo:this.tipo});
                  //   loader.dismiss();
                  // });

                  
                }else if (data.errorCode==1){
                   let toast = this.toastCtrl.create({
                          message: `Error, correo ya registrado`,
                          duration: 5000,
                          position: 'bottom'
                      });
                  toast.present(); 
                  loader.dismiss();                
                }else{
                  let toast = this.toastCtrl.create({
                          message: `Error, vuelva a intentarlo: `+ data.errorMessage,
                          duration: 2500,
                          position: 'bottom'
                      });
                  toast.present();
                  loader.dismiss();
                }
                
              });
            // }else{
            //   let toast = this.toastCtrl.create({
            //           message: `Las contraseñas no coinciden`,
            //           duration: 2500,
            //           position: 'top'
            //       });
            //   toast.present();
            // }
          }

          
        }
        
      
      }

    // }else{
    //     let toast = this.toastCtrl.create({
    //             message: `Formato de carnet de identidad no valido`,
    //             duration: 2500,
    //             position: 'top'
    //     });
    //     toast.present();      
      
    // }



    
    // this.navCtrl.push(Slider);
  }

  terminos(){
    this.navCtrl.push('TermsPage');
    // let modal = this.modalCtrl.create('TermsPage');
    // modal.present();
  }
}
