import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ActionSheetController, AlertController, Events } from 'ionic-angular';
import {Camera} from '@ionic-native/camera';


import { ServiceProvider } from '../../providers/service/service';
import { AuthProvider } from "../../providers/auth/auth";
@IonicPage()
@Component({
  selector: 'page-editprofile',
  templateUrl: 'editprofile.html',
  // providers : [Camera]
})
export class EditprofilePage { 

  public base64Image: string;
  data : {};
  public userApp:any;
   
  constructor(private authProvider: AuthProvider,public events: Events, public alertCtrl: AlertController,public ds: ServiceProvider,public navCtrl: NavController, public navParams: NavParams,public actionSheetCtrl: ActionSheetController,private camera: Camera ){
  // constructor(public alertCtrl: AlertController,public ds: ServiceProvider,public navCtrl: NavController, public navParams: NavParams,public actionSheetCtrl: ActionSheetController){
    this.base64Image = this.authProvider.getFotoPro();

    this.data = {};

    this.userApp = JSON.parse(localStorage.getItem('legal_userApp')); 
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad EditprofilePage');
    this.base64Image = this.authProvider.getFotoPro();
  }
  ionViewDidEnter() {
    this.base64Image =  this.authProvider.getFotoPro(); 
  }

  ionViewWillEnter() {
    this.data['u_nombre'] = this.userApp['u_nombre'];
    this.data['u_apellido'] = this.userApp['u_apellido'];
    this.data['u_celular'] = this.userApp['u_celular'];
    this.data['u_carnet'] = this.userApp['u_carnet'];
    let tabs = document.querySelectorAll('.show-tabbar');
    // if (tabs !== null) {
    //   Object.keys(tabs).map((key) => {
    //     tabs[key].style.display = 'none';
    //   });
    // } // end if
  }

  ionViewDidLeave() {
    // let tabs = document.querySelectorAll('.show-tabbar');
    // if (tabs !== null) {
    //   Object.keys(tabs).map((key) => {
    //     tabs[key].style.display = 'flex';
    //   });
    // } // end if
  }

  changeprofilepic() {
   
    this.data['iduserapp']=this.userApp['iduserapp'];
    this.data['token_session']=this.userApp['token_session'];

    let actionSheet = this.actionSheetCtrl.create({
      title: 'Elegir Metodo',
      buttons: [
        {
          text: 'Camara',
          role: 'camera',
          handler: () => {
            this.camera.getPicture({
              destinationType: this.camera.DestinationType.DATA_URL,
              quality: 70,
              targetWidth: 1280,
              targetHeight: 1280,
              allowEdit: true,
              correctOrientation: true,
            }).then((imageData) => {
              // imageData is a base64 encoded string
              this.base64Image = "data:image/jpeg;base64," + imageData;

              //guardar foto
              this.data['u_foto']=this.base64Image;
              
              this.ds.setModificarFotoUsuario(this.data).subscribe( data=> {
                //mensaje error o success
                console.log(data);
                this.events.publish('user:fotou', this.data['u_foto']);
                this.authProvider.setFoto(this.data['u_foto']);
              });

            }, (err) => {
              console.log(err);
            });
          }
        },{
          text: 'Galeria',
          role: 'gallery',
          handler: () => {
            this.camera.getPicture({
              destinationType: this.camera.DestinationType.DATA_URL,
              quality: 70,
              targetWidth: 1280,
              targetHeight: 1280,
              // quality: 100,
              // targetWidth: 2000,
              // targetHeight: 2000,
              allowEdit: true,
              correctOrientation: true,
              sourceType: this.camera.PictureSourceType.SAVEDPHOTOALBUM
            }).then((imageData) => {
              // imageData is a base64 encoded string
              this.base64Image = "data:image/jpeg;base64," + imageData;

              //guardar foto
              this.data['u_foto']=this.base64Image;
              
              this.ds.setModificarFotoUsuario(this.data).subscribe( data=> {
                //mensaje error o success
                this.events.publish('user:fotou', this.data['u_foto']);
                this.authProvider.setFoto(this.base64Image);
                console.log(data);
              });
              
            }, (err) => {
              console.log(err);
            });
          }
        },{
          text: 'Cancelar',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });
    actionSheet.present();
  }
  back(){
    this.navCtrl.pop()
  }

  actualizar(){
    var token = this.userApp['token_session']; 
    var user = this.userApp['iduserapp']; 
    
    this.data['iduserapp']=user;
    this.data['token_session']=token;
    this.data['u_foto']='';
    console.log(this.data);

    this.ds.setInformacionUsuarioApp(this.data).subscribe( data=> {
      console.log(data);
      let alert = this.alertCtrl.create({
                  title: 'Datos modificados',
                  subTitle: 'Gracias por actualizar tu información.',
                  buttons: [{
                      text: 'Aceptar',
                      handler: data => {
                        this.navCtrl.last();

                        this.userApp['u_nombre'] = this.data['u_nombre'] 
                        this.userApp['u_apellido'] = this.data['u_apellido'];
                        this.userApp['u_celular'] = this.data['u_celular'];
                        this.userApp['u_carnet'] = this.data['u_carnet'];
                        let nombre = this.userApp['u_nombre']+' '+this.userApp['u_apellido'];
                        this.events.publish('user:nombre', nombre);
                
                        localStorage.setItem('legal_userApp',JSON.stringify(this.userApp));
                        
                      }
                    }]
                });
                alert.present();

    });
  }

  gotopassword(){
    this.navCtrl.push('PasswordchangePage',{tipo:'user'});
  }

}
