import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ExamendetailPage } from './examendetail';

@NgModule({
  declarations: [
    ExamendetailPage,
  ],
  imports: [
    IonicPageModule.forChild(ExamendetailPage),
  ],
  exports: [
    ExamendetailPage
  ]
})
export class ExamendetailPageModule {}
