import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';

import {ServiceProvider} from '../../providers/service/service';
import { AuthProvider } from "../../providers/auth/auth";

@IonicPage()
@Component({
  selector: 'page-examendetail', 
  templateUrl: 'examendetail.html',
})
export class ExamendetailPage {

  public data:any;
  public preguntas:any;
  constructor(private authProvider: AuthProvider, public modalCtrl: ModalController, public ds: ServiceProvider, public navCtrl: NavController, public navParams: NavParams) {
    this.data = this.navParams.get('examen'); 
    this.preguntas={};
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ExamendetailPage');
    var token = this.authProvider.getToken();
    var user = this.authProvider.getProfesional();
    
    this.ds.getDetalleExameneProfesional(user,token,this.data['idexamen']).subscribe( data=> {
      
      if (data.errorCode==0){
        this.preguntas = data.msg;
      }
       console.log(data);
    });
  }

  cursar(){
    let modal = this.modalCtrl.create('ExamenPage',{examen:this.preguntas});
    modal.onDidDismiss(data => {
        
        this.navCtrl.last();
    });
     modal.present();
    

  }

}
