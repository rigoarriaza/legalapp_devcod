import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, AlertController, ToastController, LoadingController } from 'ionic-angular';

import { ServiceProvider } from '../../providers/service/service';
import { AuthProvider } from "../../providers/auth/auth";
import moment from 'moment'; 

@IonicPage()
@Component({
  selector: 'page-consulta',
  templateUrl: 'consulta.html',
})
export class ConsultaPage {
    [name: string]: any;
    
    data: {};

  public consultaTipo;
  public idprofesional;
  public profesional;
  public valor;
  constructor( private authProvider: AuthProvider, public loadingCtrl: LoadingController, public toastCtrl: ToastController,public alertCtrl: AlertController,public ds: ServiceProvider, public viewCtrl: ViewController,public navCtrl: NavController, public navParams: NavParams) {
    this.idprofesional = this.navParams.get('idprofesional');
    
    this.consultaTipo = navParams.get('tipo');
    
    if (navParams.get('pregunta') != undefined){ // si viene para modificar directa
      this.data = navParams.get('pregunta') ;
      this.data['pd_descripcion'] = this.data['pd_contenido'];
      this.profesional = this.data['p_nombre'] + ' ' + this.data['p_apellido'];
      
      this.ds.getValorServiciosProfesional(this.data['idprofesional']).subscribe( data=> {
        this.valor = data.msg;
      });
      this.valor = navParams.get('valor');
    }else{ // si es nueva pregunta
      this.data= {};
      this.data['pd_titulo'] = navParams.get('titulo');
      if( navParams.get('detalle') != undefined){
        this.data['pd_descripcion'] = navParams.get('detalle');
      }else{
        this.data['pd_descripcion'] = '';
      }
      this.profesional = navParams.get('profesional');
      this.valor = navParams.get('valor');
    }
   
    

    console.log(this.data);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ConsultaPage');

  }
  dismiss() {
    this.viewCtrl.dismiss();
  }

  preguntar(){

    
    let loader = this.loadingCtrl.create({
      content: "Guardando..."
    });
    loader.present();

    this.data['iduserapp'] = this.authProvider.getUsuario(); 
    this.data['token_session'] = this.authProvider.getToken(); 
    this.data['idespecialidad'] = 2;
    this.data['idprofesional'] = this.idprofesional ;
    this.data['privacidad'] = 0;

    if( this.data['p_privacidadform']==true){
      this.data['privacidad']=1;
    }

    console.log(this.data);
    let msj = "";
    let tit = "";
    if (this.consultaTipo == 2){
      tit = "¿Has finalizado tu pregunta directa?";
      msj = "Asegúrate de que la misma contenga todos los detalles necesarios para que el abogado pueda darte una respuesta precisa.";
    }else{ //cotizacion
      tit = "¿Has finalizado tu solicitud de cotización?";
      msj = "Recuerda que no podrás realizar cambios una vez enviada la solicitud. Asegúrate de que la misma contenga todos los detalles necesarios para que el abogado pueda realizar una cotización precisa.";
    }
    let confirm = this.alertCtrl.create({
    title: tit,
    message: msj,
    buttons: [
        {
          text: 'No, editar',
          handler: () => {
            loader.dismiss();
            console.log('Disagree clicked');
          }
        },
        {
          text: 'Si, enviar',
          handler: () => {

            //PREGUNTA DIRECTA
            if (this.consultaTipo == 2){
                if(this.data['pd_descripcion']==undefined || this.data['pd_descripcion']=='' ){
                //error pregunta vacia
                  loader.dismiss();
                  let toast = this.toastCtrl.create({
                          message: `Ingrese su pregunta`,
                          duration: 2500,
                          position: 'top'
                      });
                  toast.present();
                }else{
                  //VERIFICAR si se modifica o es nueva
                  if(this.data['idpdirecta']== undefined){
                    this.ds.postPreguntaDirecta(this.data).subscribe( data=> {
                      console.log(data);
                      loader.dismiss();
                      if (data.errorCode==0){
                        let alert = this.alertCtrl.create({
                          title: '¡Pregunta enviada!',
                          subTitle: 'Tu pregunta ha sido enviada. Verifica tu saldo y espera tu respuesta.',
                          buttons: [{
                              text: 'Aceptar',
                              handler: data => {
                                this.navCtrl.popToRoot();
                              }
                            }]
                        });
                        alert.present();
                      }else{
                        let alert = this.alertCtrl.create({
                          title: '¡error al enviar!',
                          subTitle: 'Tu pregunta no se ha podido enviar.',
                          buttons: [{
                              text: 'Aceptar',
                              handler: data => {
                                // this.navCtrl.popToRoot();
                              }
                            }]
                        });
                        alert.present();                    
                      }
                    });     
                  }else{
                    this.ds.setModificarPreguntaDirecta(this.data).subscribe( data=> {
                      console.log(data);
                      loader.dismiss();
                      if (data.errorCode==0){
                        let alert = this.alertCtrl.create({
                          title: '¡Pregunta modificada!',
                          subTitle: 'Tu pregunta ha sido enviada. Verifica tu saldo y espera tu respuesta.',
                          buttons: [{
                              text: 'Aceptar',
                              handler: data => {
                                this.navCtrl.popToRoot();
                              }
                            }]
                        });
                        alert.present();
                      }else{
                        let alert = this.alertCtrl.create({
                          title: '¡error al enviar!',
                          subTitle: 'Tu pregunta no se ha podido modificar.',
                          buttons: [{
                              text: 'Aceptar',
                              handler: data => {
                                // this.navCtrl.popToRoot();
                              }
                            }]
                        });
                        alert.present();                    
                      }
                    });                    
                  }
                }
            }else{ // COTIZACION
              this.data['c_descripcion'] = this.data['pd_descripcion'] ;
              this.data['c_titulo'] = this.data['pd_titulo'] ;

              if(this.data['pd_descripcion']==undefined || this.data['pd_descripcion']=='' ){
                    //error pregunta vacia
                loader.dismiss();
                let toast = this.toastCtrl.create({
                            message: `Pregunta vacía, ingrese su solicitud`,
                            duration: 2500,
                            position: 'top'
                        });
                toast.present();
              }else if(moment(this.data['c_iniciof'])>moment(this.data['c_finf'])){
                      //error fechas
                loader.dismiss();
                let toast = this.toastCtrl.create({
                            message: `La fecha de Finalización no es correcta.`,
                            duration: 4500,
                            position: 'top'
                        });
                toast.present();            
              }else{
                    this.ds.setCotizacionUsuarioapp(this.data).subscribe( data=> {
                      console.log(data);
                      loader.dismiss();
                
                      let alert = this.alertCtrl.create({
                        title: '¡Solicitud de cotización enviada!',
                        subTitle: 'Tu solicitud ha sido enviada',
                        buttons: [{
                            text: 'Aceptar',
                            handler: data => {
                              this.navCtrl.popToRoot();
                            }
                          }]
                      });
                      alert.present();
                    });     
                  }
            }

          }
        }]
    });
       confirm.present();

   
  }

  change() {
      // get elements
      var element   = document.getElementById('pd_descripcion');
      var textarea  = element.getElementsByTagName('textarea')[0];

      // set default style for textarea
      textarea.style.minHeight  = '0';
      textarea.style.height     = '0';

      // limit size to 96 pixels (6 lines of text)
      var scroll_height = textarea.scrollHeight;
      if(scroll_height > 260)
        scroll_height = 260;

      // apply new style
      element.style.height      = scroll_height + "px";
      textarea.style.minHeight  = scroll_height + "px";
      textarea.style.height     = scroll_height + "px";
    }
}
