import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ServiceProvider } from "../../providers/service/service";

/**
 * Generated class for the FavoritosPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-favoritos',
  templateUrl: 'favoritos.html',
})
export class FavoritosPage {

  public abogados: any;
  public userApp:any;
  public inicio:number = 0;
  public fin: number = 10;
  constructor(public ds: ServiceProvider, public navCtrl: NavController, public navParams: NavParams) {
    this.userApp = JSON.parse(localStorage.getItem('legal_userApp')); 
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FavoritosPage');

    
    this.ds.getAbogadosFavorito(this.userApp['iduserapp'],this.userApp['token_session'],this.inicio,this.fin).subscribe( data=> {
      
      if (data.errorCode==0){
        this.abogados = data.msg;
      }else if(data.errorCode==7){
        //error de token
      }
       console.log(data);
    });
  }

  gotoProfesional(datos){
    this.navCtrl.push('PerfilPage',{abogado: datos});
  }

  cargarMas(infiniteScroll) {
    console.log('Begin async operation');
    this.inicio+= 10;
    this.fin+=10;
    setTimeout(() => {
      
        this.ds.getAbogadosFavorito(this.userApp['iduserapp'],this.userApp['token_session'], this.inicio,this.fin).subscribe( data=> {
          
          if (data.errorCode==0){
            for (let i = 0; i < data.msg.length; i++) {
              this.abogados.push( data.msg[i] );
            }
            // this.abogados.concat(data.msg);
          }else if(data.errorCode==7){
            //error de token
          }
        }); 
      
      infiniteScroll.complete();
    }, 500);
  }

}
