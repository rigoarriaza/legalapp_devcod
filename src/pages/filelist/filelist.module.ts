import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FilelistPage } from './filelist';

@NgModule({
  declarations: [
    FilelistPage,
  ],
  imports: [
    IonicPageModule.forChild(FilelistPage),
  ],
})
export class FilelistPageModule {}
