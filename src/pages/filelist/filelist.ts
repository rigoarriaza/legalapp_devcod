import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, ModalController } from 'ionic-angular';


import { InAppBrowser, InAppBrowserOptions } from "@ionic-native/in-app-browser";
import { ServiceProvider } from '../../providers/service/service';
import { AuthProvider } from "../../providers/auth/auth";

@IonicPage()
@Component({
  selector: 'page-filelist',
  templateUrl: 'filelist.html',
})
export class FilelistPage {

  public archivos: any;
  public userApp: any;
  constructor(private authProvider: AuthProvider, public modalCtrl:ModalController, public alertCtrl: AlertController,private inAppBrowser: InAppBrowser, public ds: ServiceProvider,public navCtrl: NavController, public navParams: NavParams) {
    this.userApp = JSON.parse(localStorage.getItem('legal_userApp')); 
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FilelistPage');

    if( this.authProvider.getEstado() == 2 ){
      if(this.authProvider.getPlanProfesional() == null){
        //mensaje
        let msj = {};
        msj['titulo'] = "Adquiere tu plan Premium";
        msj['cuerpo'] = "Obtén un plan premium para poder acceder a los archivos que Legal App tiene para la comunidad de abogados";
        msj['opt'] = "1";

        let modal = this.modalCtrl.create('ModalmsgPage',{datos: msj});
        modal.present(); 
        modal.onDidDismiss((data)=>{
          console.log(data);
          if(data != '0'){
            this.navCtrl.push(data);
          }
        
        }); 
      }else{
        this.ds.getArchivosDisponiblesProfesional(this.userApp['idprofesional'],this.userApp['token_session']).subscribe( data=> {
          
            if (data.errorCode==0){
              this.archivos = data.msg;
            }
            console.log(data);
            //  console.log(this.preguntas);
        });
      
      }
    }else {
      //mensaje
      let msj = {};
      msj['titulo'] = "Acceso restringido";
      msj['cuerpo'] = "Su cuenta esta pendiente de ser verficada por los administradores.";
      msj['opt'] = "0";

      let modal = this.modalCtrl.create('ModalmsgPage',{datos: msj});
      modal.present();
      modal.onDidDismiss((data)=>{
        console.log(data);
        if(data != '0'){
          this.navCtrl.push(data);
        }
      
      });
        
    }

  }

  openWebpage(url: string) {
    const options: InAppBrowserOptions = {
      zoom: 'no'
    }
    let target = "_self";
         console.log(url);     
    // Opening a URL and returning an InAppBrowserObject
    const browser = this.inAppBrowser.create(url, target);

   // Inject scripts, css and more with browser.X
  }

}


// [restrict userlevel="subscriber"] 
// [/restrict]

// [not_logged_in][ultimatemember form_id=2510] [/not_logged_in]