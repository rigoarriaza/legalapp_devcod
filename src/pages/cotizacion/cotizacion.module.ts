
import { PipesModule } from '../../pipes/pipes.module';
import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CotizacionPage } from './cotizacion';

@NgModule({
  declarations: [
    CotizacionPage,
  ],
  imports: [
    IonicPageModule.forChild(CotizacionPage),PipesModule
  ],
  exports: [
    CotizacionPage
  ]
})
export class CotizacionPageModule {}
