import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,ToastController,AlertController } from 'ionic-angular';

import { ServiceProvider } from '../../providers/service/service';
import { AuthProvider } from "../../providers/auth/auth";

@IonicPage()
@Component({
  selector: 'page-cotizacion',
  templateUrl: 'cotizacion.html',
})
export class CotizacionPage {

  public idcotizacion: any;
  public data:any;
  public data_det:any;
  constructor(private authProvider: AuthProvider,public alertCtrl: AlertController,public toastCtrl: ToastController,public ds: ServiceProvider,public navCtrl: NavController, public navParams: NavParams) {
    this.idcotizacion = this.navParams.get('id');
    this.data = {};
    this.data_det = {};
    console.log(this.idcotizacion);
}

  ionViewDidLoad() {
    

    this.ds.getPreguntasCotizacionDetalle(this.authProvider.getUsuario(),this.authProvider.getToken(),this.idcotizacion).subscribe( data=> {
        
        console.log(data);
        if (data.errorCode==0){
          this.data = data.msg[0];
          this.data_det = data.msg[0].detalle_respuesta;
        }else if(data.errorCode==7){
          //error de token
          // this.navCtrl.popToRoot();
        }

      });
  }

  // responder(){
  //   this.data['idcotizacion'] = this.data['idpreguntad']; 
  //   this.data['estado'] = 1; 
  //   this.data['token_session'] = localStorage.getItem('legal_token'); 
  //   this.data['idprofesional']= localStorage.getItem('legal_idprofesional'); 
    
  //   console.log(this.data);


  //   if(this.data['respuesta']==undefined || this.data['respuesta']=='' ){
  //     //error pregunta vacia
  //     let toast = this.toastCtrl.create({
  //             message: `Ingrese su respuesta`,
  //             duration: 2500,
  //             position: 'top'
  //         });
  //     toast.present();
  //   }else{
  //     //asegurar envio de pregunta
  //     let confirm = this.alertCtrl.create({
  //     title: 'Ha finalizado su respuesta?',
  //     message: 'Recuerde que no podrá realizar cambios una vez enviada su respuesta. Asegurese que su respuesta contiene todo lo necesario.',
  //     buttons: [
  //       {
  //         text: 'No, editar',
  //         handler: () => {
  //           console.log('Disagree clicked');
  //         }
  //       },
  //       {
  //         text: 'Si, enviar',
  //         handler: () => {
            
            
  //              this.ds.postRespuestaCotizacion(this.data).subscribe( data=> {
  //               console.log(data);
  //               let alert = this.alertCtrl.create({
  //                 title: 'Respuesta enviada!',
  //                 subTitle: 'Tu respuesta ha sido enviada !',
  //                 buttons: [{
  //                     text: 'Aceptar',
  //                     handler: data => {
  //                       this.navCtrl.push('ListservicePage');
  //                     }
  //                   }]
  //               });
  //               alert.present();
  //             });
            
            
            
  //         }
  //       }
  //     ]
  //   });
  //   confirm.present();

         
  //   }
  // }
}
