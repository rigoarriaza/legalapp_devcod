import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, AlertController, ToastController, LoadingController } from 'ionic-angular';


import {ServiceProvider} from '../../providers/service/service';

@IonicPage()
@Component({
  selector: 'page-calificar',
  templateUrl: 'calificar.html',
})
export class CalificarPage {
    [name: string]: any;
    
  data: {};
  public data2: {};
  public rating:any;
  constructor(public loadingCtrl: LoadingController, public ds: ServiceProvider, public toastCtrl: ToastController,public alertCtrl: AlertController, public viewCtrl: ViewController, public navCtrl: NavController, public navParams: NavParams) {
    this.data = {};
    this.data2 = {};

    this.rating=[{id:0,val:true},{id:1,val:false},{id:2,val:false},{id:3,val:false},{id:4,val:false}];
    console.log(this.rating);
    
  }
  rate(id){
    localStorage.setItem('legal_rate',id+1);
    for (var i = 0; i < 5; i++) {
      if(i>id){
        this.rating[i].val = false;
      }else{
        this.rating[i].val = true;
      }
    }
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad CalificarPage');
    localStorage.setItem('legal_rate','1');
    this.data = this.navParams.get('id');
    this.data2 = this.data['p_respuesta'];
    console.log(this.data);
    console.log(this.data['p_respuesta']);
    console.log(this.data2['foto_prof']);
    
  }
  dismiss() {
    this.viewCtrl.dismiss();
  }

  calificar(){
    
    let loader = this.loadingCtrl.create({
        content: "Enviando..."
    });
    loader.present();

    let datos = {};

    datos['idrespuesta'] = this.data['p_respuesta'].idrespuesta; 
    datos['calificacion'] = localStorage.getItem('legal_rate');
    datos['iduserapp'] = localStorage.getItem('legal_user'); 
    datos['token_session'] = localStorage.getItem('legal_token'); 
    datos['comentario'] = this.data['comentario']; 
    
    console.log(datos);
    
    this.ds.postCalificacionExpress(datos).subscribe( data=> {

      console.log(data);
       if (data.errorCode==0){ 
        let alert = this.alertCtrl.create({
          title: 'Calificación enviada!',
          subTitle: 'Gracias por puntuar el servicio, ayudará a nuestra mejora constante!',
          buttons: [{
              text: 'Aceptar',
              handler: data => {
                this.viewCtrl.dismiss();
              }
            }]
        });
        alert.present();

       }else if (data.errorCode==8){
           let toast = this.toastCtrl.create({
                    message: `Gracias, esta respuesta ya fue calificada previamente`,
                    duration: 5000,
                    position: 'top'
                });
            toast.present();
            this.viewCtrl.dismiss();
       }else{
           let toast = this.toastCtrl.create({
                    message: `Error: intente más tarde.`,
                    duration: 5000,
                    position: 'top'
                });
            toast.present();
            this.viewCtrl.dismiss();
       }
       loader.dismiss();
    });  
         
    
  }
}
