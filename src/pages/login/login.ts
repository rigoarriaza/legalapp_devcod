import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController , LoadingController } from 'ionic-angular';
// import {TabsPage} from '../tabs/tabs';

import { Events } from 'ionic-angular';
import { ToastController } from 'ionic-angular';
import { Facebook } from '@ionic-native/facebook';

import {ServiceProvider} from '../../providers/service/service';
// import { LocalProvider } from '../../providers/local/local';
import { AuthProvider } from "../../providers/auth/auth";
import { CnnProvider } from "../../providers/cnn/cnn";

// 2103815563194214
// ionic cordova plugin add cordova-plugin-facebook4 --variable APP_ID="347110125765907" --variable APP_NAME="Legal App"
// import com.facebook.FacebookSdk;
// import com.facebook.appevents.AppEventsLogger;
//mac os hash
//keytool -exportcert -alias androiddebugkey -keystore ~/.android/debug.keystore | openssl sha1 -binary | openssl base64
//win
//keytool -exportcert -alias androiddebugkey -keystore "C:\Users\USERNAME\.android\debug.keystore" | "PATH_TO_OPENSSL_LIBRARY\bin\openssl" sha1 -binary | "PATH_TO_OPENSSL_LIBRARY\bin\openssl" base64

//      Enter keystore password:  clanbolivia
// ga0RGNYHvNM5d0SLGQfpQWAPGJ8= 

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class Login {

  user: any = {};
  showUser: boolean = false;
 
  public tipo: any;
  data : {};
  registro : {};
  constructor(private facebook: Facebook, private cnn: CnnProvider, private authProvider: AuthProvider, public loadingCtrl: LoadingController, private menu: MenuController, public toastCtrl: ToastController,public ds: ServiceProvider, public events: Events,public navCtrl: NavController, public navParams: NavParams) {
    this.data = {};
    this.registro = {};

  }
  ionViewDidEnter() {
    this.menu.swipeEnable(false);
  }
  // ionViewDidLeave() {
  //   this.menu.swipeEnable(true);
  // }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Login');
    this.tipo = localStorage.getItem('userTipo');;
   
    let sData = this.authProvider.getSesionData(this.tipo);
    if(sData != 0 ){
      this.data['sesion'] = true;
      if(this.tipo ==1){
        this.data['correo'] = sData['user_u'];
        this.data['password'] =sData['pass_u'];
      }else{
        this.data['correo'] = sData['user_p'];
        this.data['password'] =sData['pass_p'];
      }
    } 
  }
  
  gototimeline(){
    
    if(this.data['correo']== undefined || this.data['correo']=="" || this.data['password']== undefined || this.data['password']==""){
      
      let toast = this.toastCtrl.create({
              message: `Ingrese su Correo electrónico y Contraseña.`,
              duration: 2500,
              position: 'top'
          });
      toast.present();
    }else {
      if(this.data['sesion']== undefined || this.data['sesion']==false){
        this.data['sesion'] = '0';
      }else{
        this.data['sesion']='1';
      }


      //limpiar correo
      this.data['correo'].trim();

      //guardar sesion
      this.authProvider.setSesionData(this.data['correo'], this.data['password'],this.tipo, this.data['sesion']);

      if(this.cnn.isOnline()){
        //cargando modal
        let loader = this.loadingCtrl.create({
          content: "Verificando...",
        });
        loader.present();
        //LOGIN PARA USUARIO NATURAL
        if(this.tipo == "1") { 
          //para diferenciar login de fb
          this.data['tipologin']="1";

          //autenticar usuario
            this.authProvider.login(this.data['correo'],this.data['password']).then(success=>{
              if(success=='0'){
                this.navCtrl.setRoot('MenuPage');
                loader.dismiss();
              }else{
                loader.dismiss();
                this.toastCtrl.create({
                        message: `Error: -`+success,
                        duration: 5000,
                        position: 'top'
                    }).present();             
              }
            }).catch(err =>{
                loader.dismiss();

                  let toast = this.toastCtrl.create({
                        message: `Error: `+err,
                        duration: 2500,
                        position: 'top'
                    });
                  toast.present();
            }); 
          
        
        }else { 
          
          //LOGIN PARA PROFESIONAL
          this.authProvider.loginPro(this.data['correo'],this.data['password']).then(success=>{
            if(success=='0'){
                this.navCtrl.setRoot('MenuPage');
                loader.dismiss();
              }
            }).catch(err =>{
                loader.dismiss();

                  let toast = this.toastCtrl.create({
                        message: `Error: `+err,
                        duration: 2500,
                        position: 'top'
                    });
                  toast.present();
            });
          //   if (data.errorCode==0){ //si no existe error

          //     localStorage.setItem('legal_token',data.msg['token_session']); 
          //     localStorage.setItem('legal_idprofesional',data.msg['idprofesional']); 
          //     localStorage.setItem('legal_name_u',data.msg['u_nombre']+' '+data.msg['u_apellido']); 
          //     data.msg['tipo_user'] = '2';
                
          //     if(data.msg['u_foto'] == ''){
          //       localStorage.setItem('legal_foto_u', 'assets/img/avatar.png');
          //       data.msg['legal_foto'] = 'assets/img/avatar.png'; 
          //     }else{
          //       localStorage.setItem('legal_foto_u', 'http://legalbo.com/legalapp/IMGUSERS/' +  data.msg['u_foto']); 
          //       data.msg['legal_foto'] = 'http://legalbo.com/legalapp/IMGUSERS/' +  data.msg['u_foto'];
          //     }
              
          //     console.log(data.msg);
          //     localStorage.setItem('legal_userApp',JSON.stringify(data.msg)); 
                
          //     this.navCtrl.setRoot(HomeproPage);
                
          //     this.events.publish('user:created', this.tipo);
          //     loader.dismiss();
          //   }else if (data.errorCode==3){ //si no existe error
          //     loader.dismiss();

          //       let toast = this.toastCtrl.create({
          //             message: `Error: Verfique sus datos e intente de nuevo.`,
          //             duration: 2500,
          //             position: 'top'
          //         });
          //       toast.present();

          //   }else{
          //     loader.dismiss();
              
          //     let toast = this.toastCtrl.create({
          //             message: `Error: `+ data.errorMessage,
          //             duration: 3000,
          //             position: 'top'
          //         });
          //     toast.present();
          //   }
        
          // });

        }
      }else{
        this.toastCtrl.create({
          message: `Has perdido la conexión a internet.`,
          duration: 3000
        }).present();
      }
   
    }
      
    
  }
  gotoforgotpassword(){
    this.navCtrl.push('ForgotpasswordPage',{tipo:this.tipo})
  }
  gotosignup(){
   this.navCtrl.push('SignupPage',{tipo:this.tipo})
    
  }

  go_fb(){
    this.facebook.login(['public_profile', 'email'])
    .then(rta => {
      console.log(rta.status);
      this.toastCtrl.create({
          message: rta.status,
          duration: 2000
      }).present();
      if(rta.status == 'connected'){
        this.getInfo();
      };
    })
    .catch(error =>{
      console.error( error );
    });
  }

  getInfo(){
    console.log(this.facebook.getLoginStatus());

    this.facebook.api('/me?fields=id,name,email,first_name,picture,last_name,gender',['public_profile','email'])
    .then(data=>{
      console.log(data);
      this.showUser = true; 
      this.user = data;
      
      // this.registro['nombre'] = this.user.first_name;
      // this.registro['apellido'] = this.user.last_name;
      // this.registro['correo'] = this.user.email;
      // this.registro['password'] = '';
      // this.registro['celular'] = '';
      // this.registro['foto'] = this.user.picture.data.url;
      // this.registro['tokenfacebook'] = this.user.id;
      
      this.data['nombre'] = this.user.first_name;
      this.data['apellido'] = this.user.last_name;
      this.data['correo'] = this.user.email;
      this.data['foto'] = this.user.picture.data.url;
      this.data['celular'] = '';
      this.data['tipologin'] = 2;
      this.data['tokenfacebook'] = this.user.id;

      let loader = this.loadingCtrl.create({
        content: "Verificando...",
        dismissOnPageChange: true,
      });
      loader.present();  

        //autenticar usuario
        this.authProvider.loginFB(this.data).then(success=>{
          if(success=='0'){
            this.navCtrl.setRoot('MenuPage');
            loader.dismiss();
          }
          loader.dismiss();
        }).catch(err =>{
            loader.dismiss();

              let toast = this.toastCtrl.create({
                    message: `Error: `+err,
                    duration: 2500,
                    position: 'top'
                });
              toast.present();
        }); 

      // this.ds.postRegistro(this.registro).subscribe( data=> {
      //   localStorage.setItem('legal_token',data.msg['token_session']); 
      //   localStorage.setItem('legal_user',data.msg['iduserapp']); 
      //   localStorage.setItem('legal_name_u',data.msg['u_nombre']+' '+data.msg['u_apellido']); 
      //   localStorage.setItem('legal_name',data.msg['u_nombre']); 
      //   localStorage.setItem('legal_apellido',data.msg['u_apellido']); 
      //   localStorage.setItem('legal_celular',data.msg['u_celular']); 
              
      //   localStorage.setItem('legal_foto_u', 'assets/img/avatar.png'); 
        
        // this.navCtrl.setRoot(TimelinePage)
              
        // this.events.publish('user:created', this.tipo);
      // });
    })
    .catch(error =>{
      console.error( error );
    });
  }

  go_back(){
    this.navCtrl.pop();
  }
}
