import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, PopoverController, ToastController, AlertController, LoadingController } from 'ionic-angular';

import { ServiceProvider } from '../../providers/service/service';
import { AuthProvider } from "../../providers/auth/auth";
import moment from 'moment';


 
@IonicPage()
@Component({
  selector: 'page-chat',
  templateUrl: 'chat.html',
})
export class ChatPage {

  public pregunta: any;
  public preguntadet: any;
  public idpregunta:any;
  public tipo:any;
  public respuestas:any;
  public n_respuestas:boolean = false;
  public comentario :string = '';
  public formDisabled:boolean = true;
  public formShow:boolean = true;
  public calificar:boolean = true;
  public showReplica:boolean = false;
  public showCalificar:boolean = false;

   data: {};
  public n_foto='';
  public rating:any;
  public tiempoRestante:any;
  constructor(private authProvider: AuthProvider, public loadingCtrl: LoadingController, public alertCtrl: AlertController,public toastCtrl: ToastController,public ds: ServiceProvider,public modalCtrl: ModalController, public navCtrl: NavController, public navParams: NavParams,public popoverCtrl:PopoverController) {
    this.data = {};
    this.pregunta = {};
    this.preguntadet = {};
    this.respuestas = [];
}


  ionViewDidLoad() {
     this.rating=[{id:0,val:true},{id:1,val:false},{id:2,val:false},{id:3,val:false},{id:4,val:false}];
   
    this.n_foto = this.authProvider.getFotoPro();
    
    this.idpregunta = this.navParams.get('pregunta');
    this.tipo = this.navParams.get('tipo');
    
    var token = this.authProvider.getToken();
    var user = this.authProvider.getUsuario();
    console.log(this.idpregunta);
    console.log(this.tipo);
    this.tiempoRestante = "";
        
    
    if(this.tipo==1){ //pregunta express
      this.ds.getPreguntaExpressDetalle(user,token,this.idpregunta).subscribe( data=> {
        if (data.errorCode==0){
          this.pregunta = data.msg;
          this.respuestas = data.msg.detalle_r;

          // mostrar tiempos
          if(this.pregunta.p_estado == 5  ){ //contestada 24 horas para responder
            console.log(this.respuestas[0].profesional_info.idprofesional);
          
            var a = moment(this.respuestas[0].datetime);
            var b = moment();
            this.tiempoRestante  = b.diff(a,  'hours');

            if(this.tiempoRestante > 24){
              this.showReplica = false;
              this.calificar = true;
              this.showCalificar = true;
              
            }else{
              this.showReplica = true;
              this.tiempoRestante = "Te quedan " + (24 - this.tiempoRestante) + " horas para realizar una réplica.";
            }

          }else if(this.pregunta.p_estado == 7 ){
            
            this.formShow = false;
            this.showReplica = false;
            this.showCalificar = true;

          }
          if(this.pregunta.calificacion!=0){
            this.rateIni(this.pregunta.calificacion-1);
            this.calificar=false;
          }

        }else if(data.errorCode==7){
          //error de token
          this.navCtrl.popToRoot();
        }else if(data.errorCode==16){
          let confirm = this.alertCtrl.create({
          title: 'Saldo insuficiente',
          message: 'Recuerde que para ver la respueste de tu abogado, primero deberá realizar el pago respectivo.',
          buttons: [
              {
                text: 'Entiendo',
                handler: () => {
                  console.log('Disagree clicked');
                  this.navCtrl.popToRoot();
                }
              },
              {
                text: 'Ir a Mi cuenta',
                handler: () => {
                  console.log('Disagree clicked');
                  this.navCtrl.push("WalletPage");
                }
              }
            ]
          });
          confirm.present();
          //error de saldo
          
        }
        console.log(this.pregunta);
      });
    }else{ //pregunta directa
      this.ds.getPreguntaDirectaDetalle(user,token,this.idpregunta).subscribe( data=> {
        
        // debugger;
        console.log(data);
        if (data.errorCode==0){
          this.pregunta = data.msg;
          this.respuestas = data.msg.detalle_r;
          this.preguntadet['nombre_prof'] = this.pregunta['p_nombre'];
          this.preguntadet['apellido_prof'] = this.pregunta['p_apellido'];
          // this.preguntadet = data.msg.p_respuesta;
          
          console.log(data.msg );

          // debugger;

          if(this.pregunta.estado == 5  ){ //contestada 24 horas para responder

            var a = moment(this.respuestas[0].datetime);
            var b = moment();
            this.tiempoRestante  = b.diff(a,  'hours');

            if(this.tiempoRestante > 24){
              this.showReplica = false;
              // this.calificar = true;
              // this.showCalificar = true;
              
            }else{
              this.showReplica = true;
              this.tiempoRestante = "Te quedan " + (24 - this.tiempoRestante) + " horas para realizar una réplica.";
            }

          }
          // if(this.respuestas == undefined){ 
          // if(this.respuestas.length == 3){
          //   this.n_respuestas = true;

          //   let toast = this.toastCtrl.create({
          //       message: 'Ya no puedes hacer más preguntas!',
          //       duration: 3500,
          //       position: 'top'
          //   });
          //   toast.present();
          // }
          // }
          
   
          console.log(this.n_respuestas);
        }else if(data.errorCode==7){
          //error de token
          this.navCtrl.popToRoot();
        }else if(data.errorCode==16){
          let confirm = this.alertCtrl.create({
          title: 'Saldo insuficiente',
          message: 'Recuerde que para ver la respueste de tu abogado, primero debes realizar el pago respectivo.',
          buttons: [
              {
                text: 'Entiendo',
                handler: () => {
                  console.log('Disagree clicked');
                  this.navCtrl.popToRoot();
                }
              },
              {
                text: 'Ir a Mi cuenta',
                handler: () => {
                  console.log('Disagree clicked');
                  this.navCtrl.push("WalletPage");
                }
              }
            ]
          });
          confirm.present();
          //error de saldo
          
        }
        
        
      });
    }

  }
  ionViewWillEnter() {
    let tabs = document.querySelectorAll('.show-tabbar');
    if (tabs !== null) {
      Object.keys(tabs).map((key) => {
        tabs[key].style.display = 'none';
      });
    } // end if
  }

  ionViewDidLeave() {
    let tabs = document.querySelectorAll('.show-tabbar');
    if (tabs !== null) {
      Object.keys(tabs).map((key) => {
        tabs[key].style.display = 'flex';
      });
    } // end if
  }

  messageattach(myEvent){
    let popover = this.popoverCtrl.create('Messageattach');
    popover.present({
      ev:myEvent
    })
  }

  changetextarea() {
    // get elements
    var element   = document.getElementById('messageInputBox');
    var textarea  = element.getElementsByTagName('textarea')[0];

    // set default style for textarea
    textarea.style.minHeight  = '50px';
    textarea.style.height     = '0';

    // limit size to 96 pixels (6 lines of text)
    var scroll_height = textarea.scrollHeight;
    if(scroll_height > 160)
      scroll_height = 160;

    // apply new style
    element.style.height      = scroll_height + "px";
    textarea.style.minHeight  = scroll_height + "px";
    textarea.style.height     = scroll_height + "px";
  }

  goCalificar(idpregunta, calificacion){
    console.log(calificacion);
    if(calificacion==0){ 
      // let modal = this.modalCtrl.create('CalificarPage',{id:this.pregunta});
      // modal.onDidDismiss(data => {
      //   this.navCtrl.pop();
      // });
      // modal.present();
      let loader = this.loadingCtrl.create({
          content: "Enviando..."
      });
      loader.present();

      let datos = {};

      datos['idpregunta'] = idpregunta; 
      datos['calificacion'] = localStorage.getItem('legal_rate');
      datos['iduserapp'] = this.authProvider.getUsuario(); 
      datos['token_session'] = this.authProvider.getToken(); 
      datos['comentario'] = this.comentario; 
      
      console.log(datos);
      
      this.ds.postCalificacionExpress(datos).subscribe( data=> {

        console.log(data);
        if (data.errorCode==0){ 
          let toast = this.toastCtrl.create({
                  message: '¡Gracias por puntuar el servicio, ayudará a nuestra mejora constante!',
                  duration: 3500,
                  position: 'top'
              });
          toast.present();
          this.calificar=false;
        }else if (data.errorCode==8){
            let toast = this.toastCtrl.create({
                      message: `Gracias, esta respuesta ya fue calificada previamente`,
                      duration: 5000,
                      position: 'top'
                  });
              toast.present();
        }else{
            let toast = this.toastCtrl.create({
                      message: `Error: intente más tarde.`,
                      duration: 5000,
                      position: 'top'
                  });
              toast.present();
        }
        loader.dismiss();
      });  
          
    }else{
      let toast = this.toastCtrl.create({
              message: `Este servicio ya fue calificado previamente con `+calificacion+' estrellas',
              duration: 2500,
              position: 'top'
          });
      toast.present();
    }
    
  }
  rate(id){
    if(this.pregunta.calificacion==0){

      localStorage.setItem('legal_rate',id+1);
      for (var i = 0; i < 5; i++) {
        if(i>id){
          this.rating[i].val = false;
        }else{
          this.rating[i].val = true;
        }
      }
    }
  }
  rateIni(id){
   localStorage.setItem('legal_rate',id+1);
      for (var i = 0; i < 5; i++) {
        if(i>id){
          this.rating[i].val = false;
        }else{
          this.rating[i].val = true;
        }
    }
    
  }

  responder(){
 

    if(this.data['messageInputBox']==undefined || this.data['messageInputBox']=='' ){
      //error pregunta vacia
      let toast = this.toastCtrl.create({
              message: `Ingrese su pregunta`,
              duration: 2500,
              position: 'top'
          });
      toast.present();
    }else{
      let loader = this.loadingCtrl.create({
          content: "Enviando..."
      });
      loader.present();

      //asegurar envio de pregunta
      let confirm = this.alertCtrl.create({
      title: '¿Ha finalizado su pregunta?',
      message: 'Recuerde que no podrá realizar cambios una vez enviada su pregunta. Asegúrese que contiene todo lo necesario.',
      buttons: [
        {
          text: 'No, editar',
          handler: () => {
            console.log('Disagree clicked');
            loader.dismiss();
          }
        },
        {
          text: 'Si, enviar',
          handler: () => {


            this.data['token_session'] = this.authProvider.getToken(); 
            this.data['iduserapp']= this.authProvider.getUsuario(); 
            this.data['r_detalle']= this.data['messageInputBox']; 
              
            if(this.tipo==1){ //pregunta express
              this.data['idpreguntae'] = this.idpregunta; 
              this.data['idprofesional'] =this.respuestas[0].profesional_info.idprofesional;
              console.log(this.data);
              this.ds.setRespuestaPreguntaExpressUApp(this.data).subscribe( data=> {
                console.log(data);
                loader.dismiss();
                let alert = this.alertCtrl.create({
                  title: '¡Respuesta enviada!',
                  subTitle: 'Tu respuesta ha sido enviada',
                  buttons: [{
                      text: 'Aceptar',
                      handler: data => {
                        this.navCtrl.pop();
                      }
                    }]
                });
                alert.present();
              });    
            }else{
              this.data['idpdirecta'] = this.idpregunta; 
              console.log(this.data);
              this.data['idprofesional'] =this.pregunta.idprofesional;
              this.ds.setRespuestaPreguntaDirectaUApp(this.data).subscribe( data=> {
                console.log(data);
                loader.dismiss();
                let alert = this.alertCtrl.create({
                  title: '¡Respuesta enviada!',
                  subTitle: 'Tu respuesta ha sido enviada',
                  buttons: [{
                      text: 'Aceptar',
                      handler: data => {
                        this.navCtrl.pop();
                      }
                    }]
                });
                alert.present();
              });    
            }
      
            
          }
        }
      ]
    });
    confirm.present();

         
    }
  }

  cancelarPregunta(){
    //asegurar envio de pregunta
      let confirm = this.alertCtrl.create({
      title: '¿Estas seguro de Cancelar su pregunta?',
      message: 'Esta pregunta ya no estará disponible para los abogados y tu dinero será reintegrado a tu cuenta.',
      buttons: [
        {
          text: 'No, regresar',
          handler: () => {
            console.log('Disagree clicked');
            
          }
        },
        {
          text: 'Si, cancelar',
          handler: () => {

            if(this.tipo == 1 ){ //express
              this.data['idpreguntae']= this.pregunta.id; 
              this.data['iduserapp']= this.authProvider.getUsuario(); 
              this.data['token_session']= this.authProvider.getToken(); 
              console.log(this.data);

              this.ds.postCancelarPreguntaExpress(this.data).subscribe( data=> {
                console.log(data);
                
                let alert = this.alertCtrl.create({
                  title: '¡Pregunta cancelada!',
                  subTitle: 'Tu pregunta express ha sido cancelada',
                  buttons: [{
                      text: 'Aceptar',
                      handler: data => {
                        this.navCtrl.pop();
                      }
                    }]
                });
                alert.present();
              });     
            }else{
              this.data['idpdirecta']= this.pregunta.idpdirecta; 
              this.data['iduserapp']= this.authProvider.getUsuario(); 
              this.data['token_session']= this.authProvider.getToken(); 
              console.log(this.data);

              this.ds.setCancelarPreguntaDirecta(this.data).subscribe( data=> {
                console.log(data);
                
                let alert = this.alertCtrl.create({
                  title: '¡Pregunta cancelada!',
                  subTitle: 'Tu pregunta ha sido cancelada',
                  buttons: [{
                      text: 'Aceptar',
                      handler: data => {
                        this.navCtrl.pop();
                      }
                    }]
                });
                alert.present();
              });     
            }
     
            
          }
        }
      ]
    });
    confirm.present();

  }

  modificarPregunta(){
    if(this.tipo == 1){

      this.navCtrl.push('UpdatestatusPage', {pregunta:this.pregunta});  
    }else{
      this.navCtrl.push('ConsultaPage', {pregunta:this.pregunta, tipo: 2});
    }
  }

  gotoWallet(){

 
    //pagar pregunta 
    this.data['idpreguntae'] = this.idpregunta; 

    this.data['token_session'] = this.authProvider.getToken();
    this.data['iduserapp']= this.authProvider.getUsuario();

    let loader = this.loadingCtrl.create({
          content: "Pagando..."
    });
    loader.present();
    console.log(this.data);
    let confirm = this.alertCtrl.create({
      title: '¿Quieres realizar el pago de tu pregunta?',
      message: 'Vamos a verificar tu saldo para poder realizar el pago.',
      buttons: [
          {
            text: 'No, regresar',
            handler: () => {
              console.log('Disagree clicked');
               loader.dismiss();  
            }
          },
          {
            text: 'Sí, pagar',
            handler: () => {

              if(this.tipo == 1){

                this.ds.setPaymentPreguntaExpress(this.data).subscribe( data=> {
                  console.log(data);

                  if (data.errorCode==0){ 
                  
                  let alert = this.alertCtrl.create({
                      title: '¡Pregunta Pagada!',
                      subTitle: 'Tu pregunta express ha sido pagada satisfactoriamente.',
                      buttons: [{
                        text: 'Aceptar',
                        handler: data => {
                          this.navCtrl.pop();
                        }
                      }]
                    });
                    alert.present();
                  }else if (data.errorCode==15){
                  let alert = this.alertCtrl.create({
                      title: '¡Saldo insuficiente!',
                      subTitle: 'Para que la pregunta sea visible, confirma tu saldo y en breve un abogado te responderá..',
                      buttons: [{
                        text: 'Pagar pregunta',
                        handler: data => {
                          this.navCtrl.setRoot('WalletPage');
                        }
                      }]
                    });
                    alert.present();
                  }else{
                  let alert = this.alertCtrl.create({
                      title: '¡Ops Error!',
                      subTitle: 'Vuelve a interntarlo.',
                      buttons: [{
                        text: 'Aceptar',
                        handler: data => {
                          // this.navCtrl.pop();
                        }
                      }]
                    });
                    alert.present();
                  }      

                  loader.dismiss();   

                });
              }else{
                this.data['idpreguntad'] = this.idpregunta; 
                this.data['idprofesional'] = this.pregunta.idprofesional; 
                console.log(this.data);
              
                this.ds.setPaymentPreguntaDirecta(this.data).subscribe( data=> {
                  console.log(data);

                  if (data.errorCode==0){ 
                  
                  let alert = this.alertCtrl.create({
                      title: '¡Pregunta Pagada!',
                      subTitle: 'Tu pregunta directa ha sido pagada satisfactoriamente.',
                      buttons: [{
                        text: 'Aceptar',
                        handler: data => {
                          this.navCtrl.pop();
                        }
                      }]
                    });
                    alert.present();
                  }else if (data.errorCode==15){
                  let alert = this.alertCtrl.create({
                      title: '¡Saldo insuficiente!',
                      subTitle: 'Para que la pregunta sea visible, confirma tu saldo y en breve un abogado te responderá..',
                      buttons: [{
                        text: 'Pagar pregunta',
                        handler: data => {
                          this.navCtrl.setRoot('WalletPage');
                        }
                      }]
                    });
                    alert.present();
                  }else{
                  let alert = this.alertCtrl.create({
                      title: '¡Ops Error!',
                      subTitle: 'Vuelve a interntarlo.',
                      buttons: [{
                        text: 'Aceptar',
                        handler: data => {
                          // this.navCtrl.pop();
                        }
                      }]
                    });
                    alert.present();
                  }      

                  loader.dismiss();   

                });     
              }
            }
          }
        ]
    });
    
    confirm.present();



    // this.navCtrl.push('WalletPage');
  }

  completarPregunta(){

    let confirm = this.alertCtrl.create({
      title: '¿Quieres complementar tu pregunta?',
      message: 'Recuerda que tienes derecho a complementar una vez tu pregunta. El complemento a una pregunta debe ser derivada de la pregunta original',
      buttons: [
          {
                text: 'No hace falta, gracias',
                handler: () => {
                  console.log('Disagree clicked');
                  // this.navCtrl.popToRoot();
                }
          },
          {
                text: 'Sí, me encantaría',
                handler: () => {
                  console.log('Disagree clicked');
                  
                  this.formDisabled = false;
                  // this.navCtrl.push("WalletPage");
                }
          }
            ]
          });
          confirm.present();
          //error de saldo
      
  }
}
