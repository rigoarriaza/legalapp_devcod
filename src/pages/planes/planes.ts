import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ServiceProvider } from "../../providers/service/service";

/**
 * Generated class for the PlanesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-planes',
  templateUrl: 'planes.html',
})
export class PlanesPage {

  public datos: any;
  constructor(public ds: ServiceProvider, public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PlanesPage');
    this.ds.getPlanesProfesional().subscribe( data=> { 
      console.log(data);
      if (data.errorCode==0){
        this.datos = data.msg;
      }
    });    
  }

  recarga(plan){
    this.navCtrl.push('RecargaPage',{plan: plan});
  }
}
