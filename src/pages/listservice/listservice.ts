import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController, ModalController } from 'ionic-angular';


import { ServiceProvider } from '../../providers/service/service';
import { AuthProvider } from "../../providers/auth/auth";
// import { TruncateModule } from '@yellowspot/ng-truncate';

@IonicPage()
@Component({
  selector: 'page-listservice',
  templateUrl: 'listservice.html',
})
export class ListservicePage {

  public userApp: any;
  public preguntas: any;
  public titulo: any;
  public tipo: any;

  public token;
  public user;
  constructor(public modalCtrl:ModalController, private authProvider: AuthProvider, public loadingCtrl: LoadingController, public alertCtrl: AlertController,public ds: ServiceProvider, public navCtrl: NavController, public navParams: NavParams) {
    this.userApp = JSON.parse(localStorage.getItem('legal_userApp')); 
  
    this.token = this.authProvider.getToken(); 
    this.user = this.authProvider.getProfesional(); 
  }
 
  ionViewDidLoad() {
    console.log('ionViewDidLoad ListservicePage');

    // this.cargarDataRefresh();
  }

  ionViewDidEnter() {
   
    this.cargarDataRefresh();
  }

  cargarDataRefresh(){
    
    // debugger;
    
    this.tipo = this.navParams.get('tipo');

    if(this.tipo == 1) { //express
      if( this.authProvider.getEstado() == 2 ){
        // debugger;
        this.titulo = "Preguntas Express";
        this.ds.getPreguntaExpressPendiente(this.userApp['idprofesional'],this.userApp['token_session']).subscribe( data=> {
        
          if (data.errorCode==0){
            this.preguntas = data.msg;
          }else if (data.errorCode==20){ //preguntas pendientes de contestar
            //mensaje
            let msj = {};
            msj['titulo'] = "Acceso restringido";
            msj['cuerpo'] = "Su cuenta bloqueada, ya que tiene preguntas pendientes dar replica. Vaya a Mis Preguntas y de respuesta a las preguntas pendientes para poder habilitar su cuenta nuevamente.";
            msj['opt'] = "0";

            let modal = this.modalCtrl.create('ModalmsgPage',{datos: msj});
            modal.present();   
          }
        });

      }else{
        //mensaje
        let msj = {};
        msj['titulo'] = "Acceso restringido";
        msj['cuerpo'] = "Su cuenta esta pendiente de ser verficada por los administradores.";
        msj['opt'] = "0";

        let modal = this.modalCtrl.create('ModalmsgPage',{datos: msj});
        modal.present();          
      }

    }else if (this.tipo == 2){ // directas

      if( this.authProvider.getEstado() == 2 ){
        if(this.authProvider.getPlanProfesional() == null){
          //mensaje
          let msj = {};
          msj['titulo'] = "Adquiere tu plan Premium";
          msj['cuerpo'] = "Las Preguntas Directas son un servicio exclusivo para los abogados con cuenta Premium. Adquiere tu plan Premium";
          msj['opt'] = "1";

          let modal = this.modalCtrl.create('ModalmsgPage',{datos: msj});
          modal.present(); 
          modal.onDidDismiss((data)=>{
            console.log(data);
            if(data != '0'){
              this.navCtrl.push(data);
            }
          
          }); 
        }else{
          /// ok
          this.titulo = "Preguntas Directas";
          this.ds.getPreguntasDirectasPro(this.user,this.token).subscribe( data=> {
          
            if (data.errorCode==0){
              this.preguntas = data.msg;
            }else if (data.errorCode==20){ //preguntas pendientes de contestar
              //mensaje
              let msj = {};
              msj['titulo'] = "Acceso restringido";
              msj['cuerpo'] = "Su cuenta bloqueada, ya que tiene preguntas pendientes dar replica. Vaya a Mis Preguntas y de respuesta a las preguntas pendientes para poder habilitar su cuenta nuevamente.";
              msj['opt'] = "0";

              let modal = this.modalCtrl.create('ModalmsgPage',{datos: msj});
              modal.present();   
            }
            console.log(data);
            console.log(this.preguntas);
          });    
        }
      }else {
        //mensaje
        let msj = {};
        msj['titulo'] = "Acceso restringido";
        msj['cuerpo'] = "Su cuenta esta pendiente de ser verficada por los administradores.";
        msj['opt'] = "0";

        let modal = this.modalCtrl.create('ModalmsgPage',{datos: msj});
        modal.present();
        modal.onDidDismiss((data)=>{
          console.log(data);
          if(data != '0'){
            this.navCtrl.push(data);
          }
        
        });
          
      }

    }else{ //cotizaciones

      if( this.authProvider.getEstado() == 2 ){
        if(this.authProvider.getPlanProfesional() == null){
          //mensaje
          let msj = {};
          msj['titulo'] = "Adquiere tu plan Premium";
          msj['cuerpo'] = "Las Cotizaciones son un servicio exclusivo para los abogados con cuenta Premium. Adquiere tu plan Premium ";
          msj['opt'] = "1";

          let modal = this.modalCtrl.create('ModalmsgPage',{datos: msj});
          modal.present(); 
          modal.onDidDismiss((data)=>{
            console.log(data);
            if(data != '0'){
              this.navCtrl.push(data);
            }
          
          }); 
        }else{
          /// ok
          this.titulo = "Cotizaciones";
          this.ds.getCotizacionesPro(this.user,this.token).subscribe( data=> {
          
            if (data.errorCode==0){
              this.preguntas = data.msg;
            }
            console.log(data);
            console.log(this.preguntas);
          });

          //////
        
        }
      }else {
        //mensaje
        let msj = {};
        msj['titulo'] = "Acceso restringido";
        msj['cuerpo'] = "Su cuenta esta pendiente de ser verficada por los administradores.";
        msj['opt'] = "0";

        let modal = this.modalCtrl.create('ModalmsgPage',{datos: msj});
        modal.present();
        modal.onDidDismiss((data)=>{
          console.log(data);
          if(data != '0'){
            this.navCtrl.push(data);
          }
        
        });
          
      }
    
    }
  }

  
  gotoResponder(datos){

    var pre = {};
    
    pre['idprofesional'] = this.authProvider.getProfesional();; 
    pre['token_session'] = this.authProvider.getToken();; 
    pre['idpreguntae'] = datos.idpreguntae; 
    
    //verificar si esta tomada
    console.log(datos.idpreguntae);
    console.log(pre);

    this.ds.postRespuestaPreguntaExpressTomar(pre).subscribe( data=> {
    //   console.log(data);
      if (data.errorCode==0){
        this.navCtrl.push('ChatproPage', {pregunta:datos,tipo:1});
      }else{
                let alert = this.alertCtrl.create({
                  title: '¡Pregunta tomada!',
                  subTitle: 'Lo sentimos, esta pregunta ya esta siendo contestada por otro colega.',
                  buttons: [{
                      text: 'Aceptar',
                      handler: data => {
                        this.cargarDataRefresh();
                      }
                    }]
                });
                alert.present();   
      }
    });
  }
  
  gotoResponderCotizacion(datos){
    this.navCtrl.push('CotizacionproPage', {pregunta:datos});

  }
 
  gotoCompleja(datos){
    let confirm = this.alertCtrl.create({
      title: 'Marcar como compleja?',
      message: 'Al marcar como compleja esta pregunta, le está indicando a la persona que su pregunta requiere de más información.',
      buttons: [
        {
          text: 'No, regresar',
          handler: () => {
            console.log('Disagree clicked');
          }
        },
        {
          text: 'Confirmar',
          handler: () => {
            let loader = this.loadingCtrl.create({
                content: "Enviando..."
            });
            loader.present();
            let pregunta = {};
            pregunta['idprofesional'] =this.userApp['idprofesional'];
            pregunta['token_session'] =this.userApp['token_session'];
            pregunta['idpreguntae'] =datos.idpreguntae;
            console.log(pregunta);
            this.ds.setPreguntaCompleja(pregunta).subscribe( data=> {
            
              if (data.errorCode==0){
                loader.dismiss();
                let alert = this.alertCtrl.create({
                  title: 'Pregunta compleja!',
                  subTitle: 'La pregunta ha sido marcada como compleja!',
                  buttons: [{
                      text: 'Aceptar',
                      handler: data => {
                        // this.navCtrl.pop();
                      }
                    }]
                });
                alert.present();              
              }else{
                loader.dismiss();
              }
              console.log(data);
            });
            console.log('Agree clicked');
          }
        }
      ]
    });
    confirm.present();
  }

  gotoRechazar(datos){
    let confirm = this.alertCtrl.create({
      title: '¿Estas seguro de rechazar la pregunta?',
      message: 'Al marcar como rechazada, ya no podrás ver más esta pregunta.',
      buttons: [
        {
          text: 'No, regresar',
          handler: () => {
            console.log('Disagree clicked');
          }
        },
        {
          text: 'Confirmar',
          handler: () => {
            let loader = this.loadingCtrl.create({
                content: "Enviando..."
            });
            loader.present();
            let pregunta = {};
            pregunta['idprofesional'] =this.userApp['idprofesional'];
            pregunta['token_session'] =this.userApp['token_session'];
            pregunta['idpdirecta'] =datos.idpreguntad;
            pregunta['iduserapp'] =datos.iduserapp;
            console.log(pregunta);
            this.ds.setRechazarPreguntaDirecta(pregunta).subscribe( data=> {
            
              if (data.errorCode==0){
                loader.dismiss();
                let alert = this.alertCtrl.create({
                  title: '¡Pregunta rechazada!',
                  subTitle: 'La pregunta ha sido rechazada y notificado al usuario.',
                  buttons: [{
                      text: 'Aceptar',
                      handler: data => {
                        this.navCtrl.popToRoot();
                      }
                    }]
                });
                alert.present();              
              }else{
                loader.dismiss();
              }
              console.log(data);
            });
            console.log('Agree clicked');
          }
        }
      ]
    });
    confirm.present();
  }

  rechazarCotizacion(datos){
    let confirm = this.alertCtrl.create({
      title: '¿Estas seguro de rechazar la la cotización?',
      message: 'Al marcar como rechazada, ya no podrás ver más esta cotización.',
      buttons: [
        {
          text: 'No, regresar',
          handler: () => {
            console.log('Disagree clicked');
          }
        },
        {
          text: 'Confirmar',
          handler: () => {
            let loader = this.loadingCtrl.create({
                content: "Enviando..."
            });
            loader.present();
            let pregunta = {};
            pregunta['idprofesional'] =this.userApp['idprofesional'];
            pregunta['token_session'] =this.userApp['token_session'];
            pregunta['idcotizaciones'] =datos.idpreguntad;
            console.log(pregunta);
            this.ds.setRechazarCotizacion(pregunta).subscribe( data=> {
            
              if (data.errorCode==0){
                loader.dismiss();
                let alert = this.alertCtrl.create({
                  title: '¡Cotización rechazada!',
                  subTitle: 'La cotización ha sido rechazada y notificado al usuario.',
                  buttons: [{
                      text: 'Aceptar',
                      handler: data => {
                        this.navCtrl.popToRoot();
                      }
                    }]
                });
                alert.present();              
              }else{
                loader.dismiss();
              }
              console.log(data);
            });
            console.log('Agree clicked');
          }
        }
      ]
    });
    confirm.present();
  }

  gotoResponderDirecta(datos){
    console.log(datos);
    this.navCtrl.push('ChatproPage', {pregunta:datos,tipo:2});
 
  }

  doRefresh(refresher) {
    console.log('Begin async operation', refresher);
    this.cargarDataRefresh();

    setTimeout(() => {
      console.log('Async operation has ended');
      refresher.complete();
    }, 2000);
  }

  vermas(event,caracteres){

    if (event.target.classList.contains('vermas')){
      event.target.classList.add('vermenos'); // To ADD
      event.target.classList.remove('vermas'); // To Remove
    }else{
      event.target.classList.add('vermas'); // To ADD
      event.target.classList.remove('vermenos'); // To Remove     
    }
  }
}
