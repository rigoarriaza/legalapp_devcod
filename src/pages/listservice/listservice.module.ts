import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ListservicePage } from './listservice';

import { PipesModule } from '../../pipes/pipes.module';
// import { TruncateModule } from '@yellowspot/ng-truncate';

@NgModule({
  declarations: [
    ListservicePage,
  ],
  imports: [
    // TruncateModule,
    IonicPageModule.forChild(ListservicePage),PipesModule
  ],
  exports: [
    ListservicePage
  ]
})
export class ListservicePageModule {}
