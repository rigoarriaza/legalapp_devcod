import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, Events } from 'ionic-angular';

import { AuthProvider } from "../../providers/auth/auth";
import { CnnProvider } from "../../providers/cnn/cnn";

@IonicPage()
@Component({
  selector: 'page-intro',
  templateUrl: 'intro.html',
})
export class IntroPage {

  constructor(private cnn: CnnProvider, private authProvider: AuthProvider,public events: Events,public modalCtrl: ModalController,  public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Login');
    if(localStorage.getItem('legal_xpais' )== undefined){
      localStorage.setItem('legal_xpais','BO' )
      let modal = this.modalCtrl.create('XpaisPage');
      modal.present();
    }
  }

  ionViewDidEnter() {
    // this.menu.swipeEnable(false);
  }

  ionViewWillEnter(){

    //suscribirse al servicio de red
    this.cnn.con_info();

     
    // let userApp = localStorage.getItem('legal_userApp');

    if( this.authProvider.isLoggedIn()){
     
      this.navCtrl.setRoot('MenuPage');
    }
    // else{
      
      // userApp = JSON.parse(userApp);
      // // this.events.publish('user:created', userApp['tipo_user']);
      
      // if(userApp['tipo_user']=='2'){ //PROFESIONAL
      //   this.navCtrl.setRoot(HomeproPage);        
      // }else if(userApp['tipo_user']=='1'){ //PERSONA NATURAL
      //   this.navCtrl.setRoot('MenuPage');
      // }
    // }
  }
  // ionViewDidLeave() {
  //   this.menu.swipeEnable(true);
  // }

  gotologin(tipo){
    localStorage.setItem('userTipo',tipo);
    this.navCtrl.push('Login',{tipo:tipo})

  }

}
