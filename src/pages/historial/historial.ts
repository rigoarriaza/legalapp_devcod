import { Component, ChangeDetectorRef } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
// import {ChatPage} from '../chat/chat';

import { ServiceProvider } from '../../providers/service/service';
import { AuthProvider } from "../../providers/auth/auth";

@IonicPage()
@Component({
  selector: 'page-historial',
  templateUrl: 'historial.html',
})
export class HistorialPage {

  public token;
  public user;
  public preguntas: any;
  public preguntasDirectas: any;
  public preguntasCotizacion:any;
  tipopregunta: string = "exp";
  public inicio:number = 0;
  public fin: number = 10;
  
   
  public fakedatos: any;
  public haydatos: boolean = false;
  public arreglo = { "dato":
      [{"id":"1"},{"id":"2"},{"id":"3"},{"id":"4"},{"id":"5"},{"id":"6"}] 
    };
  constructor(private cf: ChangeDetectorRef, private authProvider: AuthProvider,public ds: ServiceProvider,public navCtrl: NavController, public navParams: NavParams) {
    this.fakedatos = this.arreglo.dato;

    this.token = this.authProvider.getToken(); 
    this.user = this.authProvider.getUsuario(); 
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad HistorialPage');
    
    this.cargarData();
  }


  ionViewDidEnter() {
    this.cargarData();
  }

  cargarData(){

    
    //get Preguntas express
    this.getExpress();
    
    //get Preguntas directas
    this.ds.getPreguntasDirectas(this.user,this.token).subscribe( data=> {
      
      if (data.errorCode==0){
        this.preguntasDirectas = data.msg;
      }
      this.haydatos= true;
       console.log(data);
    });

    //get Preguntas cotizaciones
    this.ds.getPreguntasCotizacion(this.user,this.token).subscribe( data=> {
      
      if (data.errorCode==0){
        this.preguntasCotizacion = data.msg;
      }
      this.haydatos= true;
       console.log(data);
    });
  }

  getExpress(){
    this.inicio = 0;
    this.fin= 10;
    this.ds.getPreguntasExpress(this.user,this.token,this.inicio,this.fin).subscribe( data=> {
      
      if (data.errorCode==0){
        this.preguntas = data.msg;
        this.haydatos= true;
      }else if (data.errorCode==3){
        // this.preguntas = {};
        this.haydatos= true;
      }
       console.log(data);
    });
  }

  gotoChat(datos,tipo){
    console.log(datos);
    if(tipo == 3){ //cotizacion
       this.navCtrl.push('CotizacionPage', {id:datos});
    }if(tipo == 2){ //directa
       this.navCtrl.push("ChatPage", {pregunta:datos.idpdirecta,tipo:tipo});
    }else{
      this.navCtrl.push("ChatPage", {pregunta:datos.idpreguntae,tipo:tipo});
      //Verificar si se puede modificar
      // if(datos.p_estado == 0 || datos.p_estado == 1 ){
      //   this.navCtrl.push('UpdatestatusPage', {pregunta:datos,tipo:tipo});
      // }else{
      //   this.navCtrl.push(ChatPage, {pregunta:datos.idpreguntae,tipo:tipo});
      // }
       
    }
   
  }
  gotoCotizar(datos,tipo){
    console.log(datos);
   
    this.navCtrl.push('CotizacionPage', {id:datos});
  }

  doRefresh(refresher) {
    console.log('Begin async operation', refresher);
    this.cargarData();

    setTimeout(() => {
      console.log('Async operation has ended');
      refresher.complete();
    }, 2000);
  }

  cargarMasExpress(infiniteScroll) {
    
    console.log('Begin async operation');
    this.inicio = this.fin;
    this.fin+=5;
    setTimeout(() => {
      // if(this.navParams.get('especialidad')!=undefined){
        this.ds.getPreguntasExpress(this.user,this.token,this.inicio,this.fin).subscribe( data=> {
      
          if (data.errorCode==0){
            for (let i = 0; i < data.msg.length; i++) {
              this.preguntas.push( data.msg[i] );
            }
            // this.abogados.concat(data.msg);
          }else if(data.errorCode==7){
            //error de token
          }
        }); 
      // }else{
      //   this.ds.getAbogados(this.authProvider.getUsuario(),this.authProvider.getToken(),this.inicio,this.fin).subscribe( data=> {
    
      //     if (data.errorCode==0){
      //       for (let i = 0; i < data.msg.length; i++) {
      //         this.abogados.push( data.msg[i] );
      //       }
      //       // this.abogados.concat(data.msg);
      //     }else if(data.errorCode==7){
      //       //error de token
      //     }
      //     console.log(this.abogados);
      //   });
      // }
      console.log('Async operation has ended');
      infiniteScroll.complete();
    }, 500);
  }

   segmentChanged()
  {
    this.cf.detectChanges();
  }


}
