import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { IntroPage } from '../pages/intro/intro';
// import { ChatPage } from '../pages/chat/chat';


import { HttpModule } from '@angular/http';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { Camera } from '@ionic-native/camera';
import { LocalNotifications } from '@ionic-native/local-notifications';
import { Network } from '@ionic-native/network';
import { Facebook } from '@ionic-native/facebook';
import { GoogleMaps } from '@ionic-native/google-maps';

//providers
import { ServiceProvider } from '../providers/service/service';
import { AuthProvider } from "../providers/auth/auth";
import { CnnProvider } from '../providers/cnn/cnn';

//lenguaje
import { registerLocaleData } from '@angular/common'; 
import localeFr from '@angular/common/locales/es-BO';
registerLocaleData(localeFr, 'es-BO');

//pipe
// import { FechasPipe } from "../pipes/fechas/fechas";

@NgModule({
  declarations: [
    MyApp,
    // ChatPage,
    IntroPage,
    // FechasPipe
  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicModule.forRoot(MyApp,{backButtonText: ''} )
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    // ChatPage,
    IntroPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    InAppBrowser,
    Camera,
    LocalNotifications,
    Network,
    Facebook,
    GoogleMaps,
    ServiceProvider,
    AuthProvider,
    CnnProvider
  ]
})
export class AppModule {}
